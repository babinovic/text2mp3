import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { Text2Mp3TestModule } from '../../../test.module';
import { UploadResponseComponent } from 'app/entities/upload-response/upload-response.component';
import { UploadResponseService } from 'app/entities/upload-response/upload-response.service';
import { UploadResponse } from 'app/shared/model/upload-response.model';

describe('Component Tests', () => {
  describe('UploadResponse Management Component', () => {
    let comp: UploadResponseComponent;
    let fixture: ComponentFixture<UploadResponseComponent>;
    let service: UploadResponseService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [UploadResponseComponent]
      })
        .overrideTemplate(UploadResponseComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UploadResponseComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UploadResponseService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new UploadResponse(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.uploadResponses && comp.uploadResponses[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
