import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { UploadResponseUpdateComponent } from 'app/entities/upload-response/upload-response-update.component';
import { UploadResponseService } from 'app/entities/upload-response/upload-response.service';
import { UploadResponse } from 'app/shared/model/upload-response.model';

describe('Component Tests', () => {
  describe('UploadResponse Management Update Component', () => {
    let comp: UploadResponseUpdateComponent;
    let fixture: ComponentFixture<UploadResponseUpdateComponent>;
    let service: UploadResponseService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [UploadResponseUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(UploadResponseUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UploadResponseUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UploadResponseService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UploadResponse(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UploadResponse();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
