import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { UploadResponseDetailComponent } from 'app/entities/upload-response/upload-response-detail.component';
import { UploadResponse } from 'app/shared/model/upload-response.model';

describe('Component Tests', () => {
  describe('UploadResponse Management Detail Component', () => {
    let comp: UploadResponseDetailComponent;
    let fixture: ComponentFixture<UploadResponseDetailComponent>;
    const route = ({ data: of({ uploadResponse: new UploadResponse(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [UploadResponseDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(UploadResponseDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UploadResponseDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load uploadResponse on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.uploadResponse).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
