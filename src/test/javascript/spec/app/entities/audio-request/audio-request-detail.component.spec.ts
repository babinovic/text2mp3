import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { AudioRequestDetailComponent } from 'app/entities/audio-request/audio-request-detail.component';
import { AudioRequest } from 'app/shared/model/audio-request.model';

describe('Component Tests', () => {
  describe('AudioRequest Management Detail Component', () => {
    let comp: AudioRequestDetailComponent;
    let fixture: ComponentFixture<AudioRequestDetailComponent>;
    const route = ({ data: of({ audioRequest: new AudioRequest(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [AudioRequestDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AudioRequestDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AudioRequestDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load audioRequest on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.audioRequest).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
