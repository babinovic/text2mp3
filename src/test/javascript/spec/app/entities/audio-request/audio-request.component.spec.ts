import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { Text2Mp3TestModule } from '../../../test.module';
import { AudioRequestComponent } from 'app/entities/audio-request/audio-request.component';
import { AudioRequestService } from 'app/entities/audio-request/audio-request.service';
import { AudioRequest } from 'app/shared/model/audio-request.model';

describe('Component Tests', () => {
  describe('AudioRequest Management Component', () => {
    let comp: AudioRequestComponent;
    let fixture: ComponentFixture<AudioRequestComponent>;
    let service: AudioRequestService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [AudioRequestComponent]
      })
        .overrideTemplate(AudioRequestComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AudioRequestComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AudioRequestService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AudioRequest(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.audioRequests && comp.audioRequests[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
