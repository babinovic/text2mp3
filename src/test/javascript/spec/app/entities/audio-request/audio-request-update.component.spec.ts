import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { AudioRequestUpdateComponent } from 'app/entities/audio-request/audio-request-update.component';
import { AudioRequestService } from 'app/entities/audio-request/audio-request.service';
import { AudioRequest } from 'app/shared/model/audio-request.model';

describe('Component Tests', () => {
  describe('AudioRequest Management Update Component', () => {
    let comp: AudioRequestUpdateComponent;
    let fixture: ComponentFixture<AudioRequestUpdateComponent>;
    let service: AudioRequestService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [AudioRequestUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AudioRequestUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AudioRequestUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AudioRequestService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AudioRequest(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AudioRequest();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
