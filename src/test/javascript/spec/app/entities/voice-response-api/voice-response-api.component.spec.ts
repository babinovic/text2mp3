import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { Text2Mp3TestModule } from '../../../test.module';
import { VoiceResponseAPIComponent } from 'app/entities/voice-response-api/voice-response-api.component';
import { VoiceResponseAPIService } from 'app/entities/voice-response-api/voice-response-api.service';
import { VoiceResponseAPI } from 'app/shared/model/voice-response-api.model';

describe('Component Tests', () => {
  describe('VoiceResponseAPI Management Component', () => {
    let comp: VoiceResponseAPIComponent;
    let fixture: ComponentFixture<VoiceResponseAPIComponent>;
    let service: VoiceResponseAPIService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [VoiceResponseAPIComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: {
                subscribe: (fn: (value: Data) => void) =>
                  fn({
                    pagingParams: {
                      predicate: 'id',
                      reverse: false,
                      page: 0
                    }
                  })
              }
            }
          }
        ]
      })
        .overrideTemplate(VoiceResponseAPIComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VoiceResponseAPIComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VoiceResponseAPIService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new VoiceResponseAPI(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.voiceResponseAPIS && comp.voiceResponseAPIS[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new VoiceResponseAPI(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.voiceResponseAPIS && comp.voiceResponseAPIS[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
