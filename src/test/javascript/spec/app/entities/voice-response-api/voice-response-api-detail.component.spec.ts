import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { VoiceResponseAPIDetailComponent } from 'app/entities/voice-response-api/voice-response-api-detail.component';
import { VoiceResponseAPI } from 'app/shared/model/voice-response-api.model';

describe('Component Tests', () => {
  describe('VoiceResponseAPI Management Detail Component', () => {
    let comp: VoiceResponseAPIDetailComponent;
    let fixture: ComponentFixture<VoiceResponseAPIDetailComponent>;
    const route = ({ data: of({ voiceResponseAPI: new VoiceResponseAPI(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [VoiceResponseAPIDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(VoiceResponseAPIDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VoiceResponseAPIDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load voiceResponseAPI on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.voiceResponseAPI).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
