import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { VoiceResponseAPIService } from 'app/entities/voice-response-api/voice-response-api.service';
import { IVoiceResponseAPI, VoiceResponseAPI } from 'app/shared/model/voice-response-api.model';

describe('Service Tests', () => {
  describe('VoiceResponseAPI Service', () => {
    let injector: TestBed;
    let service: VoiceResponseAPIService;
    let httpMock: HttpTestingController;
    let elemDefault: IVoiceResponseAPI;
    let expectedResult: IVoiceResponseAPI | IVoiceResponseAPI[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(VoiceResponseAPIService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new VoiceResponseAPI(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a VoiceResponseAPI', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new VoiceResponseAPI()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a VoiceResponseAPI', () => {
        const returnedFromService = Object.assign(
          {
            mp3Path: 'BBBBBB',
            language: 'BBBBBB',
            text: 'BBBBBB',
            textPath: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of VoiceResponseAPI', () => {
        const returnedFromService = Object.assign(
          {
            mp3Path: 'BBBBBB',
            language: 'BBBBBB',
            text: 'BBBBBB',
            textPath: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a VoiceResponseAPI', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
