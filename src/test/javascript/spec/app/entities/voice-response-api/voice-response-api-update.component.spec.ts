import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { VoiceResponseAPIUpdateComponent } from 'app/entities/voice-response-api/voice-response-api-update.component';
import { VoiceResponseAPIService } from 'app/entities/voice-response-api/voice-response-api.service';
import { VoiceResponseAPI } from 'app/shared/model/voice-response-api.model';

describe('Component Tests', () => {
  describe('VoiceResponseAPI Management Update Component', () => {
    let comp: VoiceResponseAPIUpdateComponent;
    let fixture: ComponentFixture<VoiceResponseAPIUpdateComponent>;
    let service: VoiceResponseAPIService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [VoiceResponseAPIUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(VoiceResponseAPIUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VoiceResponseAPIUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VoiceResponseAPIService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new VoiceResponseAPI(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new VoiceResponseAPI();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
