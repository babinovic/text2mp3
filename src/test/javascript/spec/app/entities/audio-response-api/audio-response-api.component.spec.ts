import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { Text2Mp3TestModule } from '../../../test.module';
import { AudioResponseAPIComponent } from 'app/entities/audio-response-api/audio-response-api.component';
import { AudioResponseAPIService } from 'app/entities/audio-response-api/audio-response-api.service';
import { AudioResponseAPI } from 'app/shared/model/audio-response-api.model';

describe('Component Tests', () => {
  describe('AudioResponseAPI Management Component', () => {
    let comp: AudioResponseAPIComponent;
    let fixture: ComponentFixture<AudioResponseAPIComponent>;
    let service: AudioResponseAPIService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [AudioResponseAPIComponent]
      })
        .overrideTemplate(AudioResponseAPIComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AudioResponseAPIComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AudioResponseAPIService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AudioResponseAPI(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.audioResponseAPIS && comp.audioResponseAPIS[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
