import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { AudioResponseAPIUpdateComponent } from 'app/entities/audio-response-api/audio-response-api-update.component';
import { AudioResponseAPIService } from 'app/entities/audio-response-api/audio-response-api.service';
import { AudioResponseAPI } from 'app/shared/model/audio-response-api.model';

describe('Component Tests', () => {
  describe('AudioResponseAPI Management Update Component', () => {
    let comp: AudioResponseAPIUpdateComponent;
    let fixture: ComponentFixture<AudioResponseAPIUpdateComponent>;
    let service: AudioResponseAPIService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [AudioResponseAPIUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AudioResponseAPIUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AudioResponseAPIUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AudioResponseAPIService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AudioResponseAPI(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AudioResponseAPI();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
