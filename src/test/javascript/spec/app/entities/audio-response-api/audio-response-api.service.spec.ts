import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AudioResponseAPIService } from 'app/entities/audio-response-api/audio-response-api.service';
import { IAudioResponseAPI, AudioResponseAPI } from 'app/shared/model/audio-response-api.model';

describe('Service Tests', () => {
  describe('AudioResponseAPI Service', () => {
    let injector: TestBed;
    let service: AudioResponseAPIService;
    let httpMock: HttpTestingController;
    let elemDefault: IAudioResponseAPI;
    let expectedResult: IAudioResponseAPI | IAudioResponseAPI[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(AudioResponseAPIService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new AudioResponseAPI(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a AudioResponseAPI', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new AudioResponseAPI()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a AudioResponseAPI', () => {
        const returnedFromService = Object.assign(
          {
            filePath: 'BBBBBB',
            text: 'BBBBBB',
            textPath: 'BBBBBB',
            language: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of AudioResponseAPI', () => {
        const returnedFromService = Object.assign(
          {
            filePath: 'BBBBBB',
            text: 'BBBBBB',
            textPath: 'BBBBBB',
            language: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a AudioResponseAPI', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
