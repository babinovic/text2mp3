import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { AudioResponseAPIDetailComponent } from 'app/entities/audio-response-api/audio-response-api-detail.component';
import { AudioResponseAPI } from 'app/shared/model/audio-response-api.model';

describe('Component Tests', () => {
  describe('AudioResponseAPI Management Detail Component', () => {
    let comp: AudioResponseAPIDetailComponent;
    let fixture: ComponentFixture<AudioResponseAPIDetailComponent>;
    const route = ({ data: of({ audioResponseAPI: new AudioResponseAPI(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [AudioResponseAPIDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AudioResponseAPIDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AudioResponseAPIDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load audioResponseAPI on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.audioResponseAPI).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
