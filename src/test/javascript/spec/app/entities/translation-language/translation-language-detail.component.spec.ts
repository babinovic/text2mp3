import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { TranslationLanguageDetailComponent } from 'app/entities/translation-language/translation-language-detail.component';
import { TranslationLanguage } from 'app/shared/model/translation-language.model';

describe('Component Tests', () => {
  describe('TranslationLanguage Management Detail Component', () => {
    let comp: TranslationLanguageDetailComponent;
    let fixture: ComponentFixture<TranslationLanguageDetailComponent>;
    const route = ({ data: of({ translationLanguage: new TranslationLanguage(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [TranslationLanguageDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TranslationLanguageDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TranslationLanguageDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load translationLanguage on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.translationLanguage).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
