import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { TranslationLanguageUpdateComponent } from 'app/entities/translation-language/translation-language-update.component';
import { TranslationLanguageService } from 'app/entities/translation-language/translation-language.service';
import { TranslationLanguage } from 'app/shared/model/translation-language.model';

describe('Component Tests', () => {
  describe('TranslationLanguage Management Update Component', () => {
    let comp: TranslationLanguageUpdateComponent;
    let fixture: ComponentFixture<TranslationLanguageUpdateComponent>;
    let service: TranslationLanguageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [TranslationLanguageUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TranslationLanguageUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TranslationLanguageUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TranslationLanguageService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TranslationLanguage(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TranslationLanguage();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
