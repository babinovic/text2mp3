import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { Text2Mp3TestModule } from '../../../test.module';
import { TranslationLanguageComponent } from 'app/entities/translation-language/translation-language.component';
import { TranslationLanguageService } from 'app/entities/translation-language/translation-language.service';
import { TranslationLanguage } from 'app/shared/model/translation-language.model';

describe('Component Tests', () => {
  describe('TranslationLanguage Management Component', () => {
    let comp: TranslationLanguageComponent;
    let fixture: ComponentFixture<TranslationLanguageComponent>;
    let service: TranslationLanguageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [TranslationLanguageComponent]
      })
        .overrideTemplate(TranslationLanguageComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TranslationLanguageComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TranslationLanguageService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new TranslationLanguage(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.translationLanguages && comp.translationLanguages[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
