import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { VoiceRequestUpdateComponent } from 'app/entities/voice-request/voice-request-update.component';
import { VoiceRequestService } from 'app/entities/voice-request/voice-request.service';
import { VoiceRequest } from 'app/shared/model/voice-request.model';

describe('Component Tests', () => {
  describe('VoiceRequest Management Update Component', () => {
    let comp: VoiceRequestUpdateComponent;
    let fixture: ComponentFixture<VoiceRequestUpdateComponent>;
    let service: VoiceRequestService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [VoiceRequestUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(VoiceRequestUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VoiceRequestUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VoiceRequestService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new VoiceRequest(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new VoiceRequest();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
