import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { VoiceRequestDetailComponent } from 'app/entities/voice-request/voice-request-detail.component';
import { VoiceRequest } from 'app/shared/model/voice-request.model';

describe('Component Tests', () => {
  describe('VoiceRequest Management Detail Component', () => {
    let comp: VoiceRequestDetailComponent;
    let fixture: ComponentFixture<VoiceRequestDetailComponent>;
    const route = ({ data: of({ voiceRequest: new VoiceRequest(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [VoiceRequestDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(VoiceRequestDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VoiceRequestDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load voiceRequest on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.voiceRequest).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
