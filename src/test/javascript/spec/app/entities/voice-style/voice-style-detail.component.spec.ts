import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { VoiceStyleDetailComponent } from 'app/entities/voice-style/voice-style-detail.component';
import { VoiceStyle } from 'app/shared/model/voice-style.model';

describe('Component Tests', () => {
  describe('VoiceStyle Management Detail Component', () => {
    let comp: VoiceStyleDetailComponent;
    let fixture: ComponentFixture<VoiceStyleDetailComponent>;
    const route = ({ data: of({ voiceStyle: new VoiceStyle(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [VoiceStyleDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(VoiceStyleDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VoiceStyleDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load voiceStyle on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.voiceStyle).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
