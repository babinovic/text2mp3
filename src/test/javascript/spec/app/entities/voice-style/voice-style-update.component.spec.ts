import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { Text2Mp3TestModule } from '../../../test.module';
import { VoiceStyleUpdateComponent } from 'app/entities/voice-style/voice-style-update.component';
import { VoiceStyleService } from 'app/entities/voice-style/voice-style.service';
import { VoiceStyle } from 'app/shared/model/voice-style.model';

describe('Component Tests', () => {
  describe('VoiceStyle Management Update Component', () => {
    let comp: VoiceStyleUpdateComponent;
    let fixture: ComponentFixture<VoiceStyleUpdateComponent>;
    let service: VoiceStyleService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Text2Mp3TestModule],
        declarations: [VoiceStyleUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(VoiceStyleUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VoiceStyleUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VoiceStyleService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new VoiceStyle(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new VoiceStyle();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
