package org.danet.web.rest;

import org.danet.Text2Mp3App;
import org.danet.domain.VoiceStyle;
import org.danet.repository.VoiceStyleRepository;
import org.danet.service.VoiceStyleService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.danet.domain.enumeration.Gender;
/**
 * Integration tests for the {@link VoiceStyleResource} REST controller.
 */
@SpringBootTest(classes = Text2Mp3App.class)

@AutoConfigureMockMvc
@WithMockUser
public class VoiceStyleResourceIT {

    private static final String DEFAULT_VOICE_STYLE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_VOICE_STYLE_NAME = "BBBBBBBBBB";

    private static final Gender DEFAULT_VOICE_STYLE_GENDER = Gender.MALE;
    private static final Gender UPDATED_VOICE_STYLE_GENDER = Gender.FEMALE;

    @Autowired
    private VoiceStyleRepository voiceStyleRepository;

    @Autowired
    private VoiceStyleService voiceStyleService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVoiceStyleMockMvc;

    private VoiceStyle voiceStyle;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VoiceStyle createEntity(EntityManager em) {
        VoiceStyle voiceStyle = new VoiceStyle()
            .voiceStyleName(DEFAULT_VOICE_STYLE_NAME)
            .voiceStyleGender(DEFAULT_VOICE_STYLE_GENDER);
        return voiceStyle;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VoiceStyle createUpdatedEntity(EntityManager em) {
        VoiceStyle voiceStyle = new VoiceStyle()
            .voiceStyleName(UPDATED_VOICE_STYLE_NAME)
            .voiceStyleGender(UPDATED_VOICE_STYLE_GENDER);
        return voiceStyle;
    }

    @BeforeEach
    public void initTest() {
        voiceStyle = createEntity(em);
    }

    @Test
    @Transactional
    public void createVoiceStyle() throws Exception {
        int databaseSizeBeforeCreate = voiceStyleRepository.findAll().size();

        // Create the VoiceStyle
        restVoiceStyleMockMvc.perform(post("/api/voice-styles")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(voiceStyle)))
            .andExpect(status().isCreated());

        // Validate the VoiceStyle in the database
        List<VoiceStyle> voiceStyleList = voiceStyleRepository.findAll();
        assertThat(voiceStyleList).hasSize(databaseSizeBeforeCreate + 1);
        VoiceStyle testVoiceStyle = voiceStyleList.get(voiceStyleList.size() - 1);
        assertThat(testVoiceStyle.getVoiceStyleName()).isEqualTo(DEFAULT_VOICE_STYLE_NAME);
        assertThat(testVoiceStyle.getVoiceStyleGender()).isEqualTo(DEFAULT_VOICE_STYLE_GENDER);
    }

    @Test
    @Transactional
    public void createVoiceStyleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = voiceStyleRepository.findAll().size();

        // Create the VoiceStyle with an existing ID
        voiceStyle.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVoiceStyleMockMvc.perform(post("/api/voice-styles")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(voiceStyle)))
            .andExpect(status().isBadRequest());

        // Validate the VoiceStyle in the database
        List<VoiceStyle> voiceStyleList = voiceStyleRepository.findAll();
        assertThat(voiceStyleList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllVoiceStyles() throws Exception {
        // Initialize the database
        voiceStyleRepository.saveAndFlush(voiceStyle);

        // Get all the voiceStyleList
        restVoiceStyleMockMvc.perform(get("/api/voice-styles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(voiceStyle.getId().intValue())))
            .andExpect(jsonPath("$.[*].voiceStyleName").value(hasItem(DEFAULT_VOICE_STYLE_NAME)))
            .andExpect(jsonPath("$.[*].voiceStyleGender").value(hasItem(DEFAULT_VOICE_STYLE_GENDER.toString())));
    }
    
    @Test
    @Transactional
    public void getVoiceStyle() throws Exception {
        // Initialize the database
        voiceStyleRepository.saveAndFlush(voiceStyle);

        // Get the voiceStyle
        restVoiceStyleMockMvc.perform(get("/api/voice-styles/{id}", voiceStyle.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(voiceStyle.getId().intValue()))
            .andExpect(jsonPath("$.voiceStyleName").value(DEFAULT_VOICE_STYLE_NAME))
            .andExpect(jsonPath("$.voiceStyleGender").value(DEFAULT_VOICE_STYLE_GENDER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingVoiceStyle() throws Exception {
        // Get the voiceStyle
        restVoiceStyleMockMvc.perform(get("/api/voice-styles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVoiceStyle() throws Exception {
        // Initialize the database
        voiceStyleService.save(voiceStyle);

        int databaseSizeBeforeUpdate = voiceStyleRepository.findAll().size();

        // Update the voiceStyle
        VoiceStyle updatedVoiceStyle = voiceStyleRepository.findById(voiceStyle.getId()).get();
        // Disconnect from session so that the updates on updatedVoiceStyle are not directly saved in db
        em.detach(updatedVoiceStyle);
        updatedVoiceStyle
            .voiceStyleName(UPDATED_VOICE_STYLE_NAME)
            .voiceStyleGender(UPDATED_VOICE_STYLE_GENDER);

        restVoiceStyleMockMvc.perform(put("/api/voice-styles")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedVoiceStyle)))
            .andExpect(status().isOk());

        // Validate the VoiceStyle in the database
        List<VoiceStyle> voiceStyleList = voiceStyleRepository.findAll();
        assertThat(voiceStyleList).hasSize(databaseSizeBeforeUpdate);
        VoiceStyle testVoiceStyle = voiceStyleList.get(voiceStyleList.size() - 1);
        assertThat(testVoiceStyle.getVoiceStyleName()).isEqualTo(UPDATED_VOICE_STYLE_NAME);
        assertThat(testVoiceStyle.getVoiceStyleGender()).isEqualTo(UPDATED_VOICE_STYLE_GENDER);
    }

    @Test
    @Transactional
    public void updateNonExistingVoiceStyle() throws Exception {
        int databaseSizeBeforeUpdate = voiceStyleRepository.findAll().size();

        // Create the VoiceStyle

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVoiceStyleMockMvc.perform(put("/api/voice-styles")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(voiceStyle)))
            .andExpect(status().isBadRequest());

        // Validate the VoiceStyle in the database
        List<VoiceStyle> voiceStyleList = voiceStyleRepository.findAll();
        assertThat(voiceStyleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVoiceStyle() throws Exception {
        // Initialize the database
        voiceStyleService.save(voiceStyle);

        int databaseSizeBeforeDelete = voiceStyleRepository.findAll().size();

        // Delete the voiceStyle
        restVoiceStyleMockMvc.perform(delete("/api/voice-styles/{id}", voiceStyle.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VoiceStyle> voiceStyleList = voiceStyleRepository.findAll();
        assertThat(voiceStyleList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
