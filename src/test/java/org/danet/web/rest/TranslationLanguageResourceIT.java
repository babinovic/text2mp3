package org.danet.web.rest;

import org.danet.Text2Mp3App;
import org.danet.domain.TranslationLanguage;
import org.danet.repository.TranslationLanguageRepository;
import org.danet.service.TranslationLanguageService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TranslationLanguageResource} REST controller.
 */
@SpringBootTest(classes = Text2Mp3App.class)

@AutoConfigureMockMvc
@WithMockUser
public class TranslationLanguageResourceIT {

    private static final String DEFAULT_TRANSLATION_LANGUAGE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_TRANSLATION_LANGUAGE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TRANSLATION_LANGUAGE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_TRANSLATION_LANGUAGE_CODE = "BBBBBBBBBB";

    @Autowired
    private TranslationLanguageRepository translationLanguageRepository;

    @Autowired
    private TranslationLanguageService translationLanguageService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTranslationLanguageMockMvc;

    private TranslationLanguage translationLanguage;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TranslationLanguage createEntity(EntityManager em) {
        TranslationLanguage translationLanguage = new TranslationLanguage()
            .translationLanguageName(DEFAULT_TRANSLATION_LANGUAGE_NAME)
            .translationLanguageCode(DEFAULT_TRANSLATION_LANGUAGE_CODE);
        return translationLanguage;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TranslationLanguage createUpdatedEntity(EntityManager em) {
        TranslationLanguage translationLanguage = new TranslationLanguage()
            .translationLanguageName(UPDATED_TRANSLATION_LANGUAGE_NAME)
            .translationLanguageCode(UPDATED_TRANSLATION_LANGUAGE_CODE);
        return translationLanguage;
    }

    @BeforeEach
    public void initTest() {
        translationLanguage = createEntity(em);
    }

    @Test
    @Transactional
    public void createTranslationLanguage() throws Exception {
        int databaseSizeBeforeCreate = translationLanguageRepository.findAll().size();

        // Create the TranslationLanguage
        restTranslationLanguageMockMvc.perform(post("/api/translation-languages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(translationLanguage)))
            .andExpect(status().isCreated());

        // Validate the TranslationLanguage in the database
        List<TranslationLanguage> translationLanguageList = translationLanguageRepository.findAll();
        assertThat(translationLanguageList).hasSize(databaseSizeBeforeCreate + 1);
        TranslationLanguage testTranslationLanguage = translationLanguageList.get(translationLanguageList.size() - 1);
        assertThat(testTranslationLanguage.getTranslationLanguageName()).isEqualTo(DEFAULT_TRANSLATION_LANGUAGE_NAME);
        assertThat(testTranslationLanguage.getTranslationLanguageCode()).isEqualTo(DEFAULT_TRANSLATION_LANGUAGE_CODE);
    }

    @Test
    @Transactional
    public void createTranslationLanguageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = translationLanguageRepository.findAll().size();

        // Create the TranslationLanguage with an existing ID
        translationLanguage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTranslationLanguageMockMvc.perform(post("/api/translation-languages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(translationLanguage)))
            .andExpect(status().isBadRequest());

        // Validate the TranslationLanguage in the database
        List<TranslationLanguage> translationLanguageList = translationLanguageRepository.findAll();
        assertThat(translationLanguageList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTranslationLanguages() throws Exception {
        // Initialize the database
        translationLanguageRepository.saveAndFlush(translationLanguage);

        // Get all the translationLanguageList
        restTranslationLanguageMockMvc.perform(get("/api/translation-languages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(translationLanguage.getId().intValue())))
            .andExpect(jsonPath("$.[*].translationLanguageName").value(hasItem(DEFAULT_TRANSLATION_LANGUAGE_NAME)))
            .andExpect(jsonPath("$.[*].translationLanguageCode").value(hasItem(DEFAULT_TRANSLATION_LANGUAGE_CODE)));
    }
    
    @Test
    @Transactional
    public void getTranslationLanguage() throws Exception {
        // Initialize the database
        translationLanguageRepository.saveAndFlush(translationLanguage);

        // Get the translationLanguage
        restTranslationLanguageMockMvc.perform(get("/api/translation-languages/{id}", translationLanguage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(translationLanguage.getId().intValue()))
            .andExpect(jsonPath("$.translationLanguageName").value(DEFAULT_TRANSLATION_LANGUAGE_NAME))
            .andExpect(jsonPath("$.translationLanguageCode").value(DEFAULT_TRANSLATION_LANGUAGE_CODE));
    }

    @Test
    @Transactional
    public void getNonExistingTranslationLanguage() throws Exception {
        // Get the translationLanguage
        restTranslationLanguageMockMvc.perform(get("/api/translation-languages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTranslationLanguage() throws Exception {
        // Initialize the database
        translationLanguageService.save(translationLanguage);

        int databaseSizeBeforeUpdate = translationLanguageRepository.findAll().size();

        // Update the translationLanguage
        TranslationLanguage updatedTranslationLanguage = translationLanguageRepository.findById(translationLanguage.getId()).get();
        // Disconnect from session so that the updates on updatedTranslationLanguage are not directly saved in db
        em.detach(updatedTranslationLanguage);
        updatedTranslationLanguage
            .translationLanguageName(UPDATED_TRANSLATION_LANGUAGE_NAME)
            .translationLanguageCode(UPDATED_TRANSLATION_LANGUAGE_CODE);

        restTranslationLanguageMockMvc.perform(put("/api/translation-languages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedTranslationLanguage)))
            .andExpect(status().isOk());

        // Validate the TranslationLanguage in the database
        List<TranslationLanguage> translationLanguageList = translationLanguageRepository.findAll();
        assertThat(translationLanguageList).hasSize(databaseSizeBeforeUpdate);
        TranslationLanguage testTranslationLanguage = translationLanguageList.get(translationLanguageList.size() - 1);
        assertThat(testTranslationLanguage.getTranslationLanguageName()).isEqualTo(UPDATED_TRANSLATION_LANGUAGE_NAME);
        assertThat(testTranslationLanguage.getTranslationLanguageCode()).isEqualTo(UPDATED_TRANSLATION_LANGUAGE_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingTranslationLanguage() throws Exception {
        int databaseSizeBeforeUpdate = translationLanguageRepository.findAll().size();

        // Create the TranslationLanguage

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTranslationLanguageMockMvc.perform(put("/api/translation-languages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(translationLanguage)))
            .andExpect(status().isBadRequest());

        // Validate the TranslationLanguage in the database
        List<TranslationLanguage> translationLanguageList = translationLanguageRepository.findAll();
        assertThat(translationLanguageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTranslationLanguage() throws Exception {
        // Initialize the database
        translationLanguageService.save(translationLanguage);

        int databaseSizeBeforeDelete = translationLanguageRepository.findAll().size();

        // Delete the translationLanguage
        restTranslationLanguageMockMvc.perform(delete("/api/translation-languages/{id}", translationLanguage.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TranslationLanguage> translationLanguageList = translationLanguageRepository.findAll();
        assertThat(translationLanguageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
