package org.danet.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.danet.Text2Mp3App;
import org.danet.domain.AudioRequest;
import org.danet.repository.AudioRequestRepository;
import org.danet.service.AudioRequestService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AudioRequestResource} REST controller.
 */
@SpringBootTest(classes = Text2Mp3App.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class AudioRequestResourceIT {

    private static final String DEFAULT_AUDIO_FILE_PATH = "AAAAAAAAAA";
    private static final String UPDATED_AUDIO_FILE_PATH = "BBBBBBBBBB";

    private static final Boolean DEFAULT_VALIDATION = false;
    private static final Boolean UPDATED_VALIDATION = true;

    private static final Boolean DEFAULT_VALIDATION_RECOGNITION = false;
    private static final Boolean UPDATED_VALIDATION_RECOGNITION = true;

    private static final String DEFAULT_RECOGNITION_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_RECOGNITION_TEXT = "BBBBBBBBBB";

    @Autowired
    private AudioRequestRepository audioRequestRepository;

    @Mock
    private AudioRequestRepository audioRequestRepositoryMock;

    @Mock
    private AudioRequestService audioRequestServiceMock;

    @Autowired
    private AudioRequestService audioRequestService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAudioRequestMockMvc;

    private AudioRequest audioRequest;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AudioRequest createEntity(EntityManager em) {
        AudioRequest audioRequest = new AudioRequest()
            .audioFilePath(DEFAULT_AUDIO_FILE_PATH)
            .validation(DEFAULT_VALIDATION)
            .validationRecognition(DEFAULT_VALIDATION_RECOGNITION)
            .recognitionText(DEFAULT_RECOGNITION_TEXT);
        return audioRequest;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AudioRequest createUpdatedEntity(EntityManager em) {
        AudioRequest audioRequest = new AudioRequest()
            .audioFilePath(UPDATED_AUDIO_FILE_PATH)
            .validation(UPDATED_VALIDATION)
            .validationRecognition(UPDATED_VALIDATION_RECOGNITION)
            .recognitionText(UPDATED_RECOGNITION_TEXT);
        return audioRequest;
    }

    @BeforeEach
    public void initTest() {
        audioRequest = createEntity(em);
    }

    @Test
    @Transactional
    public void createAudioRequest() throws Exception {
        int databaseSizeBeforeCreate = audioRequestRepository.findAll().size();

        // Create the AudioRequest
        restAudioRequestMockMvc.perform(post("/api/audio-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(audioRequest)))
            .andExpect(status().isCreated());

        // Validate the AudioRequest in the database
        List<AudioRequest> audioRequestList = audioRequestRepository.findAll();
        assertThat(audioRequestList).hasSize(databaseSizeBeforeCreate + 1);
        AudioRequest testAudioRequest = audioRequestList.get(audioRequestList.size() - 1);
        assertThat(testAudioRequest.getAudioFilePath()).isEqualTo(DEFAULT_AUDIO_FILE_PATH);
        assertThat(testAudioRequest.isValidation()).isEqualTo(DEFAULT_VALIDATION);
        assertThat(testAudioRequest.isValidationRecognition()).isEqualTo(DEFAULT_VALIDATION_RECOGNITION);
        assertThat(testAudioRequest.getRecognitionText()).isEqualTo(DEFAULT_RECOGNITION_TEXT);
    }

    @Test
    @Transactional
    public void createAudioRequestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = audioRequestRepository.findAll().size();

        // Create the AudioRequest with an existing ID
        audioRequest.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAudioRequestMockMvc.perform(post("/api/audio-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(audioRequest)))
            .andExpect(status().isBadRequest());

        // Validate the AudioRequest in the database
        List<AudioRequest> audioRequestList = audioRequestRepository.findAll();
        assertThat(audioRequestList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAudioRequests() throws Exception {
        // Initialize the database
        audioRequestRepository.saveAndFlush(audioRequest);

        // Get all the audioRequestList
        restAudioRequestMockMvc.perform(get("/api/audio-requests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(audioRequest.getId().intValue())))
            .andExpect(jsonPath("$.[*].audioFilePath").value(hasItem(DEFAULT_AUDIO_FILE_PATH)))
            .andExpect(jsonPath("$.[*].validation").value(hasItem(DEFAULT_VALIDATION.booleanValue())))
            .andExpect(jsonPath("$.[*].validationRecognition").value(hasItem(DEFAULT_VALIDATION_RECOGNITION.booleanValue())))
            .andExpect(jsonPath("$.[*].recognitionText").value(hasItem(DEFAULT_RECOGNITION_TEXT)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllAudioRequestsWithEagerRelationshipsIsEnabled() throws Exception {
        when(audioRequestServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restAudioRequestMockMvc.perform(get("/api/audio-requests?eagerload=true"))
            .andExpect(status().isOk());

        verify(audioRequestServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllAudioRequestsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(audioRequestServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restAudioRequestMockMvc.perform(get("/api/audio-requests?eagerload=true"))
            .andExpect(status().isOk());

        verify(audioRequestServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getAudioRequest() throws Exception {
        // Initialize the database
        audioRequestRepository.saveAndFlush(audioRequest);

        // Get the audioRequest
        restAudioRequestMockMvc.perform(get("/api/audio-requests/{id}", audioRequest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(audioRequest.getId().intValue()))
            .andExpect(jsonPath("$.audioFilePath").value(DEFAULT_AUDIO_FILE_PATH))
            .andExpect(jsonPath("$.validation").value(DEFAULT_VALIDATION.booleanValue()))
            .andExpect(jsonPath("$.validationRecognition").value(DEFAULT_VALIDATION_RECOGNITION.booleanValue()))
            .andExpect(jsonPath("$.recognitionText").value(DEFAULT_RECOGNITION_TEXT));
    }

    @Test
    @Transactional
    public void getNonExistingAudioRequest() throws Exception {
        // Get the audioRequest
        restAudioRequestMockMvc.perform(get("/api/audio-requests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAudioRequest() throws Exception {
        // Initialize the database
        audioRequestService.save(audioRequest);

        int databaseSizeBeforeUpdate = audioRequestRepository.findAll().size();

        // Update the audioRequest
        AudioRequest updatedAudioRequest = audioRequestRepository.findById(audioRequest.getId()).get();
        // Disconnect from session so that the updates on updatedAudioRequest are not directly saved in db
        em.detach(updatedAudioRequest);
        updatedAudioRequest
            .audioFilePath(UPDATED_AUDIO_FILE_PATH)
            .validation(UPDATED_VALIDATION)
            .validationRecognition(UPDATED_VALIDATION_RECOGNITION)
            .recognitionText(UPDATED_RECOGNITION_TEXT);

        restAudioRequestMockMvc.perform(put("/api/audio-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAudioRequest)))
            .andExpect(status().isOk());

        // Validate the AudioRequest in the database
        List<AudioRequest> audioRequestList = audioRequestRepository.findAll();
        assertThat(audioRequestList).hasSize(databaseSizeBeforeUpdate);
        AudioRequest testAudioRequest = audioRequestList.get(audioRequestList.size() - 1);
        assertThat(testAudioRequest.getAudioFilePath()).isEqualTo(UPDATED_AUDIO_FILE_PATH);
        assertThat(testAudioRequest.isValidation()).isEqualTo(UPDATED_VALIDATION);
        assertThat(testAudioRequest.isValidationRecognition()).isEqualTo(UPDATED_VALIDATION_RECOGNITION);
        assertThat(testAudioRequest.getRecognitionText()).isEqualTo(UPDATED_RECOGNITION_TEXT);
    }

    @Test
    @Transactional
    public void updateNonExistingAudioRequest() throws Exception {
        int databaseSizeBeforeUpdate = audioRequestRepository.findAll().size();

        // Create the AudioRequest

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAudioRequestMockMvc.perform(put("/api/audio-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(audioRequest)))
            .andExpect(status().isBadRequest());

        // Validate the AudioRequest in the database
        List<AudioRequest> audioRequestList = audioRequestRepository.findAll();
        assertThat(audioRequestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAudioRequest() throws Exception {
        // Initialize the database
        audioRequestService.save(audioRequest);

        int databaseSizeBeforeDelete = audioRequestRepository.findAll().size();

        // Delete the audioRequest
        restAudioRequestMockMvc.perform(delete("/api/audio-requests/{id}", audioRequest.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AudioRequest> audioRequestList = audioRequestRepository.findAll();
        assertThat(audioRequestList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
