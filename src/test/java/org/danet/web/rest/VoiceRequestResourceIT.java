package org.danet.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.danet.Text2Mp3App;
import org.danet.domain.VoiceRequest;
import org.danet.repository.VoiceRequestRepository;
import org.danet.service.VoiceRequestService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link VoiceRequestResource} REST controller.
 */
@SpringBootTest(classes = Text2Mp3App.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class VoiceRequestResourceIT {

    private static final String DEFAULT_VOICE_REQUEST_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_VOICE_REQUEST_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_VOICE_REQUEST_TEXT_FORMAT = "AAAAAAAAAA";
    private static final String UPDATED_VOICE_REQUEST_TEXT_FORMAT = "BBBBBBBBBB";

    private static final Boolean DEFAULT_VALIDATION = false;
    private static final Boolean UPDATED_VALIDATION = true;

    @Autowired
    private VoiceRequestRepository voiceRequestRepository;

    @Mock
    private VoiceRequestRepository voiceRequestRepositoryMock;

    @Mock
    private VoiceRequestService voiceRequestServiceMock;

    @Autowired
    private VoiceRequestService voiceRequestService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVoiceRequestMockMvc;

    private VoiceRequest voiceRequest;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VoiceRequest createEntity(EntityManager em) {
        VoiceRequest voiceRequest = new VoiceRequest()
            .voiceRequestText(DEFAULT_VOICE_REQUEST_TEXT)
            .voiceRequestTextFormat(DEFAULT_VOICE_REQUEST_TEXT_FORMAT)
            .validation(DEFAULT_VALIDATION);
        return voiceRequest;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VoiceRequest createUpdatedEntity(EntityManager em) {
        VoiceRequest voiceRequest = new VoiceRequest()
            .voiceRequestText(UPDATED_VOICE_REQUEST_TEXT)
            .voiceRequestTextFormat(UPDATED_VOICE_REQUEST_TEXT_FORMAT)
            .validation(UPDATED_VALIDATION);
        return voiceRequest;
    }

    @BeforeEach
    public void initTest() {
        voiceRequest = createEntity(em);
    }

    @Test
    @Transactional
    public void createVoiceRequest() throws Exception {
        int databaseSizeBeforeCreate = voiceRequestRepository.findAll().size();

        // Create the VoiceRequest
        restVoiceRequestMockMvc.perform(post("/api/voice-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(voiceRequest)))
            .andExpect(status().isCreated());

        // Validate the VoiceRequest in the database
        List<VoiceRequest> voiceRequestList = voiceRequestRepository.findAll();
        assertThat(voiceRequestList).hasSize(databaseSizeBeforeCreate + 1);
        VoiceRequest testVoiceRequest = voiceRequestList.get(voiceRequestList.size() - 1);
        assertThat(testVoiceRequest.getVoiceRequestText()).isEqualTo(DEFAULT_VOICE_REQUEST_TEXT);
        assertThat(testVoiceRequest.getVoiceRequestTextFormat()).isEqualTo(DEFAULT_VOICE_REQUEST_TEXT_FORMAT);
        assertThat(testVoiceRequest.isValidation()).isEqualTo(DEFAULT_VALIDATION);
    }

    @Test
    @Transactional
    public void createVoiceRequestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = voiceRequestRepository.findAll().size();

        // Create the VoiceRequest with an existing ID
        voiceRequest.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVoiceRequestMockMvc.perform(post("/api/voice-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(voiceRequest)))
            .andExpect(status().isBadRequest());

        // Validate the VoiceRequest in the database
        List<VoiceRequest> voiceRequestList = voiceRequestRepository.findAll();
        assertThat(voiceRequestList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllVoiceRequests() throws Exception {
        // Initialize the database
        voiceRequestRepository.saveAndFlush(voiceRequest);

        // Get all the voiceRequestList
        restVoiceRequestMockMvc.perform(get("/api/voice-requests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(voiceRequest.getId().intValue())))
            .andExpect(jsonPath("$.[*].voiceRequestText").value(hasItem(DEFAULT_VOICE_REQUEST_TEXT)))
            .andExpect(jsonPath("$.[*].voiceRequestTextFormat").value(hasItem(DEFAULT_VOICE_REQUEST_TEXT_FORMAT)))
            .andExpect(jsonPath("$.[*].validation").value(hasItem(DEFAULT_VALIDATION.booleanValue())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllVoiceRequestsWithEagerRelationshipsIsEnabled() throws Exception {
        when(voiceRequestServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restVoiceRequestMockMvc.perform(get("/api/voice-requests?eagerload=true"))
            .andExpect(status().isOk());

        verify(voiceRequestServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllVoiceRequestsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(voiceRequestServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restVoiceRequestMockMvc.perform(get("/api/voice-requests?eagerload=true"))
            .andExpect(status().isOk());

        verify(voiceRequestServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getVoiceRequest() throws Exception {
        // Initialize the database
        voiceRequestRepository.saveAndFlush(voiceRequest);

        // Get the voiceRequest
        restVoiceRequestMockMvc.perform(get("/api/voice-requests/{id}", voiceRequest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(voiceRequest.getId().intValue()))
            .andExpect(jsonPath("$.voiceRequestText").value(DEFAULT_VOICE_REQUEST_TEXT))
            .andExpect(jsonPath("$.voiceRequestTextFormat").value(DEFAULT_VOICE_REQUEST_TEXT_FORMAT))
            .andExpect(jsonPath("$.validation").value(DEFAULT_VALIDATION.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingVoiceRequest() throws Exception {
        // Get the voiceRequest
        restVoiceRequestMockMvc.perform(get("/api/voice-requests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVoiceRequest() throws Exception {
        // Initialize the database
        voiceRequestService.save(voiceRequest);

        int databaseSizeBeforeUpdate = voiceRequestRepository.findAll().size();

        // Update the voiceRequest
        VoiceRequest updatedVoiceRequest = voiceRequestRepository.findById(voiceRequest.getId()).get();
        // Disconnect from session so that the updates on updatedVoiceRequest are not directly saved in db
        em.detach(updatedVoiceRequest);
        updatedVoiceRequest
            .voiceRequestText(UPDATED_VOICE_REQUEST_TEXT)
            .voiceRequestTextFormat(UPDATED_VOICE_REQUEST_TEXT_FORMAT)
            .validation(UPDATED_VALIDATION);

        restVoiceRequestMockMvc.perform(put("/api/voice-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedVoiceRequest)))
            .andExpect(status().isOk());

        // Validate the VoiceRequest in the database
        List<VoiceRequest> voiceRequestList = voiceRequestRepository.findAll();
        assertThat(voiceRequestList).hasSize(databaseSizeBeforeUpdate);
        VoiceRequest testVoiceRequest = voiceRequestList.get(voiceRequestList.size() - 1);
        assertThat(testVoiceRequest.getVoiceRequestText()).isEqualTo(UPDATED_VOICE_REQUEST_TEXT);
        assertThat(testVoiceRequest.getVoiceRequestTextFormat()).isEqualTo(UPDATED_VOICE_REQUEST_TEXT_FORMAT);
        assertThat(testVoiceRequest.isValidation()).isEqualTo(UPDATED_VALIDATION);
    }

    @Test
    @Transactional
    public void updateNonExistingVoiceRequest() throws Exception {
        int databaseSizeBeforeUpdate = voiceRequestRepository.findAll().size();

        // Create the VoiceRequest

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVoiceRequestMockMvc.perform(put("/api/voice-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(voiceRequest)))
            .andExpect(status().isBadRequest());

        // Validate the VoiceRequest in the database
        List<VoiceRequest> voiceRequestList = voiceRequestRepository.findAll();
        assertThat(voiceRequestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVoiceRequest() throws Exception {
        // Initialize the database
        voiceRequestService.save(voiceRequest);

        int databaseSizeBeforeDelete = voiceRequestRepository.findAll().size();

        // Delete the voiceRequest
        restVoiceRequestMockMvc.perform(delete("/api/voice-requests/{id}", voiceRequest.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VoiceRequest> voiceRequestList = voiceRequestRepository.findAll();
        assertThat(voiceRequestList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
