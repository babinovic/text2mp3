package org.danet.web.rest;

import org.danet.Text2Mp3App;
import org.danet.domain.VoiceResponseAPI;
import org.danet.repository.VoiceResponseAPIRepository;
import org.danet.service.VoiceResponseAPIService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link VoiceResponseAPIResource} REST controller.
 */
@SpringBootTest(classes = Text2Mp3App.class)

@AutoConfigureMockMvc
@WithMockUser
public class VoiceResponseAPIResourceIT {

    private static final String DEFAULT_MP_3_PATH = "AAAAAAAAAA";
    private static final String UPDATED_MP_3_PATH = "BBBBBBBBBB";

    private static final String DEFAULT_LANGUAGE = "AAAAAAAAAA";
    private static final String UPDATED_LANGUAGE = "BBBBBBBBBB";

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_TEXT_PATH = "AAAAAAAAAA";
    private static final String UPDATED_TEXT_PATH = "BBBBBBBBBB";

    @Autowired
    private VoiceResponseAPIRepository voiceResponseAPIRepository;

    @Autowired
    private VoiceResponseAPIService voiceResponseAPIService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVoiceResponseAPIMockMvc;

    private VoiceResponseAPI voiceResponseAPI;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VoiceResponseAPI createEntity(EntityManager em) {
        VoiceResponseAPI voiceResponseAPI = new VoiceResponseAPI()
            .mp3Path(DEFAULT_MP_3_PATH)
            .language(DEFAULT_LANGUAGE)
            .text(DEFAULT_TEXT)
            .textPath(DEFAULT_TEXT_PATH);
        return voiceResponseAPI;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VoiceResponseAPI createUpdatedEntity(EntityManager em) {
        VoiceResponseAPI voiceResponseAPI = new VoiceResponseAPI()
            .mp3Path(UPDATED_MP_3_PATH)
            .language(UPDATED_LANGUAGE)
            .text(UPDATED_TEXT)
            .textPath(UPDATED_TEXT_PATH);
        return voiceResponseAPI;
    }

    @BeforeEach
    public void initTest() {
        voiceResponseAPI = createEntity(em);
    }

    @Test
    @Transactional
    public void createVoiceResponseAPI() throws Exception {
        int databaseSizeBeforeCreate = voiceResponseAPIRepository.findAll().size();

        // Create the VoiceResponseAPI
        restVoiceResponseAPIMockMvc.perform(post("/api/voice-response-apis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(voiceResponseAPI)))
            .andExpect(status().isCreated());

        // Validate the VoiceResponseAPI in the database
        List<VoiceResponseAPI> voiceResponseAPIList = voiceResponseAPIRepository.findAll();
        assertThat(voiceResponseAPIList).hasSize(databaseSizeBeforeCreate + 1);
        VoiceResponseAPI testVoiceResponseAPI = voiceResponseAPIList.get(voiceResponseAPIList.size() - 1);
        assertThat(testVoiceResponseAPI.getMp3Path()).isEqualTo(DEFAULT_MP_3_PATH);
        assertThat(testVoiceResponseAPI.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
        assertThat(testVoiceResponseAPI.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(testVoiceResponseAPI.getTextPath()).isEqualTo(DEFAULT_TEXT_PATH);
    }

    @Test
    @Transactional
    public void createVoiceResponseAPIWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = voiceResponseAPIRepository.findAll().size();

        // Create the VoiceResponseAPI with an existing ID
        voiceResponseAPI.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVoiceResponseAPIMockMvc.perform(post("/api/voice-response-apis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(voiceResponseAPI)))
            .andExpect(status().isBadRequest());

        // Validate the VoiceResponseAPI in the database
        List<VoiceResponseAPI> voiceResponseAPIList = voiceResponseAPIRepository.findAll();
        assertThat(voiceResponseAPIList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllVoiceResponseAPIS() throws Exception {
        // Initialize the database
        voiceResponseAPIRepository.saveAndFlush(voiceResponseAPI);

        // Get all the voiceResponseAPIList
        restVoiceResponseAPIMockMvc.perform(get("/api/voice-response-apis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(voiceResponseAPI.getId().intValue())))
            .andExpect(jsonPath("$.[*].mp3Path").value(hasItem(DEFAULT_MP_3_PATH)))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE)))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT)))
            .andExpect(jsonPath("$.[*].textPath").value(hasItem(DEFAULT_TEXT_PATH)));
    }
    
    @Test
    @Transactional
    public void getVoiceResponseAPI() throws Exception {
        // Initialize the database
        voiceResponseAPIRepository.saveAndFlush(voiceResponseAPI);

        // Get the voiceResponseAPI
        restVoiceResponseAPIMockMvc.perform(get("/api/voice-response-apis/{id}", voiceResponseAPI.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(voiceResponseAPI.getId().intValue()))
            .andExpect(jsonPath("$.mp3Path").value(DEFAULT_MP_3_PATH))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT))
            .andExpect(jsonPath("$.textPath").value(DEFAULT_TEXT_PATH));
    }

    @Test
    @Transactional
    public void getNonExistingVoiceResponseAPI() throws Exception {
        // Get the voiceResponseAPI
        restVoiceResponseAPIMockMvc.perform(get("/api/voice-response-apis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVoiceResponseAPI() throws Exception {
        // Initialize the database
        voiceResponseAPIService.save(voiceResponseAPI);

        int databaseSizeBeforeUpdate = voiceResponseAPIRepository.findAll().size();

        // Update the voiceResponseAPI
        VoiceResponseAPI updatedVoiceResponseAPI = voiceResponseAPIRepository.findById(voiceResponseAPI.getId()).get();
        // Disconnect from session so that the updates on updatedVoiceResponseAPI are not directly saved in db
        em.detach(updatedVoiceResponseAPI);
        updatedVoiceResponseAPI
            .mp3Path(UPDATED_MP_3_PATH)
            .language(UPDATED_LANGUAGE)
            .text(UPDATED_TEXT)
            .textPath(UPDATED_TEXT_PATH);

        restVoiceResponseAPIMockMvc.perform(put("/api/voice-response-apis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedVoiceResponseAPI)))
            .andExpect(status().isOk());

        // Validate the VoiceResponseAPI in the database
        List<VoiceResponseAPI> voiceResponseAPIList = voiceResponseAPIRepository.findAll();
        assertThat(voiceResponseAPIList).hasSize(databaseSizeBeforeUpdate);
        VoiceResponseAPI testVoiceResponseAPI = voiceResponseAPIList.get(voiceResponseAPIList.size() - 1);
        assertThat(testVoiceResponseAPI.getMp3Path()).isEqualTo(UPDATED_MP_3_PATH);
        assertThat(testVoiceResponseAPI.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testVoiceResponseAPI.getText()).isEqualTo(UPDATED_TEXT);
        assertThat(testVoiceResponseAPI.getTextPath()).isEqualTo(UPDATED_TEXT_PATH);
    }

    @Test
    @Transactional
    public void updateNonExistingVoiceResponseAPI() throws Exception {
        int databaseSizeBeforeUpdate = voiceResponseAPIRepository.findAll().size();

        // Create the VoiceResponseAPI

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVoiceResponseAPIMockMvc.perform(put("/api/voice-response-apis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(voiceResponseAPI)))
            .andExpect(status().isBadRequest());

        // Validate the VoiceResponseAPI in the database
        List<VoiceResponseAPI> voiceResponseAPIList = voiceResponseAPIRepository.findAll();
        assertThat(voiceResponseAPIList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVoiceResponseAPI() throws Exception {
        // Initialize the database
        voiceResponseAPIService.save(voiceResponseAPI);

        int databaseSizeBeforeDelete = voiceResponseAPIRepository.findAll().size();

        // Delete the voiceResponseAPI
        restVoiceResponseAPIMockMvc.perform(delete("/api/voice-response-apis/{id}", voiceResponseAPI.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VoiceResponseAPI> voiceResponseAPIList = voiceResponseAPIRepository.findAll();
        assertThat(voiceResponseAPIList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
