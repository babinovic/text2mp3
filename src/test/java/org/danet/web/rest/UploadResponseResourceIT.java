package org.danet.web.rest;

import org.danet.Text2Mp3App;
import org.danet.domain.UploadResponse;
import org.danet.repository.UploadResponseRepository;
import org.danet.service.UploadResponseService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UploadResponseResource} REST controller.
 */
@SpringBootTest(classes = Text2Mp3App.class)

@AutoConfigureMockMvc
@WithMockUser
public class UploadResponseResourceIT {

    private static final String DEFAULT_UPLOADED_FILE_PATH = "AAAAAAAAAA";
    private static final String UPDATED_UPLOADED_FILE_PATH = "BBBBBBBBBB";

    @Autowired
    private UploadResponseRepository uploadResponseRepository;

    @Autowired
    private UploadResponseService uploadResponseService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUploadResponseMockMvc;

    private UploadResponse uploadResponse;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UploadResponse createEntity(EntityManager em) {
        UploadResponse uploadResponse = new UploadResponse()
            .uploadedFilePath(DEFAULT_UPLOADED_FILE_PATH);
        return uploadResponse;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UploadResponse createUpdatedEntity(EntityManager em) {
        UploadResponse uploadResponse = new UploadResponse()
            .uploadedFilePath(UPDATED_UPLOADED_FILE_PATH);
        return uploadResponse;
    }

    @BeforeEach
    public void initTest() {
        uploadResponse = createEntity(em);
    }

    @Test
    @Transactional
    public void createUploadResponse() throws Exception {
        int databaseSizeBeforeCreate = uploadResponseRepository.findAll().size();

        // Create the UploadResponse
        restUploadResponseMockMvc.perform(post("/api/upload-responses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(uploadResponse)))
            .andExpect(status().isCreated());

        // Validate the UploadResponse in the database
        List<UploadResponse> uploadResponseList = uploadResponseRepository.findAll();
        assertThat(uploadResponseList).hasSize(databaseSizeBeforeCreate + 1);
        UploadResponse testUploadResponse = uploadResponseList.get(uploadResponseList.size() - 1);
        assertThat(testUploadResponse.getUploadedFilePath()).isEqualTo(DEFAULT_UPLOADED_FILE_PATH);
    }

    @Test
    @Transactional
    public void createUploadResponseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = uploadResponseRepository.findAll().size();

        // Create the UploadResponse with an existing ID
        uploadResponse.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUploadResponseMockMvc.perform(post("/api/upload-responses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(uploadResponse)))
            .andExpect(status().isBadRequest());

        // Validate the UploadResponse in the database
        List<UploadResponse> uploadResponseList = uploadResponseRepository.findAll();
        assertThat(uploadResponseList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUploadResponses() throws Exception {
        // Initialize the database
        uploadResponseRepository.saveAndFlush(uploadResponse);

        // Get all the uploadResponseList
        restUploadResponseMockMvc.perform(get("/api/upload-responses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(uploadResponse.getId().intValue())))
            .andExpect(jsonPath("$.[*].uploadedFilePath").value(hasItem(DEFAULT_UPLOADED_FILE_PATH)));
    }
    
    @Test
    @Transactional
    public void getUploadResponse() throws Exception {
        // Initialize the database
        uploadResponseRepository.saveAndFlush(uploadResponse);

        // Get the uploadResponse
        restUploadResponseMockMvc.perform(get("/api/upload-responses/{id}", uploadResponse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(uploadResponse.getId().intValue()))
            .andExpect(jsonPath("$.uploadedFilePath").value(DEFAULT_UPLOADED_FILE_PATH));
    }

    @Test
    @Transactional
    public void getNonExistingUploadResponse() throws Exception {
        // Get the uploadResponse
        restUploadResponseMockMvc.perform(get("/api/upload-responses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUploadResponse() throws Exception {
        // Initialize the database
        uploadResponseService.save(uploadResponse);

        int databaseSizeBeforeUpdate = uploadResponseRepository.findAll().size();

        // Update the uploadResponse
        UploadResponse updatedUploadResponse = uploadResponseRepository.findById(uploadResponse.getId()).get();
        // Disconnect from session so that the updates on updatedUploadResponse are not directly saved in db
        em.detach(updatedUploadResponse);
        updatedUploadResponse
            .uploadedFilePath(UPDATED_UPLOADED_FILE_PATH);

        restUploadResponseMockMvc.perform(put("/api/upload-responses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedUploadResponse)))
            .andExpect(status().isOk());

        // Validate the UploadResponse in the database
        List<UploadResponse> uploadResponseList = uploadResponseRepository.findAll();
        assertThat(uploadResponseList).hasSize(databaseSizeBeforeUpdate);
        UploadResponse testUploadResponse = uploadResponseList.get(uploadResponseList.size() - 1);
        assertThat(testUploadResponse.getUploadedFilePath()).isEqualTo(UPDATED_UPLOADED_FILE_PATH);
    }

    @Test
    @Transactional
    public void updateNonExistingUploadResponse() throws Exception {
        int databaseSizeBeforeUpdate = uploadResponseRepository.findAll().size();

        // Create the UploadResponse

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUploadResponseMockMvc.perform(put("/api/upload-responses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(uploadResponse)))
            .andExpect(status().isBadRequest());

        // Validate the UploadResponse in the database
        List<UploadResponse> uploadResponseList = uploadResponseRepository.findAll();
        assertThat(uploadResponseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUploadResponse() throws Exception {
        // Initialize the database
        uploadResponseService.save(uploadResponse);

        int databaseSizeBeforeDelete = uploadResponseRepository.findAll().size();

        // Delete the uploadResponse
        restUploadResponseMockMvc.perform(delete("/api/upload-responses/{id}", uploadResponse.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UploadResponse> uploadResponseList = uploadResponseRepository.findAll();
        assertThat(uploadResponseList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
