package org.danet.web.rest;

import org.danet.Text2Mp3App;
import org.danet.domain.AudioResponseAPI;
import org.danet.repository.AudioResponseAPIRepository;
import org.danet.service.AudioResponseAPIService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AudioResponseAPIResource} REST controller.
 */
@SpringBootTest(classes = Text2Mp3App.class)

@AutoConfigureMockMvc
@WithMockUser
public class AudioResponseAPIResourceIT {

    private static final String DEFAULT_FILE_PATH = "AAAAAAAAAA";
    private static final String UPDATED_FILE_PATH = "BBBBBBBBBB";

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_TEXT_PATH = "AAAAAAAAAA";
    private static final String UPDATED_TEXT_PATH = "BBBBBBBBBB";

    private static final String DEFAULT_LANGUAGE = "AAAAAAAAAA";
    private static final String UPDATED_LANGUAGE = "BBBBBBBBBB";

    @Autowired
    private AudioResponseAPIRepository audioResponseAPIRepository;

    @Autowired
    private AudioResponseAPIService audioResponseAPIService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAudioResponseAPIMockMvc;

    private AudioResponseAPI audioResponseAPI;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AudioResponseAPI createEntity(EntityManager em) {
        AudioResponseAPI audioResponseAPI = new AudioResponseAPI()
            .filePath(DEFAULT_FILE_PATH)
            .text(DEFAULT_TEXT)
            .textPath(DEFAULT_TEXT_PATH)
            .language(DEFAULT_LANGUAGE);
        return audioResponseAPI;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AudioResponseAPI createUpdatedEntity(EntityManager em) {
        AudioResponseAPI audioResponseAPI = new AudioResponseAPI()
            .filePath(UPDATED_FILE_PATH)
            .text(UPDATED_TEXT)
            .textPath(UPDATED_TEXT_PATH)
            .language(UPDATED_LANGUAGE);
        return audioResponseAPI;
    }

    @BeforeEach
    public void initTest() {
        audioResponseAPI = createEntity(em);
    }

    @Test
    @Transactional
    public void createAudioResponseAPI() throws Exception {
        int databaseSizeBeforeCreate = audioResponseAPIRepository.findAll().size();

        // Create the AudioResponseAPI
        restAudioResponseAPIMockMvc.perform(post("/api/audio-response-apis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(audioResponseAPI)))
            .andExpect(status().isCreated());

        // Validate the AudioResponseAPI in the database
        List<AudioResponseAPI> audioResponseAPIList = audioResponseAPIRepository.findAll();
        assertThat(audioResponseAPIList).hasSize(databaseSizeBeforeCreate + 1);
        AudioResponseAPI testAudioResponseAPI = audioResponseAPIList.get(audioResponseAPIList.size() - 1);
        assertThat(testAudioResponseAPI.getFilePath()).isEqualTo(DEFAULT_FILE_PATH);
        assertThat(testAudioResponseAPI.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(testAudioResponseAPI.getTextPath()).isEqualTo(DEFAULT_TEXT_PATH);
        assertThat(testAudioResponseAPI.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
    }

    @Test
    @Transactional
    public void createAudioResponseAPIWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = audioResponseAPIRepository.findAll().size();

        // Create the AudioResponseAPI with an existing ID
        audioResponseAPI.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAudioResponseAPIMockMvc.perform(post("/api/audio-response-apis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(audioResponseAPI)))
            .andExpect(status().isBadRequest());

        // Validate the AudioResponseAPI in the database
        List<AudioResponseAPI> audioResponseAPIList = audioResponseAPIRepository.findAll();
        assertThat(audioResponseAPIList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAudioResponseAPIS() throws Exception {
        // Initialize the database
        audioResponseAPIRepository.saveAndFlush(audioResponseAPI);

        // Get all the audioResponseAPIList
        restAudioResponseAPIMockMvc.perform(get("/api/audio-response-apis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(audioResponseAPI.getId().intValue())))
            .andExpect(jsonPath("$.[*].filePath").value(hasItem(DEFAULT_FILE_PATH)))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT)))
            .andExpect(jsonPath("$.[*].textPath").value(hasItem(DEFAULT_TEXT_PATH)))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE)));
    }
    
    @Test
    @Transactional
    public void getAudioResponseAPI() throws Exception {
        // Initialize the database
        audioResponseAPIRepository.saveAndFlush(audioResponseAPI);

        // Get the audioResponseAPI
        restAudioResponseAPIMockMvc.perform(get("/api/audio-response-apis/{id}", audioResponseAPI.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(audioResponseAPI.getId().intValue()))
            .andExpect(jsonPath("$.filePath").value(DEFAULT_FILE_PATH))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT))
            .andExpect(jsonPath("$.textPath").value(DEFAULT_TEXT_PATH))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE));
    }

    @Test
    @Transactional
    public void getNonExistingAudioResponseAPI() throws Exception {
        // Get the audioResponseAPI
        restAudioResponseAPIMockMvc.perform(get("/api/audio-response-apis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAudioResponseAPI() throws Exception {
        // Initialize the database
        audioResponseAPIService.save(audioResponseAPI);

        int databaseSizeBeforeUpdate = audioResponseAPIRepository.findAll().size();

        // Update the audioResponseAPI
        AudioResponseAPI updatedAudioResponseAPI = audioResponseAPIRepository.findById(audioResponseAPI.getId()).get();
        // Disconnect from session so that the updates on updatedAudioResponseAPI are not directly saved in db
        em.detach(updatedAudioResponseAPI);
        updatedAudioResponseAPI
            .filePath(UPDATED_FILE_PATH)
            .text(UPDATED_TEXT)
            .textPath(UPDATED_TEXT_PATH)
            .language(UPDATED_LANGUAGE);

        restAudioResponseAPIMockMvc.perform(put("/api/audio-response-apis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAudioResponseAPI)))
            .andExpect(status().isOk());

        // Validate the AudioResponseAPI in the database
        List<AudioResponseAPI> audioResponseAPIList = audioResponseAPIRepository.findAll();
        assertThat(audioResponseAPIList).hasSize(databaseSizeBeforeUpdate);
        AudioResponseAPI testAudioResponseAPI = audioResponseAPIList.get(audioResponseAPIList.size() - 1);
        assertThat(testAudioResponseAPI.getFilePath()).isEqualTo(UPDATED_FILE_PATH);
        assertThat(testAudioResponseAPI.getText()).isEqualTo(UPDATED_TEXT);
        assertThat(testAudioResponseAPI.getTextPath()).isEqualTo(UPDATED_TEXT_PATH);
        assertThat(testAudioResponseAPI.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingAudioResponseAPI() throws Exception {
        int databaseSizeBeforeUpdate = audioResponseAPIRepository.findAll().size();

        // Create the AudioResponseAPI

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAudioResponseAPIMockMvc.perform(put("/api/audio-response-apis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(audioResponseAPI)))
            .andExpect(status().isBadRequest());

        // Validate the AudioResponseAPI in the database
        List<AudioResponseAPI> audioResponseAPIList = audioResponseAPIRepository.findAll();
        assertThat(audioResponseAPIList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAudioResponseAPI() throws Exception {
        // Initialize the database
        audioResponseAPIService.save(audioResponseAPI);

        int databaseSizeBeforeDelete = audioResponseAPIRepository.findAll().size();

        // Delete the audioResponseAPI
        restAudioResponseAPIMockMvc.perform(delete("/api/audio-response-apis/{id}", audioResponseAPI.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AudioResponseAPI> audioResponseAPIList = audioResponseAPIRepository.findAll();
        assertThat(audioResponseAPIList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
