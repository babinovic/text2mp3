package org.danet.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.danet.web.rest.TestUtil;

public class UploadResponseTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UploadResponse.class);
        UploadResponse uploadResponse1 = new UploadResponse();
        uploadResponse1.setId(1L);
        UploadResponse uploadResponse2 = new UploadResponse();
        uploadResponse2.setId(uploadResponse1.getId());
        assertThat(uploadResponse1).isEqualTo(uploadResponse2);
        uploadResponse2.setId(2L);
        assertThat(uploadResponse1).isNotEqualTo(uploadResponse2);
        uploadResponse1.setId(null);
        assertThat(uploadResponse1).isNotEqualTo(uploadResponse2);
    }
}
