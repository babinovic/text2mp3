package org.danet.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.danet.web.rest.TestUtil;

public class VoiceStyleTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VoiceStyle.class);
        VoiceStyle voiceStyle1 = new VoiceStyle();
        voiceStyle1.setId(1L);
        VoiceStyle voiceStyle2 = new VoiceStyle();
        voiceStyle2.setId(voiceStyle1.getId());
        assertThat(voiceStyle1).isEqualTo(voiceStyle2);
        voiceStyle2.setId(2L);
        assertThat(voiceStyle1).isNotEqualTo(voiceStyle2);
        voiceStyle1.setId(null);
        assertThat(voiceStyle1).isNotEqualTo(voiceStyle2);
    }
}
