package org.danet.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.danet.web.rest.TestUtil;

public class VoiceResponseAPITest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VoiceResponseAPI.class);
        VoiceResponseAPI voiceResponseAPI1 = new VoiceResponseAPI();
        voiceResponseAPI1.setId(1L);
        VoiceResponseAPI voiceResponseAPI2 = new VoiceResponseAPI();
        voiceResponseAPI2.setId(voiceResponseAPI1.getId());
        assertThat(voiceResponseAPI1).isEqualTo(voiceResponseAPI2);
        voiceResponseAPI2.setId(2L);
        assertThat(voiceResponseAPI1).isNotEqualTo(voiceResponseAPI2);
        voiceResponseAPI1.setId(null);
        assertThat(voiceResponseAPI1).isNotEqualTo(voiceResponseAPI2);
    }
}
