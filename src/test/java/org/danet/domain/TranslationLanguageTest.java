package org.danet.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.danet.web.rest.TestUtil;

public class TranslationLanguageTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TranslationLanguage.class);
        TranslationLanguage translationLanguage1 = new TranslationLanguage();
        translationLanguage1.setId(1L);
        TranslationLanguage translationLanguage2 = new TranslationLanguage();
        translationLanguage2.setId(translationLanguage1.getId());
        assertThat(translationLanguage1).isEqualTo(translationLanguage2);
        translationLanguage2.setId(2L);
        assertThat(translationLanguage1).isNotEqualTo(translationLanguage2);
        translationLanguage1.setId(null);
        assertThat(translationLanguage1).isNotEqualTo(translationLanguage2);
    }
}
