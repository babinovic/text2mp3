package org.danet.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.danet.web.rest.TestUtil;

public class VoiceRequestTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VoiceRequest.class);
        VoiceRequest voiceRequest1 = new VoiceRequest();
        voiceRequest1.setId(1L);
        VoiceRequest voiceRequest2 = new VoiceRequest();
        voiceRequest2.setId(voiceRequest1.getId());
        assertThat(voiceRequest1).isEqualTo(voiceRequest2);
        voiceRequest2.setId(2L);
        assertThat(voiceRequest1).isNotEqualTo(voiceRequest2);
        voiceRequest1.setId(null);
        assertThat(voiceRequest1).isNotEqualTo(voiceRequest2);
    }
}
