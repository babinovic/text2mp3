package org.danet.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.danet.web.rest.TestUtil;

public class AudioResponseAPITest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AudioResponseAPI.class);
        AudioResponseAPI audioResponseAPI1 = new AudioResponseAPI();
        audioResponseAPI1.setId(1L);
        AudioResponseAPI audioResponseAPI2 = new AudioResponseAPI();
        audioResponseAPI2.setId(audioResponseAPI1.getId());
        assertThat(audioResponseAPI1).isEqualTo(audioResponseAPI2);
        audioResponseAPI2.setId(2L);
        assertThat(audioResponseAPI1).isNotEqualTo(audioResponseAPI2);
        audioResponseAPI1.setId(null);
        assertThat(audioResponseAPI1).isNotEqualTo(audioResponseAPI2);
    }
}
