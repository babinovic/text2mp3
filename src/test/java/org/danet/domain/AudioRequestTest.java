package org.danet.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.danet.web.rest.TestUtil;

public class AudioRequestTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AudioRequest.class);
        AudioRequest audioRequest1 = new AudioRequest();
        audioRequest1.setId(1L);
        AudioRequest audioRequest2 = new AudioRequest();
        audioRequest2.setId(audioRequest1.getId());
        assertThat(audioRequest1).isEqualTo(audioRequest2);
        audioRequest2.setId(2L);
        assertThat(audioRequest1).isNotEqualTo(audioRequest2);
        audioRequest1.setId(null);
        assertThat(audioRequest1).isNotEqualTo(audioRequest2);
    }
}
