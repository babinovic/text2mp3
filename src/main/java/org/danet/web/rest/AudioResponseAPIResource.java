package org.danet.web.rest;

import org.danet.domain.AudioResponseAPI;
import org.danet.service.AudioResponseAPIService;
import org.danet.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.danet.domain.AudioResponseAPI}.
 */
@RestController
@RequestMapping("/api")
public class AudioResponseAPIResource {

    private final Logger log = LoggerFactory.getLogger(AudioResponseAPIResource.class);

    private static final String ENTITY_NAME = "audioResponseAPI";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AudioResponseAPIService audioResponseAPIService;

    public AudioResponseAPIResource(AudioResponseAPIService audioResponseAPIService) {
        this.audioResponseAPIService = audioResponseAPIService;
    }

    /**
     * {@code POST  /audio-response-apis} : Create a new audioResponseAPI.
     *
     * @param audioResponseAPI the audioResponseAPI to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new audioResponseAPI, or with status {@code 400 (Bad Request)} if the audioResponseAPI has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/audio-response-apis")
    public ResponseEntity<AudioResponseAPI> createAudioResponseAPI(@RequestBody AudioResponseAPI audioResponseAPI) throws URISyntaxException {
        log.debug("REST request to save AudioResponseAPI : {}", audioResponseAPI);
        if (audioResponseAPI.getId() != null) {
            throw new BadRequestAlertException("A new audioResponseAPI cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AudioResponseAPI result = audioResponseAPIService.save(audioResponseAPI);
        return ResponseEntity.created(new URI("/api/audio-response-apis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /audio-response-apis} : Updates an existing audioResponseAPI.
     *
     * @param audioResponseAPI the audioResponseAPI to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated audioResponseAPI,
     * or with status {@code 400 (Bad Request)} if the audioResponseAPI is not valid,
     * or with status {@code 500 (Internal Server Error)} if the audioResponseAPI couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/audio-response-apis")
    public ResponseEntity<AudioResponseAPI> updateAudioResponseAPI(@RequestBody AudioResponseAPI audioResponseAPI) throws URISyntaxException {
        log.debug("REST request to update AudioResponseAPI : {}", audioResponseAPI);
        if (audioResponseAPI.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AudioResponseAPI result = audioResponseAPIService.save(audioResponseAPI);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, audioResponseAPI.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /audio-response-apis} : get all the audioResponseAPIS.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of audioResponseAPIS in body.
     */
    @GetMapping("/audio-response-apis")
    public List<AudioResponseAPI> getAllAudioResponseAPIS() {
        log.debug("REST request to get all AudioResponseAPIS");
        return audioResponseAPIService.findAll();
    }

    /**
     * {@code GET  /audio-response-apis/:id} : get the "id" audioResponseAPI.
     *
     * @param id the id of the audioResponseAPI to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the audioResponseAPI, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/audio-response-apis/{id}")
    public ResponseEntity<AudioResponseAPI> getAudioResponseAPI(@PathVariable Long id) {
        log.debug("REST request to get AudioResponseAPI : {}", id);
        Optional<AudioResponseAPI> audioResponseAPI = audioResponseAPIService.findOne(id);
        return ResponseUtil.wrapOrNotFound(audioResponseAPI);
    }

    /**
     * {@code DELETE  /audio-response-apis/:id} : delete the "id" audioResponseAPI.
     *
     * @param id the id of the audioResponseAPI to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/audio-response-apis/{id}")
    public ResponseEntity<Void> deleteAudioResponseAPI(@PathVariable Long id) {
        log.debug("REST request to delete AudioResponseAPI : {}", id);
        audioResponseAPIService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
