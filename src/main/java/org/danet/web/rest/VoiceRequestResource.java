package org.danet.web.rest;

import org.danet.domain.VoiceRequest;
import org.danet.domain.VoiceResponseAPI;
import org.danet.service.VoiceRequestService;
import org.danet.service.VoiceResponseAPIService;
import org.danet.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing {@link org.danet.domain.VoiceRequest}.
 */
@RestController
@RequestMapping("/api")
public class VoiceRequestResource {

    private final Logger log = LoggerFactory.getLogger(VoiceRequestResource.class);

    private static final String ENTITY_NAME = "voiceRequest";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VoiceRequestService voiceRequestService;
    private final VoiceResponseAPIService voiceResponseAPIService;

    public VoiceRequestResource(VoiceRequestService voiceRequestService, VoiceResponseAPIService voiceResponseAPIService) {
        this.voiceRequestService = voiceRequestService;
        this.voiceResponseAPIService = voiceResponseAPIService;
    }

    /**
     * {@code POST  /voice-requests} : Create a new voiceRequest.
     *
     * @param voiceRequest the voiceRequest to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new voiceRequest, or with status {@code 400 (Bad Request)} if the voiceRequest has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/voice-requests")
    public ResponseEntity<VoiceRequest> createVoiceRequest(@RequestBody VoiceRequest voiceRequest) throws URISyntaxException {
        log.debug("REST request to save VoiceRequest : {}", voiceRequest);
        if (voiceRequest.getId() != null) {
            throw new BadRequestAlertException("A new voiceRequest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VoiceRequest result = voiceRequestService.save(voiceRequest);
        return ResponseEntity.created(new URI("/api/voice-requests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /voice-requests} : Updates an existing voiceRequest.
     *
     * @param voiceRequest the voiceRequest to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated voiceRequest,
     * or with status {@code 400 (Bad Request)} if the voiceRequest is not valid,
     * or with status {@code 500 (Internal Server Error)} if the voiceRequest couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/voice-requests")
    public ResponseEntity<VoiceRequest> updateVoiceRequest(@RequestBody VoiceRequest voiceRequest) throws URISyntaxException {
        log.debug("REST request to update VoiceRequest : {}", voiceRequest);
        if (voiceRequest.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VoiceRequest result = voiceRequestService.save(voiceRequest);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, voiceRequest.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /voice-requests} : get all the voiceRequests.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of voiceRequests in body.
     */
    @GetMapping("/voice-requests")
    public ResponseEntity<List<VoiceRequest>> getAllVoiceRequests(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of VoiceRequests");
        Page<VoiceRequest> page;
        if (eagerload) {
            page = voiceRequestService.findAllWithEagerRelationships(pageable);
        } else {
            page = voiceRequestService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /voice-requests/:id} : get the "id" voiceRequest.
     *
     * @param id the id of the voiceRequest to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the voiceRequest, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/voice-requests/{id}")
    public ResponseEntity<VoiceRequest> getVoiceRequest(@PathVariable Long id) {
        log.debug("REST request to get VoiceRequest : {}", id);
        Optional<VoiceRequest> voiceRequest = voiceRequestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(voiceRequest);
    }

    /**
     * {@code DELETE  /voice-requests/:id} : delete the "id" voiceRequest.
     *
     * @param id the id of the voiceRequest to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/voice-requests/{id}")
    public ResponseEntity<Void> deleteVoiceRequest(@PathVariable Long id) {
        log.debug("REST request to delete VoiceRequest : {}", id);
        voiceRequestService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

     /** 
    *
    * @param voiceRequest the voiceRequest to create.
    * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the mp3 path, or with status {@code 400 (Bad Request)} if the voiceRequest has already an ID.
    * @throws Exception 
    */
    @PostMapping("/voice-request-generate")
    public ResponseEntity<Set<VoiceResponseAPI>> generateVoiceRequest(@RequestBody VoiceRequest voiceRequest) throws Exception 
    {
        log.debug("REST request to generate audio from VoiceRequest : {}", voiceRequest);
        if (voiceRequest.getId() != null) 
        {
            throw new BadRequestAlertException("A new voiceRequest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Set<VoiceResponseAPI> voiceResponseAPI = this.voiceResponseAPIService.getVoices(voiceRequest);
        
        return ResponseEntity.ok()		
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, voiceResponseAPI.iterator().next().getMp3Path()))
            .body(voiceResponseAPI);
    }
}
