package org.danet.web.rest;

import org.danet.domain.TranslationLanguage;
import org.danet.service.TranslationLanguageService;
import org.danet.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.danet.domain.TranslationLanguage}.
 */
@RestController
@RequestMapping("/api")
public class TranslationLanguageResource {

    private final Logger log = LoggerFactory.getLogger(TranslationLanguageResource.class);

    private static final String ENTITY_NAME = "translationLanguage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TranslationLanguageService translationLanguageService;

    public TranslationLanguageResource(TranslationLanguageService translationLanguageService) {
        this.translationLanguageService = translationLanguageService;
    }

    /**
     * {@code POST  /translation-languages} : Create a new translationLanguage.
     *
     * @param translationLanguage the translationLanguage to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new translationLanguage, or with status {@code 400 (Bad Request)} if the translationLanguage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/translation-languages")
    public ResponseEntity<TranslationLanguage> createTranslationLanguage(@RequestBody TranslationLanguage translationLanguage) throws URISyntaxException {
        log.debug("REST request to save TranslationLanguage : {}", translationLanguage);
        if (translationLanguage.getId() != null) {
            throw new BadRequestAlertException("A new translationLanguage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TranslationLanguage result = translationLanguageService.save(translationLanguage);
        return ResponseEntity.created(new URI("/api/translation-languages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /translation-languages} : Updates an existing translationLanguage.
     *
     * @param translationLanguage the translationLanguage to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated translationLanguage,
     * or with status {@code 400 (Bad Request)} if the translationLanguage is not valid,
     * or with status {@code 500 (Internal Server Error)} if the translationLanguage couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/translation-languages")
    public ResponseEntity<TranslationLanguage> updateTranslationLanguage(@RequestBody TranslationLanguage translationLanguage) throws URISyntaxException {
        log.debug("REST request to update TranslationLanguage : {}", translationLanguage);
        if (translationLanguage.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TranslationLanguage result = translationLanguageService.save(translationLanguage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, translationLanguage.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /translation-languages} : get all the translationLanguages.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of translationLanguages in body.
     */
    @GetMapping("/translation-languages")
    public List<TranslationLanguage> getAllTranslationLanguages() {
        log.debug("REST request to get all TranslationLanguages");
        return translationLanguageService.findAll();
    }

    /**
     * {@code GET  /translation-languages/:id} : get the "id" translationLanguage.
     *
     * @param id the id of the translationLanguage to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the translationLanguage, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/translation-languages/{id}")
    public ResponseEntity<TranslationLanguage> getTranslationLanguage(@PathVariable Long id) {
        log.debug("REST request to get TranslationLanguage : {}", id);
        Optional<TranslationLanguage> translationLanguage = translationLanguageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(translationLanguage);
    }

    /**
     * {@code DELETE  /translation-languages/:id} : delete the "id" translationLanguage.
     *
     * @param id the id of the translationLanguage to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/translation-languages/{id}")
    public ResponseEntity<Void> deleteTranslationLanguage(@PathVariable Long id) {
        log.debug("REST request to delete TranslationLanguage : {}", id);
        translationLanguageService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
