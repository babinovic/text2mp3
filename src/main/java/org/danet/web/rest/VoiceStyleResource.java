package org.danet.web.rest;

import org.danet.domain.VoiceStyle;
import org.danet.service.VoiceStyleService;
import org.danet.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.danet.domain.VoiceStyle}.
 */
@RestController
@RequestMapping("/api")
public class VoiceStyleResource {

    private final Logger log = LoggerFactory.getLogger(VoiceStyleResource.class);

    private static final String ENTITY_NAME = "voiceStyle";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VoiceStyleService voiceStyleService;

    public VoiceStyleResource(VoiceStyleService voiceStyleService) {
        this.voiceStyleService = voiceStyleService;
    }

    /**
     * {@code POST  /voice-styles} : Create a new voiceStyle.
     *
     * @param voiceStyle the voiceStyle to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new voiceStyle, or with status {@code 400 (Bad Request)} if the voiceStyle has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/voice-styles")
    public ResponseEntity<VoiceStyle> createVoiceStyle(@RequestBody VoiceStyle voiceStyle) throws URISyntaxException {
        log.debug("REST request to save VoiceStyle : {}", voiceStyle);
        if (voiceStyle.getId() != null) {
            throw new BadRequestAlertException("A new voiceStyle cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VoiceStyle result = voiceStyleService.save(voiceStyle);
        return ResponseEntity.created(new URI("/api/voice-styles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /voice-styles} : Updates an existing voiceStyle.
     *
     * @param voiceStyle the voiceStyle to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated voiceStyle,
     * or with status {@code 400 (Bad Request)} if the voiceStyle is not valid,
     * or with status {@code 500 (Internal Server Error)} if the voiceStyle couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/voice-styles")
    public ResponseEntity<VoiceStyle> updateVoiceStyle(@RequestBody VoiceStyle voiceStyle) throws URISyntaxException {
        log.debug("REST request to update VoiceStyle : {}", voiceStyle);
        if (voiceStyle.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VoiceStyle result = voiceStyleService.save(voiceStyle);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, voiceStyle.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /voice-styles} : get all the voiceStyles.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of voiceStyles in body.
     */
    @GetMapping("/voice-styles")
    public ResponseEntity<List<VoiceStyle>> getAllVoiceStyles(Pageable pageable) {
        log.debug("REST request to get a page of VoiceStyles");
        Page<VoiceStyle> page = voiceStyleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /voice-styles/:id} : get the "id" voiceStyle.
     *
     * @param id the id of the voiceStyle to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the voiceStyle, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/voice-styles/{id}")
    public ResponseEntity<VoiceStyle> getVoiceStyle(@PathVariable Long id) {
        log.debug("REST request to get VoiceStyle : {}", id);
        Optional<VoiceStyle> voiceStyle = voiceStyleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(voiceStyle);
    }

    /**
     * {@code DELETE  /voice-styles/:id} : delete the "id" voiceStyle.
     *
     * @param id the id of the voiceStyle to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/voice-styles/{id}")
    public ResponseEntity<Void> deleteVoiceStyle(@PathVariable Long id) {
        log.debug("REST request to delete VoiceStyle : {}", id);
        voiceStyleService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /voice-styles-by-language/:idLanguage} : get all the voiceStyles of specified language
     *
     * @param idLanguage the language id.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of voiceStyles in body.
     */
    @GetMapping("/voice-styles-by-language/{idLanguage}")
    public ResponseEntity<List<VoiceStyle>> getVoiceStylesByLanguage(@PathVariable Long idLanguage) {
        log.debug("REST request to get a page of VoiceStyles of language ",idLanguage);
        List<VoiceStyle> voiceStyles = voiceStyleService.findAllByLanguage(idLanguage);
        HttpHeaders headers = HeaderUtil.createAlert(applicationName,  "REST request to get a page of VoiceStyles of language " +String.valueOf(idLanguage), String.valueOf(idLanguage));
        return ResponseEntity.ok().headers(headers).body(voiceStyles);
    }
}
