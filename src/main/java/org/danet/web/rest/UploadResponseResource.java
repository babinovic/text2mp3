package org.danet.web.rest;

import org.danet.domain.UploadResponse;
import org.danet.service.UploadResponseService;
import org.danet.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.danet.domain.UploadResponse}.
 */
@RestController
@RequestMapping("/api")
public class UploadResponseResource {

    private final Logger log = LoggerFactory.getLogger(UploadResponseResource.class);

    private static final String ENTITY_NAME = "uploadResponse";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UploadResponseService uploadResponseService;

    public UploadResponseResource(UploadResponseService uploadResponseService) {
        this.uploadResponseService = uploadResponseService;
    }

    /**
     * {@code POST  /upload-responses} : Create a new uploadResponse.
     *
     * @param uploadResponse the uploadResponse to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new uploadResponse, or with status {@code 400 (Bad Request)} if the uploadResponse has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/upload-responses")
    public ResponseEntity<UploadResponse> createUploadResponse(@RequestBody UploadResponse uploadResponse) throws URISyntaxException {
        log.debug("REST request to save UploadResponse : {}", uploadResponse);
        if (uploadResponse.getId() != null) {
            throw new BadRequestAlertException("A new uploadResponse cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UploadResponse result = uploadResponseService.save(uploadResponse);
        return ResponseEntity.created(new URI("/api/upload-responses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /upload-responses} : Updates an existing uploadResponse.
     *
     * @param uploadResponse the uploadResponse to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated uploadResponse,
     * or with status {@code 400 (Bad Request)} if the uploadResponse is not valid,
     * or with status {@code 500 (Internal Server Error)} if the uploadResponse couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/upload-responses")
    public ResponseEntity<UploadResponse> updateUploadResponse(@RequestBody UploadResponse uploadResponse) throws URISyntaxException {
        log.debug("REST request to update UploadResponse : {}", uploadResponse);
        if (uploadResponse.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UploadResponse result = uploadResponseService.save(uploadResponse);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, uploadResponse.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /upload-responses} : get all the uploadResponses.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of uploadResponses in body.
     */
    @GetMapping("/upload-responses")
    public List<UploadResponse> getAllUploadResponses() {
        log.debug("REST request to get all UploadResponses");
        return uploadResponseService.findAll();
    }

    /**
     * {@code GET  /upload-responses/:id} : get the "id" uploadResponse.
     *
     * @param id the id of the uploadResponse to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the uploadResponse, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/upload-responses/{id}")
    public ResponseEntity<UploadResponse> getUploadResponse(@PathVariable Long id) {
        log.debug("REST request to get UploadResponse : {}", id);
        Optional<UploadResponse> uploadResponse = uploadResponseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(uploadResponse);
    }

    /**
     * {@code DELETE  /upload-responses/:id} : delete the "id" uploadResponse.
     *
     * @param id the id of the uploadResponse to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/upload-responses/{id}")
    public ResponseEntity<Void> deleteUploadResponse(@PathVariable Long id) {
        log.debug("REST request to delete UploadResponse : {}", id);
        uploadResponseService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
