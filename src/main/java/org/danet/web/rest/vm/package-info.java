/**
 * View Models used by Spring MVC REST controllers.
 */
package org.danet.web.rest.vm;
