package org.danet.web.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.danet.domain.AudioRequest;
import org.danet.domain.AudioResponseAPI;
import org.danet.domain.UploadResponse;
import org.danet.domain.VoiceResponseAPI;
import org.danet.service.AudioRequestService;
import org.danet.service.AudioResponseAPIService;
import org.danet.service.UploadService;
import org.danet.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link org.danet.domain.AudioRequest}.
 */
@RestController
@RequestMapping("/api")
public class AudioRequestResource {

    private final Logger log = LoggerFactory.getLogger(AudioRequestResource.class);

    private static final String ENTITY_NAME = "audioRequest";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AudioRequestService audioRequestService;

    public AudioRequestResource(AudioRequestService audioRequestService) {
        this.audioRequestService = audioRequestService;
    }

    private UploadService uploadService;

    @Autowired
    public void setUploadService(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    private AudioResponseAPIService audioResponseAPIService;

    @Autowired
    public void setUploadService(AudioResponseAPIService audioResponseAPIService) {
        this.audioResponseAPIService = audioResponseAPIService;
    }

    /**
     * {@code POST  /audio-requests} : Create a new audioRequest.
     *
     * @param audioRequest the audioRequest to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new audioRequest, or with status {@code 400 (Bad Request)}
     *         if the audioRequest has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/audio-requests")
    public ResponseEntity<AudioRequest> createAudioRequest(@RequestBody AudioRequest audioRequest)
            throws URISyntaxException {
        log.debug("REST request to save AudioRequest : {}", audioRequest);
        if (audioRequest.getId() != null) {
            throw new BadRequestAlertException("A new audioRequest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AudioRequest result = audioRequestService.save(audioRequest);
        return ResponseEntity
                .created(new URI("/api/audio-requests/" + result.getId())).headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /audio-requests} : Updates an existing audioRequest.
     *
     * @param audioRequest the audioRequest to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated audioRequest, or with status {@code 400 (Bad Request)} if
     *         the audioRequest is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the audioRequest couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/audio-requests")
    public ResponseEntity<AudioRequest> updateAudioRequest(@RequestBody AudioRequest audioRequest)
            throws URISyntaxException {
        log.debug("REST request to update AudioRequest : {}", audioRequest);
        if (audioRequest.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AudioRequest result = audioRequestService.save(audioRequest);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME,
                audioRequest.getId().toString())).body(result);
    }

    /**
     * {@code GET  /audio-requests} : get all the audioRequests.
     *
     * @param eagerload flag to eager load entities from relationships (This is
     *                  applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of audioRequests in body.
     */
    @GetMapping("/audio-requests")
    public List<AudioRequest> getAllAudioRequests(
            @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all AudioRequests");
        return audioRequestService.findAll();
    }

    /**
     * {@code GET  /audio-requests/:id} : get the "id" audioRequest.
     *
     * @param id the id of the audioRequest to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the audioRequest, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/audio-requests/{id}")
    public ResponseEntity<AudioRequest> getAudioRequest(@PathVariable Long id) {
        log.debug("REST request to get AudioRequest : {}", id);
        Optional<AudioRequest> audioRequest = audioRequestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(audioRequest);
    }

    /**
     * {@code DELETE  /audio-requests/:id} : delete the "id" audioRequest.
     *
     * @param id the id of the audioRequest to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/audio-requests/{id}")
    public ResponseEntity<Void> deleteAudioRequest(@PathVariable Long id) {
        log.debug("REST request to delete AudioRequest : {}", id);
        audioRequestService.delete(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
                .build();
    }

    @PostMapping(value = "/uploadFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UploadResponse> uploadFile(@RequestPart(value = "file") MultipartFile file) throws IOException, URISyntaxException 
    {
        // create local copy
        if (file.getOriginalFilename().isEmpty()) {
            throw new BadRequestAlertException("File invalid", ENTITY_NAME, "invalid");
        }
        UploadResponse uploadResponse = new UploadResponse();
        uploadResponse.setUploadedFilePath(this.uploadService.uploadAudioFile(file));

        return ResponseEntity.ok().headers(HeaderUtil.createAlert(applicationName, "file uploaded", ENTITY_NAME))
                .body(uploadResponse);
    }

    @PostMapping(value = "/audio-request-generate")
    public ResponseEntity<Set<VoiceResponseAPI>> getTranslatedVoices(@RequestBody AudioRequest audioRequest) throws IOException, URISyntaxException 
    {     
        Set<VoiceResponseAPI> uploadResponse = new HashSet<>();
        try 
        {
            uploadResponse = this.audioResponseAPIService.getTranslatedVoices(audioRequest);
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
         
        return ResponseEntity.ok()
        .headers(HeaderUtil.createAlert(applicationName, "file uploaded", ENTITY_NAME))
        .body(uploadResponse);
    }

    @PostMapping(value = "/audio-recognition-text")
    public ResponseEntity<AudioResponseAPI> getRecognitionText(@RequestBody AudioRequest audioRequest)
    {     
        AudioResponseAPI recogntionResponse = new AudioResponseAPI();
        try 
        {
            recogntionResponse.setText(this.audioResponseAPIService.getTextFromSpeech(audioRequest.getAudioFilePath(), audioRequest.getLanguage()));
            recogntionResponse.setLanguage(audioRequest.getLanguage().getLanguageName());
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
         
        return ResponseEntity.ok()
        .headers(HeaderUtil.createAlert(applicationName, "file uploaded", ENTITY_NAME))
        .body(recogntionResponse);
    }
}
