package org.danet.web.rest;

import org.danet.domain.VoiceResponseAPI;
import org.danet.service.VoiceResponseAPIService;
import org.danet.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing {@link org.danet.domain.VoiceResponseAPI}.
 */
@RestController
@RequestMapping("/api")
public class VoiceResponseAPIResource {

    private final Logger log = LoggerFactory.getLogger(VoiceResponseAPIResource.class);

    private static final String ENTITY_NAME = "voiceResponseAPI";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VoiceResponseAPIService voiceResponseAPIService;

    public VoiceResponseAPIResource(VoiceResponseAPIService voiceResponseAPIService) {
        this.voiceResponseAPIService = voiceResponseAPIService;
    }

    /**
     * {@code POST  /voice-response-apis} : Create a new voiceResponseAPI.
     *
     * @param voiceResponseAPI the voiceResponseAPI to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new voiceResponseAPI, or with status {@code 400 (Bad Request)} if the voiceResponseAPI has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/voice-response-apis")
    public ResponseEntity<VoiceResponseAPI> createVoiceResponseAPI(@RequestBody VoiceResponseAPI voiceResponseAPI) throws URISyntaxException {
        log.debug("REST request to save VoiceResponseAPI : {}", voiceResponseAPI);
        if (voiceResponseAPI.getId() != null) {
            throw new BadRequestAlertException("A new voiceResponseAPI cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VoiceResponseAPI result = voiceResponseAPIService.save(voiceResponseAPI);
        return ResponseEntity.created(new URI("/api/voice-response-apis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /voice-response-apis} : Updates an existing voiceResponseAPI.
     *
     * @param voiceResponseAPI the voiceResponseAPI to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated voiceResponseAPI,
     * or with status {@code 400 (Bad Request)} if the voiceResponseAPI is not valid,
     * or with status {@code 500 (Internal Server Error)} if the voiceResponseAPI couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/voice-response-apis")
    public ResponseEntity<VoiceResponseAPI> updateVoiceResponseAPI(@RequestBody VoiceResponseAPI voiceResponseAPI) throws URISyntaxException {
        log.debug("REST request to update VoiceResponseAPI : {}", voiceResponseAPI);
        if (voiceResponseAPI.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VoiceResponseAPI result = voiceResponseAPIService.save(voiceResponseAPI);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, voiceResponseAPI.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /voice-response-apis} : get all the voiceResponseAPIS.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of voiceResponseAPIS in body.
     */
    @GetMapping("/voice-response-apis")
    public ResponseEntity<List<VoiceResponseAPI>> getAllVoiceResponseAPIS(Pageable pageable) {
        log.debug("REST request to get a page of VoiceResponseAPIS");
        Page<VoiceResponseAPI> page = voiceResponseAPIService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /voice-response-apis/:id} : get the "id" voiceResponseAPI.
     *
     * @param id the id of the voiceResponseAPI to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the voiceResponseAPI, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/voice-response-apis/{id}")
    public ResponseEntity<VoiceResponseAPI> getVoiceResponseAPI(@PathVariable Long id) {
        log.debug("REST request to get VoiceResponseAPI : {}", id);
        Optional<VoiceResponseAPI> voiceResponseAPI = voiceResponseAPIService.findOne(id);
        return ResponseUtil.wrapOrNotFound(voiceResponseAPI);
    }

    /**
     * {@code DELETE  /voice-response-apis/:id} : delete the "id" voiceResponseAPI.
     *
     * @param id the id of the voiceResponseAPI to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/voice-response-apis/{id}")
    public ResponseEntity<Void> deleteVoiceResponseAPI(@PathVariable Long id) {
        log.debug("REST request to delete VoiceResponseAPI : {}", id);
        voiceResponseAPIService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code POST  /voice-response-apis} : Resume text2speech request with validated translations
     *
     * @param voiceResponseAPI the voiceResponseAPI to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new voiceResponseAPI, or with status {@code 400 (Bad Request)} if the voiceResponseAPI has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/voice-request-generate-validation")
    public ResponseEntity<Set<VoiceResponseAPI>> generateVoiceRequestAfterValidation(@RequestBody Set<VoiceResponseAPI> voiceResponseAPIList) throws Exception 
    {
        log.debug("REST request to generate VoiceResponseAPI : {} after validation");
        Set<VoiceResponseAPI> result = voiceResponseAPIService.getTranslatedVoicesAfterValidation(voiceResponseAPIList);

        return ResponseEntity.ok().headers(HeaderUtil.createAlert(applicationName, "Voices generated after validation", ENTITY_NAME))
            .body(result);    
    }


}
