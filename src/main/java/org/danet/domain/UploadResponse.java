package org.danet.domain;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A UploadResponse.
 */
@Entity
@Table(name = "upload_response")
public class UploadResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uploaded_file_path")
    private String uploadedFilePath;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUploadedFilePath() {
        return uploadedFilePath;
    }

    public UploadResponse uploadedFilePath(String uploadedFilePath) {
        this.uploadedFilePath = uploadedFilePath;
        return this;
    }

    public void setUploadedFilePath(String uploadedFilePath) {
        this.uploadedFilePath = uploadedFilePath;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UploadResponse)) {
            return false;
        }
        return id != null && id.equals(((UploadResponse) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UploadResponse{" +
            "id=" + getId() +
            ", uploadedFilePath='" + getUploadedFilePath() + "'" +
            "}";
    }
}
