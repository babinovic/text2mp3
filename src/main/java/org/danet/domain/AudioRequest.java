package org.danet.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A AudioRequest.
 */
@Entity
@Table(name = "audio_request")
public class AudioRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "audio_file_path")
    private String audioFilePath;

    @Column(name = "validation")
    private Boolean validation;

    @Column(name = "validation_recognition")
    private Boolean validationRecognition;

    @Column(name = "recognition_text")
    private String recognitionText;

    @ManyToMany
    @JoinTable(name = "audio_request_audio_translation_language",
               joinColumns = @JoinColumn(name = "audio_request_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "audio_translation_language_id", referencedColumnName = "id"))
    private Set<Language> audioTranslationLanguages = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("audioRequests")
    private Language language;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAudioFilePath() {
        return audioFilePath;
    }

    public AudioRequest audioFilePath(String audioFilePath) {
        this.audioFilePath = audioFilePath;
        return this;
    }

    public void setAudioFilePath(String audioFilePath) {
        this.audioFilePath = audioFilePath;
    }

    public Boolean isValidation() {
        return validation;
    }

    public AudioRequest validation(Boolean validation) {
        this.validation = validation;
        return this;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public Boolean isValidationRecognition() {
        return validationRecognition;
    }

    public AudioRequest validationRecognition(Boolean validationRecognition) {
        this.validationRecognition = validationRecognition;
        return this;
    }

    public void setValidationRecognition(Boolean validationRecognition) {
        this.validationRecognition = validationRecognition;
    }

    public String getRecognitionText() {
        return recognitionText;
    }

    public AudioRequest recognitionText(String recognitionText) {
        this.recognitionText = recognitionText;
        return this;
    }

    public void setRecognitionText(String recognitionText) {
        this.recognitionText = recognitionText;
    }

    public Set<Language> getAudioTranslationLanguages() {
        return audioTranslationLanguages;
    }

    public AudioRequest audioTranslationLanguages(Set<Language> languages) {
        this.audioTranslationLanguages = languages;
        return this;
    }

    public AudioRequest addAudioTranslationLanguage(Language language) {
        this.audioTranslationLanguages.add(language);
        return this;
    }

    public AudioRequest removeAudioTranslationLanguage(Language language) {
        this.audioTranslationLanguages.remove(language);
        return this;
    }

    public void setAudioTranslationLanguages(Set<Language> languages) {
        this.audioTranslationLanguages = languages;
    }

    public Language getLanguage() {
        return language;
    }

    public AudioRequest language(Language language) {
        this.language = language;
        return this;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AudioRequest)) {
            return false;
        }
        return id != null && id.equals(((AudioRequest) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AudioRequest{" +
            "id=" + getId() +
            ", audioFilePath='" + getAudioFilePath() + "'" +
            ", validation='" + isValidation() + "'" +
            ", validationRecognition='" + isValidationRecognition() + "'" +
            ", recognitionText='" + getRecognitionText() + "'" +
            "}";
    }
}
