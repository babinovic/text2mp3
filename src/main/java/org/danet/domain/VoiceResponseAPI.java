package org.danet.domain;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A VoiceResponseAPI.
 */
@Entity
@Table(name = "voice_responseapi")
public class VoiceResponseAPI implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "mp_3_path")
    private String mp3Path;

    @Column(name = "language")
    private String language;

    @Column(name = "text")
    private String text;

    @Column(name = "text_path")
    private String textPath;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMp3Path() {
        return mp3Path;
    }

    public VoiceResponseAPI mp3Path(String mp3Path) {
        this.mp3Path = mp3Path;
        return this;
    }

    public void setMp3Path(String mp3Path) {
        this.mp3Path = mp3Path;
    }

    public String getLanguage() {
        return language;
    }

    public VoiceResponseAPI language(String language) {
        this.language = language;
        return this;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public VoiceResponseAPI text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTextPath() {
        return textPath;
    }

    public VoiceResponseAPI textPath(String textPath) {
        this.textPath = textPath;
        return this;
    }

    public void setTextPath(String textPath) {
        this.textPath = textPath;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VoiceResponseAPI)) {
            return false;
        }
        return id != null && id.equals(((VoiceResponseAPI) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "VoiceResponseAPI{" +
            "id=" + getId() +
            ", mp3Path='" + getMp3Path() + "'" +
            ", language='" + getLanguage() + "'" +
            ", text='" + getText() + "'" +
            ", textPath='" + getTextPath() + "'" +
            "}";
    }
}
