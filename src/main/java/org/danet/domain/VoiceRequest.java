package org.danet.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A VoiceRequest.
 */
@Entity
@Table(name = "voice_request")
public class VoiceRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "voice_request_text")
    private String voiceRequestText;

    @Column(name = "voice_request_text_format")
    private String voiceRequestTextFormat;

    @Column(name = "validation")
    private Boolean validation;

    @ManyToOne
    @JsonIgnoreProperties("voiceRequests")
    private VoiceStyle voiceStyle;

    @ManyToMany
    @JoinTable(name = "voice_request_translation_language",
               joinColumns = @JoinColumn(name = "voice_request_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "translation_language_id", referencedColumnName = "id"))
    private Set<Language> translationLanguages = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVoiceRequestText() {
        return voiceRequestText;
    }

    public VoiceRequest voiceRequestText(String voiceRequestText) {
        this.voiceRequestText = voiceRequestText;
        return this;
    }

    public void setVoiceRequestText(String voiceRequestText) {
        this.voiceRequestText = voiceRequestText;
    }

    public String getVoiceRequestTextFormat() {
        return voiceRequestTextFormat;
    }

    public VoiceRequest voiceRequestTextFormat(String voiceRequestTextFormat) {
        this.voiceRequestTextFormat = voiceRequestTextFormat;
        return this;
    }

    public void setVoiceRequestTextFormat(String voiceRequestTextFormat) {
        this.voiceRequestTextFormat = voiceRequestTextFormat;
    }

    public Boolean isValidation() {
        return validation;
    }

    public VoiceRequest validation(Boolean validation) {
        this.validation = validation;
        return this;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public VoiceStyle getVoiceStyle() {
        return voiceStyle;
    }

    public VoiceRequest voiceStyle(VoiceStyle voiceStyle) {
        this.voiceStyle = voiceStyle;
        return this;
    }

    public void setVoiceStyle(VoiceStyle voiceStyle) {
        this.voiceStyle = voiceStyle;
    }

    public Set<Language> getTranslationLanguages() {
        return translationLanguages;
    }

    public VoiceRequest translationLanguages(Set<Language> languages) {
        this.translationLanguages = languages;
        return this;
    }

    public VoiceRequest addTranslationLanguage(Language language) {
        this.translationLanguages.add(language);
        return this;
    }

    public VoiceRequest removeTranslationLanguage(Language language) {
        this.translationLanguages.remove(language);
        return this;
    }

    public void setTranslationLanguages(Set<Language> languages) {
        this.translationLanguages = languages;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VoiceRequest)) {
            return false;
        }
        return id != null && id.equals(((VoiceRequest) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "VoiceRequest{" +
            "id=" + getId() +
            ", voiceRequestText='" + getVoiceRequestText() + "'" +
            ", voiceRequestTextFormat='" + getVoiceRequestTextFormat() + "'" +
            ", validation='" + isValidation() + "'" +
            "}";
    }
}
