package org.danet.domain;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * A TranslationLanguage.
 */
@Entity
@Table(name = "translation_language")
public class TranslationLanguage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "translation_language_name")
    private String translationLanguageName;

    @Column(name = "translation_language_code")
    private String translationLanguageCode;

    @OneToMany(mappedBy = "translationLanguage")
    private Set<Language> languages = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTranslationLanguageName() {
        return translationLanguageName;
    }

    public TranslationLanguage translationLanguageName(String translationLanguageName) {
        this.translationLanguageName = translationLanguageName;
        return this;
    }

    public void setTranslationLanguageName(String translationLanguageName) {
        this.translationLanguageName = translationLanguageName;
    }

    public String getTranslationLanguageCode() {
        return translationLanguageCode;
    }

    public TranslationLanguage translationLanguageCode(String translationLanguageCode) {
        this.translationLanguageCode = translationLanguageCode;
        return this;
    }

    public void setTranslationLanguageCode(String translationLanguageCode) {
        this.translationLanguageCode = translationLanguageCode;
    }

    public Set<Language> getLanguages() {
        return languages;
    }

    public TranslationLanguage languages(Set<Language> languages) {
        this.languages = languages;
        return this;
    }

    public TranslationLanguage addLanguage(Language language) {
        this.languages.add(language);
        language.setTranslationLanguage(this);
        return this;
    }

    public TranslationLanguage removeLanguage(Language language) {
        this.languages.remove(language);
        language.setTranslationLanguage(null);
        return this;
    }

    public void setLanguages(Set<Language> languages) {
        this.languages = languages;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TranslationLanguage)) {
            return false;
        }
        return id != null && id.equals(((TranslationLanguage) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TranslationLanguage{" +
            "id=" + getId() +
            ", translationLanguageName='" + getTranslationLanguageName() + "'" +
            ", translationLanguageCode='" + getTranslationLanguageCode() + "'" +
            "}";
    }
}
