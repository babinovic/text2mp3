package org.danet.domain;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A AudioResponseAPI.
 */
@Entity
@Table(name = "audio_responseapi")
public class AudioResponseAPI implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "file_path")
    private String filePath;

    @Column(name = "text")
    private String text;

    @Column(name = "text_path")
    private String textPath;

    @Column(name = "language")
    private String language;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public AudioResponseAPI filePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getText() {
        return text;
    }

    public AudioResponseAPI text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTextPath() {
        return textPath;
    }

    public AudioResponseAPI textPath(String textPath) {
        this.textPath = textPath;
        return this;
    }

    public void setTextPath(String textPath) {
        this.textPath = textPath;
    }

    public String getLanguage() {
        return language;
    }

    public AudioResponseAPI language(String language) {
        this.language = language;
        return this;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AudioResponseAPI)) {
            return false;
        }
        return id != null && id.equals(((AudioResponseAPI) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AudioResponseAPI{" +
            "id=" + getId() +
            ", filePath='" + getFilePath() + "'" +
            ", text='" + getText() + "'" +
            ", textPath='" + getTextPath() + "'" +
            ", language='" + getLanguage() + "'" +
            "}";
    }
}
