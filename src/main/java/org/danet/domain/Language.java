package org.danet.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Language.
 */
@Entity
@Table(name = "language")
public class Language implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "language_name")
    private String languageName;

    @Column(name = "language_code")
    private String languageCode;

    @OneToMany(mappedBy = "language")
    private Set<VoiceStyle> voiceStyles = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("languages")
    private TranslationLanguage translationLanguage;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLanguageName() {
        return languageName;
    }

    public Language languageName(String languageName) {
        this.languageName = languageName;
        return this;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public Language languageCode(String languageCode) {
        this.languageCode = languageCode;
        return this;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public Set<VoiceStyle> getVoiceStyles() {
        return voiceStyles;
    }

    public Language voiceStyles(Set<VoiceStyle> voiceStyles) {
        this.voiceStyles = voiceStyles;
        return this;
    }

    public Language addVoiceStyle(VoiceStyle voiceStyle) {
        this.voiceStyles.add(voiceStyle);
        voiceStyle.setLanguage(this);
        return this;
    }

    public Language removeVoiceStyle(VoiceStyle voiceStyle) {
        this.voiceStyles.remove(voiceStyle);
        voiceStyle.setLanguage(null);
        return this;
    }

    public void setVoiceStyles(Set<VoiceStyle> voiceStyles) {
        this.voiceStyles = voiceStyles;
    }

    public TranslationLanguage getTranslationLanguage() {
        return translationLanguage;
    }

    public Language translationLanguage(TranslationLanguage translationLanguage) {
        this.translationLanguage = translationLanguage;
        return this;
    }

    public void setTranslationLanguage(TranslationLanguage translationLanguage) {
        this.translationLanguage = translationLanguage;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Language)) {
            return false;
        }
        return id != null && id.equals(((Language) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Language{" +
            "id=" + getId() +
            ", languageName='" + getLanguageName() + "'" +
            ", languageCode='" + getLanguageCode() + "'" +
            "}";
    }
}
