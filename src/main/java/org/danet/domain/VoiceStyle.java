package org.danet.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.danet.domain.enumeration.Gender;

/**
 * A VoiceStyle.
 */
@Entity
@Table(name = "voice_style")
public class VoiceStyle implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "voice_style_name")
    private String voiceStyleName;

    @Enumerated(EnumType.STRING)
    @Column(name = "voice_style_gender")
    private Gender voiceStyleGender;

    @ManyToOne
    @JsonIgnoreProperties("voiceStyles")
    private Language language;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVoiceStyleName() {
        return voiceStyleName;
    }

    public VoiceStyle voiceStyleName(String voiceStyleName) {
        this.voiceStyleName = voiceStyleName;
        return this;
    }

    public void setVoiceStyleName(String voiceStyleName) {
        this.voiceStyleName = voiceStyleName;
    }

    public Gender getVoiceStyleGender() {
        return voiceStyleGender;
    }

    public VoiceStyle voiceStyleGender(Gender voiceStyleGender) {
        this.voiceStyleGender = voiceStyleGender;
        return this;
    }

    public void setVoiceStyleGender(Gender voiceStyleGender) {
        this.voiceStyleGender = voiceStyleGender;
    }

    public Language getLanguage() {
        return language;
    }

    public VoiceStyle language(Language language) {
        this.language = language;
        return this;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VoiceStyle)) {
            return false;
        }
        return id != null && id.equals(((VoiceStyle) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "VoiceStyle{" +
            "id=" + getId() +
            ", voiceStyleName='" + getVoiceStyleName() + "'" +
            ", voiceStyleGender='" + getVoiceStyleGender() + "'" +
            "}";
    }
}
