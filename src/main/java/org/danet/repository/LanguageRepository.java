package org.danet.repository;

import org.danet.domain.Language;
import org.danet.repository.custom.LanguageRepositoryCustom;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Language entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LanguageRepository extends JpaRepository<Language, Long>, LanguageRepositoryCustom
{

}
