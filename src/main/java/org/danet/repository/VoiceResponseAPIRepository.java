package org.danet.repository;

import org.danet.domain.VoiceResponseAPI;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the VoiceResponseAPI entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VoiceResponseAPIRepository extends JpaRepository<VoiceResponseAPI, Long> {
}
