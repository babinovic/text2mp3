package org.danet.repository;

import org.danet.domain.AudioResponseAPI;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AudioResponseAPI entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AudioResponseAPIRepository extends JpaRepository<AudioResponseAPI, Long> {
}
