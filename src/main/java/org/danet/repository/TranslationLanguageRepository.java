package org.danet.repository;

import org.danet.domain.TranslationLanguage;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TranslationLanguage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TranslationLanguageRepository extends JpaRepository<TranslationLanguage, Long> {
}
