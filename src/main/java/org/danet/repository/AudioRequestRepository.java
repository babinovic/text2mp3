package org.danet.repository;

import org.danet.domain.AudioRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the AudioRequest entity.
 */
@Repository
public interface AudioRequestRepository extends JpaRepository<AudioRequest, Long> {

    @Query(value = "select distinct audioRequest from AudioRequest audioRequest left join fetch audioRequest.audioTranslationLanguages",
        countQuery = "select count(distinct audioRequest) from AudioRequest audioRequest")
    Page<AudioRequest> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct audioRequest from AudioRequest audioRequest left join fetch audioRequest.audioTranslationLanguages")
    List<AudioRequest> findAllWithEagerRelationships();

    @Query("select audioRequest from AudioRequest audioRequest left join fetch audioRequest.audioTranslationLanguages where audioRequest.id =:id")
    Optional<AudioRequest> findOneWithEagerRelationships(@Param("id") Long id);
}
