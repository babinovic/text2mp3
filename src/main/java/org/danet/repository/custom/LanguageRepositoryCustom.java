package org.danet.repository.custom;

import org.danet.domain.Language;

public interface LanguageRepositoryCustom 
{
	Language findByLanguageCode(String languageDetectedCode);

}
