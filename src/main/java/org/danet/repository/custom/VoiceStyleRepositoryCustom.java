package org.danet.repository.custom;

import java.util.List;
import org.danet.domain.VoiceStyle;

public interface VoiceStyleRepositoryCustom 
{
	List<VoiceStyle> findAllByLanguage(Long idLanguage);

	List<VoiceStyle> findByLanguageName(String languageName);
}
