package org.danet.repository;

import java.util.List;

import org.danet.domain.VoiceStyle;
import org.danet.repository.custom.VoiceStyleRepositoryCustom;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the VoiceStyle entity.
 */
@SuppressWarnings("unused")
@Repository
 
public interface VoiceStyleRepository extends JpaRepository<VoiceStyle, Long>, VoiceStyleRepositoryCustom 
{

}
