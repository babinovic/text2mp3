package org.danet.repository.impl;

import org.danet.domain.Language;
import org.danet.repository.custom.LanguageRepositoryCustom;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional(readOnly = true)

public class LanguageRepositoryImpl implements LanguageRepositoryCustom {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public Language findByLanguageCode(String languageDetectedCode) 
    {
        Query query = entityManager.createNativeQuery("SELECT * FROM text2mp3.language l WHERE l.language_code LIKE ? LIMIT 1", Language.class);
        query.setParameter(1, "%" + languageDetectedCode + "%");

        return (Language) query.getSingleResult();
    }
	
}