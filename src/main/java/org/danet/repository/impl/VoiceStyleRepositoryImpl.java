package org.danet.repository.impl;

import org.danet.domain.VoiceStyle;
import org.danet.repository.custom.VoiceStyleRepositoryCustom;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional(readOnly = true)

public class VoiceStyleRepositoryImpl implements VoiceStyleRepositoryCustom 
{
    @PersistenceContext
    EntityManager entityManager;
    
	@Override
	public List<VoiceStyle> findAllByLanguage(Long idLanguage) 
	{
        Query query = entityManager.createNativeQuery("SELECT * FROM text2mp3.voice_style WHERE language_id = ?", VoiceStyle.class);
        query.setParameter(1, idLanguage);

        return query.getResultList();
	}

    @Override
    public List<VoiceStyle> findByLanguageName(String languageName) 
    {
        Query query = entityManager.createNativeQuery("SELECT * FROM text2mp3.voice_style v INNER JOIN text2mp3.language l ON v.language_id = l.id WHERE l.language_name = ?", VoiceStyle.class);
        query.setParameter(1, languageName);

        return query.getResultList();
    }
	
}