package org.danet.repository;

import org.danet.domain.UploadResponse;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the UploadResponse entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UploadResponseRepository extends JpaRepository<UploadResponse, Long> {
}
