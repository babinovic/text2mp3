package org.danet.repository;

import org.danet.domain.VoiceRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the VoiceRequest entity.
 */
@Repository
public interface VoiceRequestRepository extends JpaRepository<VoiceRequest, Long> {

    @Query(value = "select distinct voiceRequest from VoiceRequest voiceRequest left join fetch voiceRequest.translationLanguages",
        countQuery = "select count(distinct voiceRequest) from VoiceRequest voiceRequest")
    Page<VoiceRequest> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct voiceRequest from VoiceRequest voiceRequest left join fetch voiceRequest.translationLanguages")
    List<VoiceRequest> findAllWithEagerRelationships();

    @Query("select voiceRequest from VoiceRequest voiceRequest left join fetch voiceRequest.translationLanguages where voiceRequest.id =:id")
    Optional<VoiceRequest> findOneWithEagerRelationships(@Param("id") Long id);
}
