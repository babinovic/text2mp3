package org.danet.service.impl;

import org.danet.service.VoiceRequestService;
import org.danet.domain.VoiceRequest;
import org.danet.repository.VoiceRequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link VoiceRequest}.
 */
@Service
@Transactional
public class VoiceRequestServiceImpl implements VoiceRequestService {

    private final Logger log = LoggerFactory.getLogger(VoiceRequestServiceImpl.class);

    private final VoiceRequestRepository voiceRequestRepository;

    public VoiceRequestServiceImpl(VoiceRequestRepository voiceRequestRepository) {
        this.voiceRequestRepository = voiceRequestRepository;
    }

    /**
     * Save a voiceRequest.
     *
     * @param voiceRequest the entity to save.
     * @return the persisted entity.
     */
    @Override
    public VoiceRequest save(VoiceRequest voiceRequest) {
        log.debug("Request to save VoiceRequest : {}", voiceRequest);
        return voiceRequestRepository.save(voiceRequest);
    }

    /**
     * Get all the voiceRequests.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<VoiceRequest> findAll(Pageable pageable) {
        log.debug("Request to get all VoiceRequests");
        return voiceRequestRepository.findAll(pageable);
    }

    /**
     * Get all the voiceRequests with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<VoiceRequest> findAllWithEagerRelationships(Pageable pageable) {
        return voiceRequestRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one voiceRequest by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<VoiceRequest> findOne(Long id) {
        log.debug("Request to get VoiceRequest : {}", id);
        return voiceRequestRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the voiceRequest by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete VoiceRequest : {}", id);
        voiceRequestRepository.deleteById(id);
    }
}
