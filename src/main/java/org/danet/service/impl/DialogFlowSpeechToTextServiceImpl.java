/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.danet.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.github.trilarion.sound.vorbis.sampled.spi.VorbisAudioFileReader;

// Imports the Google Cloud client library

import com.google.cloud.dialogflow.v2.AudioEncoding;
import com.google.cloud.dialogflow.v2.DetectIntentRequest;
import com.google.cloud.dialogflow.v2.DetectIntentResponse;
import com.google.cloud.dialogflow.v2.InputAudioConfig;
import com.google.cloud.dialogflow.v2.QueryInput;
import com.google.cloud.dialogflow.v2.QueryResult;
import com.google.cloud.dialogflow.v2.SessionName;
import com.google.cloud.dialogflow.v2.SessionsClient;
import com.google.cloud.dialogflow.v2.SessionsSettings;
import com.google.protobuf.ByteString;

import org.danet.domain.Language;
import org.danet.service.DialogFlowSpeechToTextService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.threeten.bp.Duration;


/**
 * DialogFlow API Detect Intent sample with audio files.
 */
@Service
@Transactional
public class DialogFlowSpeechToTextServiceImpl implements DialogFlowSpeechToTextService {
  // [START dialogflow_detect_intent_audio]

  /**
   * Returns the result of detect intent with an audio file as input.
   *
   * Using the same `session_id` between requests allows continuation of the
   * conversation.
   *
   * @param projectId     Project/Agent Id.
   * @param audioFilePath Path to the audio file.
   * @param sessionId     Identifier of the DetectIntent session.
   * @param languageCode  Language code of the query.
   * @return QueryResult for the request.
   */
  @Override
  public QueryResult detectIntentAudio(String projectId, String audioFilePath, String sessionId, String languageCode) throws Exception 
  {
    // Instantiates a client
    SessionsSettings.Builder sessionsSettingsBuilder = SessionsSettings.newBuilder();
    sessionsSettingsBuilder.detectIntentSettings().getRetrySettings().toBuilder().setTotalTimeout(Duration.ofSeconds(600));
    SessionsSettings sessionsSettings = sessionsSettingsBuilder.build();

    try (SessionsClient sessionsClient = SessionsClient.create(sessionsSettings)) 
    {
      // Set the session name using the sessionId (UUID) and projectID (my-project-id)
      SessionName session = SessionName.of(projectId, sessionId);
      System.out.println("Session Path: " + session.toString());

      // Note: hard coding audioEncoding and sampleRateHertz for simplicity.
      // Audio encoding of the audio content sent in the query request.
      AudioEncoding audioEncoding = getAudioEncoding(audioFilePath);

      // Instructs the speech recognizer how to process the audio content.
      InputAudioConfig inputAudioConfig = InputAudioConfig.newBuilder().setAudioEncoding(audioEncoding) // audioEncoding
          .setLanguageCode(languageCode) // languageCode = "en-US"
          .setSampleRateHertz(getAudioFileSampleRate(audioFilePath))
          .build();

      // Build the query with the InputAudioConfig
      QueryInput queryInput = QueryInput.newBuilder().setAudioConfig(inputAudioConfig).build();

      // Read the bytes from the audio file
      byte[] inputAudio = Files.readAllBytes(Paths.get(audioFilePath));

      // Build the DetectIntentRequest
      DetectIntentRequest request = DetectIntentRequest.newBuilder().setSession(session.toString())
          .setQueryInput(queryInput).setInputAudio(ByteString.copyFrom(inputAudio))
          .build();

      // Performs the detect intent request
      DetectIntentResponse response = sessionsClient.detectIntent(request);

      // Display the query result
      QueryResult queryResult = response.getQueryResult();
      System.out.println("====================");
      System.out.format("Query Text: '%s'\n", queryResult.getQueryText());
      System.out.format("Detected Intent: %s (confidence: %f)\n", queryResult.getIntent().getDisplayName(),
          queryResult.getIntentDetectionConfidence());
      System.out.format("Fulfillment Text: '%s'\n", queryResult.getFulfillmentText());

      return queryResult;
    }
  }
  // [END dialogflow_detect_intent_audio]

  @Override
  public String getTextFromAudio(String fsFilePath, Language language) throws Exception {
    // Set the session name using the sessionId (UUID) and projectID (my-project-id)

    String projectId = "devtonic-186413";
    Random rand = new Random();
    String sessionId = Integer.toString(rand.nextInt((1000000000 - 1) + 1) + 1);
    QueryResult queryResult = this.detectIntentAudio(projectId, fsFilePath, sessionId, language.getLanguageCode());

    return queryResult.getQueryText();
  }

  private int getAudioFileSampleRate(String audioFilePath) throws IOException
  {
    int sampleRateHertz = 44100;
    // Detect the Sample Rate (in Khz)
    File fileWAV = new File(audioFilePath);
    
    try
    {
      AudioFileFormat aff = AudioSystem.getAudioFileFormat(fileWAV);
      sampleRateHertz = (int) aff.getFormat().getFrameRate();
    }
    catch (UnsupportedAudioFileException exception)
    {
      if (getFileType(audioFilePath).equals("FLAC"))
      {
        sampleRateHertz = 44100;
        System.out.println("Flac file - file sample rate=44100");
      }
      if (getFileType(audioFilePath).equals("OGG"))
      {
        sampleRateHertz = getVorbisAudioFileSampleRate(audioFilePath);
        System.out.println("OGG file - file sample rate="+sampleRateHertz);
      }
      System.out.println("Unsupported file format for rate detection - will use default 44100 Hz");
    }
    return sampleRateHertz;
  }

  private AudioEncoding getAudioEncoding(String audioFilePath)
  {
    AudioEncoding extension;
    switch (getFileType(audioFilePath)) 
    {
      case "FLAC":  extension = AudioEncoding.AUDIO_ENCODING_FLAC;
                    break;
      case "OPUS" :  extension = AudioEncoding.AUDIO_ENCODING_OGG_OPUS;
                    break;  
      default :     extension = AudioEncoding.AUDIO_ENCODING_LINEAR_16;     
                    break;                 
    } 

    return extension;
    
  }

  private String getFileType(String audioFilePath)
  {
    String fileName = new File(audioFilePath).getName();
    if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
    return fileName.substring(fileName.lastIndexOf(".")+1).toUpperCase();
    else return "";
  }

  private int getVorbisAudioFileSampleRate(String audioFilePath)
  {
    File fileIn = new File(audioFilePath);
    int sampleRateHertz = 44100;
    try 
    {
      AudioInputStream in=null;
      VorbisAudioFileReader vb=new VorbisAudioFileReader();
      in=vb.getAudioInputStream(fileIn);
      AudioFormat baseFormat=in.getFormat();
      sampleRateHertz = (int) baseFormat.getSampleRate();
    }
    catch (Exception exception)
    {
      System.out.println("Error during getting OGG sample rate - error="+exception.toString());
    }
    return sampleRateHertz;
  }
}
