package org.danet.service.impl;

import org.danet.service.VoiceStyleService;
import org.danet.domain.VoiceStyle;
import org.danet.repository.VoiceStyleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link VoiceStyle}.
 */
@Service
@Transactional
public class VoiceStyleServiceImpl implements VoiceStyleService {

    private final Logger log = LoggerFactory.getLogger(VoiceStyleServiceImpl.class);

    private final VoiceStyleRepository voiceStyleRepository;

    public VoiceStyleServiceImpl(VoiceStyleRepository voiceStyleRepository) {
        this.voiceStyleRepository = voiceStyleRepository;
    }

    /**
     * Save a voiceStyle.
     *
     * @param voiceStyle the entity to save.
     * @return the persisted entity.
     */
    @Override
    public VoiceStyle save(VoiceStyle voiceStyle) {
        log.debug("Request to save VoiceStyle : {}", voiceStyle);
        return voiceStyleRepository.save(voiceStyle);
    }

    /**
     * Get all the voiceStyles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<VoiceStyle> findAll(Pageable pageable) {
        log.debug("Request to get all VoiceStyles");
        return voiceStyleRepository.findAll(pageable);
    }

    /**
     * Get one voiceStyle by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<VoiceStyle> findOne(Long id) {
        log.debug("Request to get VoiceStyle : {}", id);
        return voiceStyleRepository.findById(id);
    }

    /**
     * Delete the voiceStyle by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete VoiceStyle : {}", id);
        voiceStyleRepository.deleteById(id);
    }

    /**
     * Get all the voiceStyles of the language
     *
     * @param idLanguage the language id.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<VoiceStyle> findAllByLanguage(Long idLanguage) {
        log.debug("Request to get all VoiceStyles for language ",idLanguage);
        return voiceStyleRepository.findAllByLanguage(idLanguage);
    }

    @Override
    public List<VoiceStyle> findByLanguageName(String languageName) 
    {
        return voiceStyleRepository.findByLanguageName(languageName);
    }
}
