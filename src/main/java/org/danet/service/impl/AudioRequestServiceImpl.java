package org.danet.service.impl;

import org.danet.service.AudioRequestService;
import org.danet.domain.AudioRequest;
import org.danet.repository.AudioRequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AudioRequest}.
 */
@Service
@Transactional
public class AudioRequestServiceImpl implements AudioRequestService {

    private final Logger log = LoggerFactory.getLogger(AudioRequestServiceImpl.class);

    private final AudioRequestRepository audioRequestRepository;

    public AudioRequestServiceImpl(AudioRequestRepository audioRequestRepository) {
        this.audioRequestRepository = audioRequestRepository;
    }

    /**
     * Save a audioRequest.
     *
     * @param audioRequest the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AudioRequest save(AudioRequest audioRequest) {
        log.debug("Request to save AudioRequest : {}", audioRequest);
        return audioRequestRepository.save(audioRequest);
    }

    /**
     * Get all the audioRequests.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<AudioRequest> findAll() {
        log.debug("Request to get all AudioRequests");
        return audioRequestRepository.findAllWithEagerRelationships();
    }

    /**
     * Get all the audioRequests with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<AudioRequest> findAllWithEagerRelationships(Pageable pageable) {
        return audioRequestRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one audioRequest by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AudioRequest> findOne(Long id) {
        log.debug("Request to get AudioRequest : {}", id);
        return audioRequestRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the audioRequest by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AudioRequest : {}", id);
        audioRequestRepository.deleteById(id);
    }
}
