package org.danet.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import org.danet.config.Constants;
import org.danet.service.UploadService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Service Implementation for managing {@link Language}.
 */
@Service
@Transactional
public class UploadServiceImpl implements UploadService 
{
    @Override
    public String uploadAudioFile(MultipartFile file)
    {
        String filePath = "/content/tmp/" + UUID.randomUUID().toString()+"."+ getFileType(file);
        String folderPath = this.getClass().getClassLoader().getResource(".").getFile().toString() + "static";
        String hostName = ServletUriComponentsBuilder.fromCurrentServletMapping().build().toUriString();
        if (hostName.indexOf("localhost")<0)
        {
            hostName = Constants.HOST_NAME;
        }
        String fileUrl = hostName + filePath;

        File directory = new File(folderPath +  "content/tmp");
         if (! directory.exists())
         {
             directory.mkdir();
         }
         BufferedOutputStream outputStream;
         try 
         {
            outputStream = new BufferedOutputStream(new FileOutputStream(new File(folderPath, filePath)));
            outputStream.write(file.getBytes());
            outputStream.flush();
            outputStream.close();
         } 
         catch (FileNotFoundException e) 
         {
             e.printStackTrace();
         } catch (IOException e) {
            e.printStackTrace();
         }
         
        return fileUrl;
    }

    private String getFileType(MultipartFile audioFile)
    {
        return audioFile.getOriginalFilename().split("\\.")[1];
    }
}
