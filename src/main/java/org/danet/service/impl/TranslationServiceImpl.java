package org.danet.service.impl;

import org.danet.domain.VoiceRequest;
import org.danet.service.TranslationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.cloud.translate.*;

@Service
@Transactional
public class TranslationServiceImpl implements TranslationService
{
    Translate translate = TranslateOptions.getDefaultInstance().getService();

    public String translateText(VoiceRequest voiceRequest, String inputLanguage, String outputLanguage, String format) throws Exception
    {
        Translation translation = translate.translate(voiceRequest.getVoiceRequestText(), Translate.TranslateOption.sourceLanguage(inputLanguage), Translate.TranslateOption.targetLanguage(outputLanguage), Translate.TranslateOption.format(format));
        return translation.getTranslatedText();
    }	
}