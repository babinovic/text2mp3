package org.danet.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.google.cloud.speech.v1p1beta1.RecognitionAudio;
import com.google.cloud.speech.v1p1beta1.RecognitionConfig;
import com.google.cloud.speech.v1p1beta1.RecognitionConfig.AudioEncoding;
import com.google.cloud.speech.v1p1beta1.RecognizeResponse;
import com.google.cloud.speech.v1p1beta1.SpeechClient;
import com.google.cloud.speech.v1p1beta1.SpeechRecognitionAlternative;
import com.google.cloud.speech.v1p1beta1.SpeechRecognitionResult;
import com.google.protobuf.ByteString;

import org.danet.domain.Language;
import org.danet.service.SpeechToTextService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Language}.
 */
@Service
@Transactional
public class SpeechToTextServiceImpl implements SpeechToTextService 
{
    private final Logger log = LoggerFactory.getLogger(LanguageServiceImpl.class);

    @Override
    public String getTextFromAudio(String filePath, Language language) throws Exception 
    {
        log.info("Query for speech detection of this file: " + filePath);
        String fullSpeechString = "";
        // Instantiates a client
        try (SpeechClient speechClient = SpeechClient.create()) 
        {            
            // Reads the audio file into memory
            Path path = Paths.get(filePath);
            byte[] data = Files.readAllBytes(path);
            ByteString audioBytes = ByteString.copyFrom(data);
  
            // Builds the sync recognize request
            RecognitionConfig config = RecognitionConfig.newBuilder()
                    .setEncoding(AudioEncoding.LINEAR16)
                    .setSampleRateHertz(getAudioFileSampleRate(filePath))
                    .setAudioChannelCount(getAudioFileChannels(filePath))
                    .setLanguageCode(language.getLanguageCode())
                    .build();
            RecognitionAudio audio = RecognitionAudio.newBuilder().setContent(audioBytes).build();
  
            // Performs speech recognition on the audio file
            RecognizeResponse response = speechClient.recognize(config, audio);
            List<SpeechRecognitionResult> results = response.getResultsList();
  
            for (SpeechRecognitionResult result : results) 
            {
              // There can be several alternative transcripts for a given chunk of speech. Just use the
              // first (most likely) one here.
              SpeechRecognitionAlternative alternative = result.getAlternativesList().get(0);
              System.out.printf("Transcription: %s%n", alternative.getTranscript());
              fullSpeechString += alternative.getTranscript();
            }
            System.out.printf("Full Transcription: %s%n", fullSpeechString);
            return fullSpeechString;
          }  
    }

    private int getAudioFileSampleRate(String audioFilePath) throws UnsupportedAudioFileException, IOException
    {
      // Detect the Sample Rate (in Khz)
      File fileWAV = new File(audioFilePath);
      AudioFileFormat aff = AudioSystem.getAudioFileFormat(fileWAV);
      int sampleRateHertz = (int) aff.getFormat().getFrameRate();
      return sampleRateHertz;
    }

    private int getAudioFileChannels(String audioFilePath) throws UnsupportedAudioFileException, IOException
    {
      // Detect the Sample Rate (in Khz)
      File fileWAV = new File(audioFilePath);
      AudioFileFormat aff = AudioSystem.getAudioFileFormat(fileWAV);
      int channelsCount = (int) aff.getFormat().getChannels();
      return channelsCount;
    }
}
