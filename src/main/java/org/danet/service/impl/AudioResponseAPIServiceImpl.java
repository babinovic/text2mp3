package org.danet.service.impl;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.danet.domain.AudioRequest;
import org.danet.domain.AudioResponseAPI;
import org.danet.domain.Language;
import org.danet.domain.VoiceRequest;
import org.danet.domain.VoiceResponseAPI;
import org.danet.domain.VoiceStyle;
import org.danet.repository.AudioResponseAPIRepository;
import org.danet.service.AudioResponseAPIService;
import org.danet.service.DialogFlowSpeechToTextService;
import org.danet.service.VoiceResponseAPIService;
import org.danet.service.VoiceStyleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link AudioResponseAPI}.
 */
@Service
@Transactional
public class AudioResponseAPIServiceImpl implements AudioResponseAPIService {

    private final Logger log = LoggerFactory.getLogger(AudioResponseAPIServiceImpl.class);

    private final AudioResponseAPIRepository audioResponseAPIRepository;

    public AudioResponseAPIServiceImpl(AudioResponseAPIRepository audioResponseAPIRepository) {
        this.audioResponseAPIRepository = audioResponseAPIRepository;
    }

    private VoiceResponseAPIService voiceResponseAPIService;

    @Autowired
    public void setVoiceResponseAPIService(VoiceResponseAPIService voiceResponseAPIService) {
        this.voiceResponseAPIService = voiceResponseAPIService;
    }

    private DialogFlowSpeechToTextService dialogFlowSpeechToTextService;

    @Autowired
    public void setDialogFlowSpeechToTextService(DialogFlowSpeechToTextService dialogFlowSpeechToTextService) {
        this.dialogFlowSpeechToTextService = dialogFlowSpeechToTextService;
    }

    private VoiceStyleService voiceStyleService;

    @Autowired
    public void setVoiceStyleService(VoiceStyleService voiceStyleService) {
        this.voiceStyleService = voiceStyleService;
    }

    /**
     * Save a audioResponseAPI.
     *
     * @param audioResponseAPI the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AudioResponseAPI save(AudioResponseAPI audioResponseAPI) 
    {
        log.debug("Request to save AudioResponseAPI : {}", audioResponseAPI);
        return audioResponseAPIRepository.save(audioResponseAPI);
    }

    /**
     * Get all the audioResponseAPIS.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<AudioResponseAPI> findAll() {
        log.debug("Request to get all AudioResponseAPIS");
        return audioResponseAPIRepository.findAll();
    }

    /**
     * Get one audioResponseAPI by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AudioResponseAPI> findOne(Long id) {
        log.debug("Request to get AudioResponseAPI : {}", id);
        return audioResponseAPIRepository.findById(id);
    }

    /**
     * Delete the audioResponseAPI by id
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AudioResponseAPI : {}", id);
        audioResponseAPIRepository.deleteById(id);
    }

    /**
     * Get translated voices from audio request
     *
     * @param audioRequest the audio request
     * @return the entity.
     */
    @Override
    public Set<VoiceResponseAPI> getTranslatedVoices(AudioRequest audioRequest) throws Exception {
        Set<VoiceResponseAPI> response = new HashSet<VoiceResponseAPI>();

        if (!audioRequest.getAudioFilePath().equals("")) 
        {
            String extractedText = audioRequest.getRecognitionText();
            if (extractedText.equals(""))
            {
                extractedText = getTextFromSpeech(audioRequest.getAudioFilePath(), audioRequest.getLanguage());
            }
            if (!extractedText.equals(""))
            {
                VoiceRequest createdVoiceRequest = getVoiceRequest(audioRequest, extractedText);
                response = voiceResponseAPIService.getVoices(createdVoiceRequest);
            }
        }
        return response;
    }

    /**
     * Get translated voices from audio request
     *
     * @param filePath the audio file path
     * @param language the input language
     * @return the extracted text.
     */
    @Override
    public String getTextFromSpeech(String filePath, Language language) throws Exception {
        // Google
        // return speechToTextService.getTextFromAudio(getFSFilePath(filePath),language);

        // WAV file has to be mono, conversion from stereo (future use ?)
        // filePath = manageAudioFileChannels(filePath);

        // DialogFlow
        return dialogFlowSpeechToTextService.getTextFromAudio(getFSFilePath(filePath), language);

    }

    /**
     * Transform audio request to voice request once the text is extracted
     *
     * @param filePath the audio file path
     * @param language the input language
     * @return the extracted text.
     */
    private VoiceRequest getVoiceRequest(AudioRequest audioRequest, String extractedText) 
    {
        VoiceRequest voiceRequest = new VoiceRequest();
        voiceRequest.setVoiceRequestTextFormat("text");
        voiceRequest.setVoiceRequestText(extractedText);
        voiceRequest.setTranslationLanguages(audioRequest.getAudioTranslationLanguages());
        voiceRequest.setValidation(audioRequest.isValidation());
        List<VoiceStyle> voicestyles = voiceStyleService.findAllByLanguage(audioRequest.getLanguage().getId());
        voiceRequest.setVoiceStyle(voicestyles.get(0));

        return voiceRequest;
    }

    /**
     * Transform url path into file system path
     *
     * @param filePath the url of the audio file
     * @return the path on the file system
     * @throws MalformedURLException
     */
    private String getFSFilePath(String urlFilePath) throws MalformedURLException
    {
        File file = new File(new URL(urlFilePath).getFile());
        String folderPath = this.getClass().getClassLoader().getResource(".").getFile().toString() + "static";
        int contentPathStart = file.getPath().indexOf("\\content");
        String filePath = (folderPath.substring(1) + file.getPath().substring(contentPathStart)).replace("\\", "/");
        log.debug("URL : ", urlFilePath, " - FS : ",filePath );


        return filePath;
    }
}
