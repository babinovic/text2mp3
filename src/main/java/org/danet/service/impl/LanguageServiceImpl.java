package org.danet.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.danet.domain.Language;
import org.danet.repository.LanguageRepository;
import org.danet.service.LanguageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Language}.
 */
@Service
@Transactional
public class LanguageServiceImpl implements LanguageService {

    private final Logger log = LoggerFactory.getLogger(LanguageServiceImpl.class);

    private final LanguageRepository languageRepository;

    public LanguageServiceImpl(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    /**
     * Save a language.
     *
     * @param language the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Language save(Language language) {
        log.debug("Request to save Language : {}", language);
        return languageRepository.save(language);
    }

    /**
     * Get all the languages.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Language> findAll() {
        log.debug("Request to get all Languages");
        return languageRepository.findAll();
    }

    /**
     * Get one language by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Language> findOne(Long id) {
        log.debug("Request to get Language : {}", id);
        return languageRepository.findById(id);
    }

    /**
     * Delete the language by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Language : {}", id);
        languageRepository.deleteById(id);
    }

    @Override
    public Language findByLanguageCode(String languageDetectedCode) 
    {
        //get the language from the full code (e.g. en-US)
        Language language = languageRepository.findByLanguageCode(languageDetectedCode);

        if (language == null)
        {
            language = languageRepository.findByLanguageCode(languageDetectedCode.substring(0, 2));
        }

        return language;
    }

    @Override
    public Language findDefaultLanguage(String requestLanguage) 
    {
        
        Language languageDetected = new Language();
        if (requestLanguage.length()<2)
        {
            requestLanguage = "en-GB";
        }
        else if (requestLanguage.length()>4)
        {
            languageDetected = this.findByLanguageCode(requestLanguage.substring(0,5));
        }
        else
        {
            languageDetected = this.findByLanguageCode(requestLanguage.substring(0,2));
        }
        
        return languageDetected;
    }

    @Override
    public Language findDefaultTranslationLanguage(Language voiceLanguage) 
    {
        // default translation language depending on default language (en-US, es-SP si default=en)
        if (voiceLanguage.getLanguageCode().substring(0,1) == "en")
        {
            return this.findByLanguageCode("es-ES");    
        }
        else
        {
            return this.findByLanguageCode("en-GB");  
        }
    }

    @Override
    public List<Language> findAllWithDefault(String voiceLanguage) 
    {
        Language defaultLanguage = this.findDefaultLanguage(voiceLanguage);
        Language defaultTranslationLanguage = this.findDefaultTranslationLanguage(defaultLanguage);
        List<Language> languages = this.findAll();

        // put default in first pos
        int indexDefaultLanguage = languages.indexOf(defaultLanguage);
        Collections.swap(languages, indexDefaultLanguage, 0);

         // put default translation in second pos
         int indexDefaultTranslationLanguage = languages.indexOf(defaultTranslationLanguage);
         Collections.swap(languages, indexDefaultTranslationLanguage, 1);

        return languages;
    }
}
