package org.danet.service.impl;

import org.danet.service.UploadResponseService;
import org.danet.domain.UploadResponse;
import org.danet.repository.UploadResponseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link UploadResponse}.
 */
@Service
@Transactional
public class UploadResponseServiceImpl implements UploadResponseService {

    private final Logger log = LoggerFactory.getLogger(UploadResponseServiceImpl.class);

    private final UploadResponseRepository uploadResponseRepository;

    public UploadResponseServiceImpl(UploadResponseRepository uploadResponseRepository) {
        this.uploadResponseRepository = uploadResponseRepository;
    }

    /**
     * Save a uploadResponse.
     *
     * @param uploadResponse the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UploadResponse save(UploadResponse uploadResponse) {
        log.debug("Request to save UploadResponse : {}", uploadResponse);
        return uploadResponseRepository.save(uploadResponse);
    }

    /**
     * Get all the uploadResponses.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<UploadResponse> findAll() {
        log.debug("Request to get all UploadResponses");
        return uploadResponseRepository.findAll();
    }

    /**
     * Get one uploadResponse by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UploadResponse> findOne(Long id) {
        log.debug("Request to get UploadResponse : {}", id);
        return uploadResponseRepository.findById(id);
    }

    /**
     * Delete the uploadResponse by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UploadResponse : {}", id);
        uploadResponseRepository.deleteById(id);
    }
}
