package org.danet.service.impl;

import org.danet.service.TranslationLanguageService;
import org.danet.domain.TranslationLanguage;
import org.danet.repository.TranslationLanguageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TranslationLanguage}.
 */
@Service
@Transactional
public class TranslationLanguageServiceImpl implements TranslationLanguageService {

    private final Logger log = LoggerFactory.getLogger(TranslationLanguageServiceImpl.class);

    private final TranslationLanguageRepository translationLanguageRepository;

    public TranslationLanguageServiceImpl(TranslationLanguageRepository translationLanguageRepository) {
        this.translationLanguageRepository = translationLanguageRepository;
    }

    /**
     * Save a translationLanguage.
     *
     * @param translationLanguage the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TranslationLanguage save(TranslationLanguage translationLanguage) {
        log.debug("Request to save TranslationLanguage : {}", translationLanguage);
        return translationLanguageRepository.save(translationLanguage);
    }

    /**
     * Get all the translationLanguages.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<TranslationLanguage> findAll() {
        log.debug("Request to get all TranslationLanguages");
        return translationLanguageRepository.findAll();
    }

    /**
     * Get one translationLanguage by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TranslationLanguage> findOne(Long id) {
        log.debug("Request to get TranslationLanguage : {}", id);
        return translationLanguageRepository.findById(id);
    }

    /**
     * Delete the translationLanguage by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TranslationLanguage : {}", id);
        translationLanguageRepository.deleteById(id);
    }
}
