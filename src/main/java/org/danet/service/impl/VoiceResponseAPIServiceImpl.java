package org.danet.service.impl;

import org.danet.service.SynthetizeSpeechService;
import org.danet.service.TranslationService;
import org.danet.service.VoiceResponseAPIService;
import org.danet.service.VoiceStyleService;
import org.danet.config.Constants;
import org.danet.domain.Language;
import org.danet.domain.VoiceRequest;
import org.danet.domain.VoiceResponseAPI;
import org.danet.domain.VoiceStyle;
import org.danet.repository.VoiceResponseAPIRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * Service Implementation for managing {@link VoiceResponseAPI}.
 */
@Service
@Transactional
public class VoiceResponseAPIServiceImpl implements VoiceResponseAPIService {

    private final Logger log = LoggerFactory.getLogger(VoiceResponseAPIServiceImpl.class);
    private SynthetizeSpeechService synthetizeSpeechService;

    @Autowired
    public void setSynthetizeSpeechService(SynthetizeSpeechService synthetizeSpeechService) {
        this.synthetizeSpeechService = synthetizeSpeechService;
    }

    private TranslationService translationService;

    @Autowired
    public void setTranslationService(TranslationService translationService) {
        this.translationService = translationService;
    }

    private VoiceStyleService voiceStyleService;

    @Autowired
    public void setVoiceStyleResource(VoiceStyleService voiceStyleService) {
        this.voiceStyleService = voiceStyleService;
    }

    private final VoiceResponseAPIRepository voiceResponseAPIRepository;

    public VoiceResponseAPIServiceImpl(VoiceResponseAPIRepository voiceResponseAPIRepository) {
        this.voiceResponseAPIRepository = voiceResponseAPIRepository;
    }

    /**
     * Save a voiceResponseAPI.
     *
     * @param voiceResponseAPI the entity to save.
     * @return the persisted entity.
     */
    @Override
    public VoiceResponseAPI save(VoiceResponseAPI voiceResponseAPI) {
        log.debug("Request to save VoiceResponseAPI : {}", voiceResponseAPI);
        return voiceResponseAPIRepository.save(voiceResponseAPI);
    }

    /**
     * Get all the voiceResponseAPIS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<VoiceResponseAPI> findAll(Pageable pageable) {
        log.debug("Request to get all VoiceResponseAPIS");
        return voiceResponseAPIRepository.findAll(pageable);
    }

    /**
     * Get one voiceResponseAPI by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<VoiceResponseAPI> findOne(Long id) {
        log.debug("Request to get VoiceResponseAPI : {}", id);
        return voiceResponseAPIRepository.findById(id);
    }

    /**
     * Delete the voiceResponseAPI by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete VoiceResponseAPI : {}", id);
        voiceResponseAPIRepository.deleteById(id);
    }

    /**
     * Get voice file and text from voice request
     *
     * @param voiceRequest the voice request
     * @return list of voice responses
     */
    @Override
    public Set<VoiceResponseAPI> getVoices(VoiceRequest voiceRequest) throws Exception 
    {
        Set<VoiceResponseAPI> response = new HashSet<VoiceResponseAPI>();

        // test: with or without translation
        if (voiceRequest.getTranslationLanguages().isEmpty()) 
        {
            response.add(this.getAudioVoice(voiceRequest));
        } 
        else 
        {
            response = this.getTranslatedVoices(voiceRequest);
        }
        return response;
    }

    public VoiceResponseAPI getAudioVoice(VoiceRequest voiceRequest) throws Exception 
    {
        return this.synthetizeSpeechService.synthesizeText(voiceRequest);
    }

    /**
      * Get translated voices audio files with validation management
      *
      * @param voiceRequest the voice request
      * @return list of voice responses
      * @throws Exception
      */
    private Set<VoiceResponseAPI> getTranslatedVoices(VoiceRequest voiceRequest) throws Exception 
    {
        Set<VoiceResponseAPI> response = new HashSet<VoiceResponseAPI>();

        Set<Language> translationLanguages = voiceRequest.getTranslationLanguages();

        // for each translation requested, we generate a VoiceResponseAPI
        for (Language translationLanguage : translationLanguages) 
        {
            VoiceRequest voiceGeneratedRequest = new VoiceRequest();
            voiceGeneratedRequest.setVoiceRequestText(voiceRequest.getVoiceRequestText());
            List<VoiceStyle> voiceStyles = voiceStyleService.findAllByLanguage(translationLanguage.getId());
            voiceGeneratedRequest.setVoiceStyle(voiceStyles.get(0));

            VoiceResponseAPI languageResponse = new VoiceResponseAPI();
            languageResponse.setLanguage(translationLanguage.getLanguageName());

            String inputLanguage = voiceRequest.getVoiceStyle().getLanguage().getTranslationLanguage().getTranslationLanguageCode();
            String outputLanguage = translationLanguage.getTranslationLanguage().getTranslationLanguageCode();
            String format = voiceRequest.getVoiceRequestTextFormat().toString();

            String translatedText = this.translationService.translateText(voiceGeneratedRequest, inputLanguage, outputLanguage, format);
            byte[] bytes = translatedText.getBytes(StandardCharsets.UTF_8);
            String utf8EncodedString = new String(bytes, StandardCharsets.UTF_8);
            languageResponse.setText(utf8EncodedString);
            voiceGeneratedRequest.setVoiceRequestText(translatedText);
            voiceGeneratedRequest.setValidation(voiceRequest.isValidation());
            languageResponse.setTextPath(this.createTranslatedFile(translatedText, format));
            if (!voiceRequest.isValidation())
            {    
                languageResponse.setMp3Path(this.synthetizeSpeechService.synthesizeText(voiceGeneratedRequest).getMp3Path());
            }
            else
            {
                languageResponse.setMp3Path("");
            }
            response.add(languageResponse);
        }
        return response;
    }

    /**
      * Get translated voices audio files after validation
      *
      * @param voiceRequest the voice request
      * @return list of voice responses
      * @throws Exception
      */
      public Set<VoiceResponseAPI> getTranslatedVoicesAfterValidation(Set<VoiceResponseAPI> voiceResponseAPIList)
      {
          Set<VoiceResponseAPI> response = voiceResponseAPIList;
  
          // for each translation requested, we generate a VoiceResponseAPI
          for (VoiceResponseAPI voiceResponseAPI : voiceResponseAPIList) 
          {
            
            String translatedText = voiceResponseAPI.getText();
            
            String format = "text";
            if (voiceResponseAPI.getTextPath().substring(voiceResponseAPI.getTextPath().length()-4)=="html")
            {
                format = "html";
            }
            try 
            {
                voiceResponseAPI.setTextPath(this.createTranslatedFile(translatedText, format));
                VoiceRequest voiceGeneratedRequest = new VoiceRequest();
                voiceGeneratedRequest.setVoiceRequestText(translatedText);
                List<VoiceStyle> voiceStyles = voiceStyleService.findByLanguageName(voiceResponseAPI.getLanguage());
                voiceGeneratedRequest.setVoiceStyle(voiceStyles.get(0));

                voiceResponseAPI.setMp3Path(this.synthetizeSpeechService.synthesizeText(voiceGeneratedRequest).getMp3Path());
            } 
            catch (Exception e) 
            {
                e.printStackTrace();
            }

            
            response.add(voiceResponseAPI);
          }
          return response;
      }
  

    /**
     * Creates a text file and append translated text.
     *
     * @param id the id of the entity.
     * @throws IOException
     */
    public String createTranslatedFile(String content, String format) throws IOException
    {
         // Write the translated text to the output file.
        String extension = format.equals("text") ? ".txt" : ".html";
        String filePath = "/content/tmp/" + UUID.randomUUID().toString()+extension;        
        String folderPath = this.getClass().getClassLoader().getResource(".").getFile().toString() + "static";
        String hostName = ServletUriComponentsBuilder.fromCurrentServletMapping().build().toUriString();
        if (hostName.indexOf("localhost")<0)
        {
            hostName = Constants.HOST_NAME;
        }
        String fileUrl = hostName + filePath;
        
        File directory = new File(folderPath +  "content/tmp");
        if (! directory.exists())
        {
            directory.mkdir();
        }
        String fileFullPath = folderPath + filePath;
        System.out.println("Chemin fichier content : "+fileFullPath);
        File file = new File(fileFullPath);
        if (file.createNewFile()) 
        {
            System.out.println("File is created!");
        } 
        else 
        {
            System.out.println("File not created");
        }
        try (OutputStream out = new FileOutputStream(file)) 
        {
            out.write(content.getBytes("UTF-8"));
            System.out.println("Translated text written to file " + fileUrl);
            return fileUrl;
        }
    }
}
