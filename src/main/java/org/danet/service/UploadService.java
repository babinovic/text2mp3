package org.danet.service;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

/**
 * Service Interface for managing {@link Language}.
 */
public interface UploadService 
{

   /**
     * Creates a local copy of the uploaded file and return its url
     *
     * @param file the uploaded file
 * @throws FileNotFoundException
 * @throws IOException
     */
    String uploadAudioFile(MultipartFile file);
    
}
