package org.danet.service;

import java.util.List;
import java.util.Optional;

import org.danet.domain.Language;

/**
 * Service Interface for managing {@link Language}.
 */
public interface LanguageService {

    /**
     * Save a language.
     *
     * @param language the entity to save.
     * @return the persisted entity.
     */
    Language save(Language language);

    /**
     * Get all the languages.
     *
     * @return the list of entities.
     */
    List<Language> findAll();

    /**
     * Get the "id" language.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Language> findOne(Long id);

    /**
     * Delete the "id" language.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Selects a language depending on the Locale language of HttpServletRequest
     *
     * @param languageDetectedCode the language code
     */
    Language findByLanguageCode(String languageDetectedCode);

    /**
     * Selects a language depending on the Locale language of HttpServletRequest
     *
     * @param requestLanguage the language code
     */
    Language findDefaultLanguage(String requestLanguage);

    /**
     * Selects a translation language depending on voice language
     *
     * @param voiceLanguage the language code
     */
    Language findDefaultTranslationLanguage(Language voiceLanguage);

    /**
     * Selects a translation language depending on voice language
     *
     * @param voiceLanguage the language code
     */
    List<Language> findAllWithDefault(String voiceLanguage);
    
}
