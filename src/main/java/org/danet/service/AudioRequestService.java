package org.danet.service;

import org.danet.domain.AudioRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link AudioRequest}.
 */
public interface AudioRequestService {

    /**
     * Save a audioRequest.
     *
     * @param audioRequest the entity to save.
     * @return the persisted entity.
     */
    AudioRequest save(AudioRequest audioRequest);

    /**
     * Get all the audioRequests.
     *
     * @return the list of entities.
     */
    List<AudioRequest> findAll();

    /**
     * Get all the audioRequests with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<AudioRequest> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" audioRequest.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AudioRequest> findOne(Long id);

    /**
     * Delete the "id" audioRequest.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
