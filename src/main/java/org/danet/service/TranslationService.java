package org.danet.service;

import org.danet.domain.VoiceRequest;

/**
 * Service Interface for translation
 */
public interface TranslationService 
{
	 String translateText(VoiceRequest voiceRequest, String inputLanguage, String outputLanguage, String format) throws Exception; 	 
}