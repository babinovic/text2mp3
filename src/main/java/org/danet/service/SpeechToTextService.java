package org.danet.service;

import org.danet.domain.Language;

public interface SpeechToTextService 
{
	 String getTextFromAudio(String filePath, Language language) throws Exception;
}
