package org.danet.service;

import org.danet.domain.VoiceRequest;
import org.danet.domain.VoiceResponseAPI;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;

/**
 * Service Interface for managing {@link VoiceResponseAPI}.
 */
public interface VoiceResponseAPIService {

    /**
     * Save a voiceResponseAPI.
     *
     * @param voiceResponseAPI the entity to save.
     * @return the persisted entity.
     */
    VoiceResponseAPI save(VoiceResponseAPI voiceResponseAPI);

    /**
     * Get all the voiceResponseAPIS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<VoiceResponseAPI> findAll(Pageable pageable);

    /**
     * Get the "id" voiceResponseAPI.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<VoiceResponseAPI> findOne(Long id);

    /**
     * Delete the "id" voiceResponseAPI.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Provides mp3 path once audio received
     *
     * @param voiceRequest the request coming from angular
     */
    VoiceResponseAPI getAudioVoice(VoiceRequest voiceRequest) throws Exception;

    /**
     * Provides mp3 files path and translated texts
     *
     * @param voiceRequest the request coming from angular
     */
    Set<VoiceResponseAPI> getVoices(VoiceRequest voiceRequest) throws Exception;

    /**
     * creates file from translated text (in text or html)
     *
     * @param content text to be inserted in file
     * @param format file format (text or html)
     */    
    String createTranslatedFile(String content, String format) throws IOException;

	Set<VoiceResponseAPI> getTranslatedVoicesAfterValidation(Set<VoiceResponseAPI> voiceResponseAPIList) throws Exception;
}
