package org.danet.service;

import org.danet.domain.UploadResponse;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link UploadResponse}.
 */
public interface UploadResponseService {

    /**
     * Save a uploadResponse.
     *
     * @param uploadResponse the entity to save.
     * @return the persisted entity.
     */
    UploadResponse save(UploadResponse uploadResponse);

    /**
     * Get all the uploadResponses.
     *
     * @return the list of entities.
     */
    List<UploadResponse> findAll();

    /**
     * Get the "id" uploadResponse.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UploadResponse> findOne(Long id);

    /**
     * Delete the "id" uploadResponse.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
