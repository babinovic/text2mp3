package org.danet.service;

import com.google.cloud.dialogflow.v2.QueryResult;

import org.danet.domain.Language;

public interface DialogFlowSpeechToTextService 
{
    QueryResult detectIntentAudio(String projectId, String audioFilePath, String sessionId, String languageCode) throws Exception;

	String getTextFromAudio(String fsFilePath, Language language) throws Exception;
    
}
