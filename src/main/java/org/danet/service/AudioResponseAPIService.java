package org.danet.service;

import org.danet.domain.AudioRequest;
import org.danet.domain.AudioResponseAPI;
import org.danet.domain.Language;
import org.danet.domain.VoiceResponseAPI;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Service Interface for managing {@link AudioResponseAPI}.
 */
public interface AudioResponseAPIService {

    /**
     * Save a audioResponseAPI.
     *
     * @param audioResponseAPI the entity to save.
     * @return the persisted entity.
     */
    AudioResponseAPI save(AudioResponseAPI audioResponseAPI);

    /**
     * Get all the audioResponseAPIS.
     *
     * @return the list of entities.
     */
    List<AudioResponseAPI> findAll();

    /**
     * Get the "id" audioResponseAPI.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AudioResponseAPI> findOne(Long id);

    /**
     * Delete the "id" audioResponseAPI.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Provides wav files path and translated text for each language
     *
     * @param audioRequest the request coming from angular
     */
    Set<VoiceResponseAPI> getTranslatedVoices(AudioRequest audioRequest) throws Exception;

    /**
     * Provides text extracted from speech
     *
     * @param filePath the recorded or uploaded file
     * @param language the input language
     */
    String getTextFromSpeech(String filePath, Language language) throws Exception;
}
