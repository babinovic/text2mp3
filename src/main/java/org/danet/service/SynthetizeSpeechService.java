package org.danet.service;

import org.danet.domain.VoiceRequest;
import org.danet.domain.VoiceResponseAPI;

import com.google.protobuf.ByteString;

public interface SynthetizeSpeechService 
{
	 VoiceResponseAPI synthesizeText(VoiceRequest voiceRequest) throws Exception;
	 
	 ByteString synthesizeTextWithAudioProfile(String text, String effectsProfile) throws Exception;
	 
	 ByteString synthesizeSsml(String ssml) throws Exception;
	 
}
