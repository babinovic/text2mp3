package org.danet.service;

import org.danet.domain.VoiceStyle;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link VoiceStyle}.
 */
public interface VoiceStyleService {

    /**
     * Save a voiceStyle.
     *
     * @param voiceStyle the entity to save.
     * @return the persisted entity.
     */
    VoiceStyle save(VoiceStyle voiceStyle);

    /**
     * Get all the voiceStyles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<VoiceStyle> findAll(Pageable pageable);

    /**
     * Get the "id" voiceStyle.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<VoiceStyle> findOne(Long id);

    /**
     * Delete the "id" voiceStyle.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

     /**
     * Get all the voiceStyles of one language
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */

	List<VoiceStyle> findAllByLanguage(Long idLanguage);

     /**
     * Get all the voiceStyles of one language by name
     *
     * @param language the language name.
     * @return one entity
     */
	List<VoiceStyle> findByLanguageName(String language);
}
