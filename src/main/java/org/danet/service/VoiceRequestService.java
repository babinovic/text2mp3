package org.danet.service;

import org.danet.domain.VoiceRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link VoiceRequest}.
 */
public interface VoiceRequestService {

    /**
     * Save a voiceRequest.
     *
     * @param voiceRequest the entity to save.
     * @return the persisted entity.
     */
    VoiceRequest save(VoiceRequest voiceRequest);

    /**
     * Get all the voiceRequests.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<VoiceRequest> findAll(Pageable pageable);

    /**
     * Get all the voiceRequests with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<VoiceRequest> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" voiceRequest.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<VoiceRequest> findOne(Long id);

    /**
     * Delete the "id" voiceRequest.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
