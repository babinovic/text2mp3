package org.danet.service;

import org.danet.domain.TranslationLanguage;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link TranslationLanguage}.
 */
public interface TranslationLanguageService {

    /**
     * Save a translationLanguage.
     *
     * @param translationLanguage the entity to save.
     * @return the persisted entity.
     */
    TranslationLanguage save(TranslationLanguage translationLanguage);

    /**
     * Get all the translationLanguages.
     *
     * @return the list of entities.
     */
    List<TranslationLanguage> findAll();

    /**
     * Get the "id" translationLanguage.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TranslationLanguage> findOne(Long id);

    /**
     * Delete the "id" translationLanguage.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
