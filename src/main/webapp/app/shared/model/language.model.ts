import { IVoiceStyle } from 'app/shared/model/voice-style.model';
import { ITranslationLanguage } from 'app/shared/model/translation-language.model';

export interface ILanguage {
  id?: number;
  languageName?: string;
  languageCode?: string;
  voiceStyles?: IVoiceStyle[];
  translationLanguage?: ITranslationLanguage;
}

export class Language implements ILanguage {
  constructor(
    public id?: number,
    public languageName?: string,
    public languageCode?: string,
    public voiceStyles?: IVoiceStyle[],
    public translationLanguage?: ITranslationLanguage
  ) {}
}
