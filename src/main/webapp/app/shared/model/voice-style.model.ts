import { ILanguage } from 'app/shared/model/language.model';
import { Gender } from 'app/shared/model/enumerations/gender.model';

export interface IVoiceStyle {
  id?: number;
  voiceStyleName?: string;
  voiceStyleGender?: Gender;
  language?: ILanguage;
}

export class VoiceStyle implements IVoiceStyle {
  constructor(public id?: number, public voiceStyleName?: string, public voiceStyleGender?: Gender, public language?: ILanguage) {}
}
