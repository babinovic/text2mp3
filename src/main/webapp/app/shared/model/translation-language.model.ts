import { ILanguage } from 'app/shared/model/language.model';

export interface ITranslationLanguage {
  id?: number;
  translationLanguageName?: string;
  translationLanguageCode?: string;
  languages?: ILanguage[];
}

export class TranslationLanguage implements ITranslationLanguage {
  constructor(
    public id?: number,
    public translationLanguageName?: string,
    public translationLanguageCode?: string,
    public languages?: ILanguage[]
  ) {}
}
