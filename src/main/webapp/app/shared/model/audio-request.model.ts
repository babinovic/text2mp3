import { ILanguage } from 'app/shared/model/language.model';

export interface IAudioRequest {
  id?: number;
  audioFilePath?: string;
  validation?: boolean;
  validationRecognition?: boolean;
  recognitionText?: string;
  audioTranslationLanguages?: ILanguage[];
  language?: ILanguage;
}

export class AudioRequest implements IAudioRequest {
  constructor(
    public id?: number,
    public audioFilePath?: string,
    public validation?: boolean,
    public validationRecognition?: boolean,
    public recognitionText?: string,
    public audioTranslationLanguages?: ILanguage[],
    public language?: ILanguage
  ) {
    this.validation = this.validation || false;
    this.validationRecognition = this.validationRecognition || false;
  }
}
