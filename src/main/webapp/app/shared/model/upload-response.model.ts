export interface IUploadResponse {
  id?: number;
  uploadedFilePath?: string;
}

export class UploadResponse implements IUploadResponse {
  constructor(public id?: number, public uploadedFilePath?: string) {}
}
