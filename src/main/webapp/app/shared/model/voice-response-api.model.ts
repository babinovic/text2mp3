export interface IVoiceResponseAPI {
  id?: number;
  mp3Path?: string;
  language?: string;
  text?: string;
  textPath?: string;
}

export class VoiceResponseAPI implements IVoiceResponseAPI {
  constructor(public id?: number, public mp3Path?: string, public language?: string, public text?: string, public textPath?: string) {}
}
