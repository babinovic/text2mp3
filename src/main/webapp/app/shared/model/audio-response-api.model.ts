export interface IAudioResponseAPI {
  id?: number;
  filePath?: string;
  text?: string;
  textPath?: string;
  language?: string;
}

export class AudioResponseAPI implements IAudioResponseAPI {
  constructor(public id?: number, public filePath?: string, public text?: string, public textPath?: string, public language?: string) {}
}
