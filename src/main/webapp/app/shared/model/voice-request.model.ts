import { IVoiceStyle } from 'app/shared/model/voice-style.model';
import { ILanguage } from 'app/shared/model/language.model';

export interface IVoiceRequest {
  id?: number;
  voiceRequestText?: string;
  voiceRequestTextFormat?: string;
  validation?: boolean;
  voiceStyle?: IVoiceStyle;
  translationLanguages?: ILanguage[];
}

export class VoiceRequest implements IVoiceRequest {
  constructor(
    public id?: number,
    public voiceRequestText?: string,
    public voiceRequestTextFormat?: string,
    public validation?: boolean,
    public voiceStyle?: IVoiceStyle,
    public translationLanguages?: ILanguage[]
  ) {
    this.validation = this.validation || false;
  }
}
