import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAudioRequest } from 'app/shared/model/audio-request.model';

type EntityResponseType = HttpResponse<IAudioRequest>;
type EntityArrayResponseType = HttpResponse<IAudioRequest[]>;

@Injectable({ providedIn: 'root' })
export class AudioRequestService {
  public resourceUrl = SERVER_API_URL + 'api/audio-requests';

  constructor(protected http: HttpClient) {}

  create(audioRequest: IAudioRequest): Observable<EntityResponseType> {
    return this.http.post<IAudioRequest>(this.resourceUrl, audioRequest, { observe: 'response' });
  }

  update(audioRequest: IAudioRequest): Observable<EntityResponseType> {
    return this.http.put<IAudioRequest>(this.resourceUrl, audioRequest, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAudioRequest>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAudioRequest[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
