import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAudioRequest } from 'app/shared/model/audio-request.model';

@Component({
  selector: 'jhi-audio-request-detail',
  templateUrl: './audio-request-detail.component.html'
})
export class AudioRequestDetailComponent implements OnInit {
  audioRequest: IAudioRequest | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ audioRequest }) => (this.audioRequest = audioRequest));
  }

  previousState(): void {
    window.history.back();
  }
}
