import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAudioRequest, AudioRequest } from 'app/shared/model/audio-request.model';
import { AudioRequestService } from './audio-request.service';
import { AudioRequestComponent } from './audio-request.component';
import { AudioRequestDetailComponent } from './audio-request-detail.component';
import { AudioRequestUpdateComponent } from './audio-request-update.component';

@Injectable({ providedIn: 'root' })
export class AudioRequestResolve implements Resolve<IAudioRequest> {
  constructor(private service: AudioRequestService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAudioRequest> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((audioRequest: HttpResponse<AudioRequest>) => {
          if (audioRequest.body) {
            return of(audioRequest.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AudioRequest());
  }
}

export const audioRequestRoute: Routes = [
  {
    path: '',
    component: AudioRequestComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.audioRequest.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AudioRequestDetailComponent,
    resolve: {
      audioRequest: AudioRequestResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.audioRequest.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AudioRequestUpdateComponent,
    resolve: {
      audioRequest: AudioRequestResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.audioRequest.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AudioRequestUpdateComponent,
    resolve: {
      audioRequest: AudioRequestResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.audioRequest.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
