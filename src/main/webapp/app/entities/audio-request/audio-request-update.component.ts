import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAudioRequest, AudioRequest } from 'app/shared/model/audio-request.model';
import { AudioRequestService } from './audio-request.service';
import { ILanguage } from 'app/shared/model/language.model';
import { LanguageService } from 'app/entities/language/language.service';

@Component({
  selector: 'jhi-audio-request-update',
  templateUrl: './audio-request-update.component.html'
})
export class AudioRequestUpdateComponent implements OnInit {
  isSaving = false;
  languages: ILanguage[] = [];

  editForm = this.fb.group({
    id: [],
    audioFilePath: [],
    validation: [],
    validationRecognition: [],
    recognitionText: [],
    audioTranslationLanguages: [],
    language: []
  });

  constructor(
    protected audioRequestService: AudioRequestService,
    protected languageService: LanguageService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ audioRequest }) => {
      this.updateForm(audioRequest);

      this.languageService.query().subscribe((res: HttpResponse<ILanguage[]>) => (this.languages = res.body || []));
    });
  }

  updateForm(audioRequest: IAudioRequest): void {
    this.editForm.patchValue({
      id: audioRequest.id,
      audioFilePath: audioRequest.audioFilePath,
      validation: audioRequest.validation,
      validationRecognition: audioRequest.validationRecognition,
      recognitionText: audioRequest.recognitionText,
      audioTranslationLanguages: audioRequest.audioTranslationLanguages,
      language: audioRequest.language
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const audioRequest = this.createFromForm();
    if (audioRequest.id !== undefined) {
      this.subscribeToSaveResponse(this.audioRequestService.update(audioRequest));
    } else {
      this.subscribeToSaveResponse(this.audioRequestService.create(audioRequest));
    }
  }

  private createFromForm(): IAudioRequest {
    return {
      ...new AudioRequest(),
      id: this.editForm.get(['id'])!.value,
      audioFilePath: this.editForm.get(['audioFilePath'])!.value,
      validation: this.editForm.get(['validation'])!.value,
      validationRecognition: this.editForm.get(['validationRecognition'])!.value,
      recognitionText: this.editForm.get(['recognitionText'])!.value,
      audioTranslationLanguages: this.editForm.get(['audioTranslationLanguages'])!.value,
      language: this.editForm.get(['language'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAudioRequest>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ILanguage): any {
    return item.id;
  }

  getSelected(selectedVals: ILanguage[], option: ILanguage): ILanguage {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
