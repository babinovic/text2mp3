import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAudioRequest } from 'app/shared/model/audio-request.model';
import { AudioRequestService } from './audio-request.service';

@Component({
  templateUrl: './audio-request-delete-dialog.component.html'
})
export class AudioRequestDeleteDialogComponent {
  audioRequest?: IAudioRequest;

  constructor(
    protected audioRequestService: AudioRequestService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.audioRequestService.delete(id).subscribe(() => {
      this.eventManager.broadcast('audioRequestListModification');
      this.activeModal.close();
    });
  }
}
