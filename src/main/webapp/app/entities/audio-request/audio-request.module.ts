import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Text2Mp3SharedModule } from 'app/shared/shared.module';
import { AudioRequestComponent } from './audio-request.component';
import { AudioRequestDetailComponent } from './audio-request-detail.component';
import { AudioRequestUpdateComponent } from './audio-request-update.component';
import { AudioRequestDeleteDialogComponent } from './audio-request-delete-dialog.component';
import { audioRequestRoute } from './audio-request.route';

@NgModule({
  imports: [Text2Mp3SharedModule, RouterModule.forChild(audioRequestRoute)],
  declarations: [AudioRequestComponent, AudioRequestDetailComponent, AudioRequestUpdateComponent, AudioRequestDeleteDialogComponent],
  entryComponents: [AudioRequestDeleteDialogComponent]
})
export class Text2Mp3AudioRequestModule {}
