import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAudioRequest } from 'app/shared/model/audio-request.model';
import { AudioRequestService } from './audio-request.service';
import { AudioRequestDeleteDialogComponent } from './audio-request-delete-dialog.component';

@Component({
  selector: 'jhi-audio-request',
  templateUrl: './audio-request.component.html'
})
export class AudioRequestComponent implements OnInit, OnDestroy {
  audioRequests?: IAudioRequest[];
  eventSubscriber?: Subscription;

  constructor(
    protected audioRequestService: AudioRequestService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.audioRequestService.query().subscribe((res: HttpResponse<IAudioRequest[]>) => (this.audioRequests = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAudioRequests();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAudioRequest): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAudioRequests(): void {
    this.eventSubscriber = this.eventManager.subscribe('audioRequestListModification', () => this.loadAll());
  }

  delete(audioRequest: IAudioRequest): void {
    const modalRef = this.modalService.open(AudioRequestDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.audioRequest = audioRequest;
  }
}
