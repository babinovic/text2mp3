import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'language',
        loadChildren: () => import('./language/language.module').then(m => m.Text2Mp3LanguageModule)
      },
      {
        path: 'voice-style',
        loadChildren: () => import('./voice-style/voice-style.module').then(m => m.Text2Mp3VoiceStyleModule)
      },
      {
        path: 'voice-request',
        loadChildren: () => import('./voice-request/voice-request.module').then(m => m.Text2Mp3VoiceRequestModule)
      },
      {
        path: 'voice-response-api',
        loadChildren: () => import('./voice-response-api/voice-response-api.module').then(m => m.Text2Mp3VoiceResponseAPIModule)
      },
      {
        path: 'translation-language',
        loadChildren: () => import('./translation-language/translation-language.module').then(m => m.Text2Mp3TranslationLanguageModule)
      },
      {
        path: 'audio-request',
        loadChildren: () => import('./audio-request/audio-request.module').then(m => m.Text2Mp3AudioRequestModule)
      },
      {
        path: 'upload-response',
        loadChildren: () => import('./upload-response/upload-response.module').then(m => m.Text2Mp3UploadResponseModule)
      },
      {
        path: 'audio-response-api',
        loadChildren: () => import('./audio-response-api/audio-response-api.module').then(m => m.Text2Mp3AudioResponseAPIModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class Text2Mp3EntityModule {}
