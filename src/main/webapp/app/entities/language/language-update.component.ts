import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ILanguage, Language } from 'app/shared/model/language.model';
import { LanguageService } from './language.service';
import { ITranslationLanguage } from 'app/shared/model/translation-language.model';
import { TranslationLanguageService } from 'app/entities/translation-language/translation-language.service';

@Component({
  selector: 'jhi-language-update',
  templateUrl: './language-update.component.html'
})
export class LanguageUpdateComponent implements OnInit {
  isSaving = false;
  translationlanguages: ITranslationLanguage[] = [];

  editForm = this.fb.group({
    id: [],
    languageName: [],
    languageCode: [],
    translationLanguage: []
  });

  constructor(
    protected languageService: LanguageService,
    protected translationLanguageService: TranslationLanguageService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ language }) => {
      this.updateForm(language);

      this.translationLanguageService
        .query()
        .subscribe((res: HttpResponse<ITranslationLanguage[]>) => (this.translationlanguages = res.body || []));
    });
  }

  updateForm(language: ILanguage): void {
    this.editForm.patchValue({
      id: language.id,
      languageName: language.languageName,
      languageCode: language.languageCode,
      translationLanguage: language.translationLanguage
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const language = this.createFromForm();
    if (language.id !== undefined) {
      this.subscribeToSaveResponse(this.languageService.update(language));
    } else {
      this.subscribeToSaveResponse(this.languageService.create(language));
    }
  }

  private createFromForm(): ILanguage {
    return {
      ...new Language(),
      id: this.editForm.get(['id'])!.value,
      languageName: this.editForm.get(['languageName'])!.value,
      languageCode: this.editForm.get(['languageCode'])!.value,
      translationLanguage: this.editForm.get(['translationLanguage'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILanguage>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ITranslationLanguage): any {
    return item.id;
  }
}
