import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IUploadResponse, UploadResponse } from 'app/shared/model/upload-response.model';
import { UploadResponseService } from './upload-response.service';
import { UploadResponseComponent } from './upload-response.component';
import { UploadResponseDetailComponent } from './upload-response-detail.component';
import { UploadResponseUpdateComponent } from './upload-response-update.component';

@Injectable({ providedIn: 'root' })
export class UploadResponseResolve implements Resolve<IUploadResponse> {
  constructor(private service: UploadResponseService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUploadResponse> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((uploadResponse: HttpResponse<UploadResponse>) => {
          if (uploadResponse.body) {
            return of(uploadResponse.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new UploadResponse());
  }
}

export const uploadResponseRoute: Routes = [
  {
    path: '',
    component: UploadResponseComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.uploadResponse.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UploadResponseDetailComponent,
    resolve: {
      uploadResponse: UploadResponseResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.uploadResponse.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UploadResponseUpdateComponent,
    resolve: {
      uploadResponse: UploadResponseResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.uploadResponse.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UploadResponseUpdateComponent,
    resolve: {
      uploadResponse: UploadResponseResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.uploadResponse.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
