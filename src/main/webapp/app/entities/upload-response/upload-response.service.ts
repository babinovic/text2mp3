import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IUploadResponse } from 'app/shared/model/upload-response.model';

type EntityResponseType = HttpResponse<IUploadResponse>;
type EntityArrayResponseType = HttpResponse<IUploadResponse[]>;

@Injectable({ providedIn: 'root' })
export class UploadResponseService {
  public resourceUrl = SERVER_API_URL + 'api/upload-responses';

  constructor(protected http: HttpClient) {}

  create(uploadResponse: IUploadResponse): Observable<EntityResponseType> {
    return this.http.post<IUploadResponse>(this.resourceUrl, uploadResponse, { observe: 'response' });
  }

  update(uploadResponse: IUploadResponse): Observable<EntityResponseType> {
    return this.http.put<IUploadResponse>(this.resourceUrl, uploadResponse, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUploadResponse>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUploadResponse[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
