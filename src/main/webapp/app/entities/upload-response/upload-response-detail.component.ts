import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUploadResponse } from 'app/shared/model/upload-response.model';

@Component({
  selector: 'jhi-upload-response-detail',
  templateUrl: './upload-response-detail.component.html'
})
export class UploadResponseDetailComponent implements OnInit {
  uploadResponse: IUploadResponse | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ uploadResponse }) => (this.uploadResponse = uploadResponse));
  }

  previousState(): void {
    window.history.back();
  }
}
