import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IUploadResponse } from 'app/shared/model/upload-response.model';
import { UploadResponseService } from './upload-response.service';
import { UploadResponseDeleteDialogComponent } from './upload-response-delete-dialog.component';

@Component({
  selector: 'jhi-upload-response',
  templateUrl: './upload-response.component.html'
})
export class UploadResponseComponent implements OnInit, OnDestroy {
  uploadResponses?: IUploadResponse[];
  eventSubscriber?: Subscription;

  constructor(
    protected uploadResponseService: UploadResponseService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.uploadResponseService.query().subscribe((res: HttpResponse<IUploadResponse[]>) => (this.uploadResponses = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInUploadResponses();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IUploadResponse): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInUploadResponses(): void {
    this.eventSubscriber = this.eventManager.subscribe('uploadResponseListModification', () => this.loadAll());
  }

  delete(uploadResponse: IUploadResponse): void {
    const modalRef = this.modalService.open(UploadResponseDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.uploadResponse = uploadResponse;
  }
}
