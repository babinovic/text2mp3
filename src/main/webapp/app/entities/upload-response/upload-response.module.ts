import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Text2Mp3SharedModule } from 'app/shared/shared.module';
import { UploadResponseComponent } from './upload-response.component';
import { UploadResponseDetailComponent } from './upload-response-detail.component';
import { UploadResponseUpdateComponent } from './upload-response-update.component';
import { UploadResponseDeleteDialogComponent } from './upload-response-delete-dialog.component';
import { uploadResponseRoute } from './upload-response.route';

@NgModule({
  imports: [Text2Mp3SharedModule, RouterModule.forChild(uploadResponseRoute)],
  declarations: [
    UploadResponseComponent,
    UploadResponseDetailComponent,
    UploadResponseUpdateComponent,
    UploadResponseDeleteDialogComponent
  ],
  entryComponents: [UploadResponseDeleteDialogComponent]
})
export class Text2Mp3UploadResponseModule {}
