import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IUploadResponse, UploadResponse } from 'app/shared/model/upload-response.model';
import { UploadResponseService } from './upload-response.service';

@Component({
  selector: 'jhi-upload-response-update',
  templateUrl: './upload-response-update.component.html'
})
export class UploadResponseUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    uploadedFilePath: []
  });

  constructor(protected uploadResponseService: UploadResponseService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ uploadResponse }) => {
      this.updateForm(uploadResponse);
    });
  }

  updateForm(uploadResponse: IUploadResponse): void {
    this.editForm.patchValue({
      id: uploadResponse.id,
      uploadedFilePath: uploadResponse.uploadedFilePath
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const uploadResponse = this.createFromForm();
    if (uploadResponse.id !== undefined) {
      this.subscribeToSaveResponse(this.uploadResponseService.update(uploadResponse));
    } else {
      this.subscribeToSaveResponse(this.uploadResponseService.create(uploadResponse));
    }
  }

  private createFromForm(): IUploadResponse {
    return {
      ...new UploadResponse(),
      id: this.editForm.get(['id'])!.value,
      uploadedFilePath: this.editForm.get(['uploadedFilePath'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUploadResponse>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
