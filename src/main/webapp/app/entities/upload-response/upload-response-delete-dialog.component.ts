import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUploadResponse } from 'app/shared/model/upload-response.model';
import { UploadResponseService } from './upload-response.service';

@Component({
  templateUrl: './upload-response-delete-dialog.component.html'
})
export class UploadResponseDeleteDialogComponent {
  uploadResponse?: IUploadResponse;

  constructor(
    protected uploadResponseService: UploadResponseService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.uploadResponseService.delete(id).subscribe(() => {
      this.eventManager.broadcast('uploadResponseListModification');
      this.activeModal.close();
    });
  }
}
