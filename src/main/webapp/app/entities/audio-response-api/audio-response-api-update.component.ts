import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAudioResponseAPI, AudioResponseAPI } from 'app/shared/model/audio-response-api.model';
import { AudioResponseAPIService } from './audio-response-api.service';

@Component({
  selector: 'jhi-audio-response-api-update',
  templateUrl: './audio-response-api-update.component.html'
})
export class AudioResponseAPIUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    filePath: [],
    text: [],
    textPath: [],
    language: []
  });

  constructor(
    protected audioResponseAPIService: AudioResponseAPIService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ audioResponseAPI }) => {
      this.updateForm(audioResponseAPI);
    });
  }

  updateForm(audioResponseAPI: IAudioResponseAPI): void {
    this.editForm.patchValue({
      id: audioResponseAPI.id,
      filePath: audioResponseAPI.filePath,
      text: audioResponseAPI.text,
      textPath: audioResponseAPI.textPath,
      language: audioResponseAPI.language
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const audioResponseAPI = this.createFromForm();
    if (audioResponseAPI.id !== undefined) {
      this.subscribeToSaveResponse(this.audioResponseAPIService.update(audioResponseAPI));
    } else {
      this.subscribeToSaveResponse(this.audioResponseAPIService.create(audioResponseAPI));
    }
  }

  private createFromForm(): IAudioResponseAPI {
    return {
      ...new AudioResponseAPI(),
      id: this.editForm.get(['id'])!.value,
      filePath: this.editForm.get(['filePath'])!.value,
      text: this.editForm.get(['text'])!.value,
      textPath: this.editForm.get(['textPath'])!.value,
      language: this.editForm.get(['language'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAudioResponseAPI>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
