import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAudioResponseAPI } from 'app/shared/model/audio-response-api.model';

type EntityResponseType = HttpResponse<IAudioResponseAPI>;
type EntityArrayResponseType = HttpResponse<IAudioResponseAPI[]>;

@Injectable({ providedIn: 'root' })
export class AudioResponseAPIService {
  public resourceUrl = SERVER_API_URL + 'api/audio-response-apis';

  constructor(protected http: HttpClient) {}

  create(audioResponseAPI: IAudioResponseAPI): Observable<EntityResponseType> {
    return this.http.post<IAudioResponseAPI>(this.resourceUrl, audioResponseAPI, { observe: 'response' });
  }

  update(audioResponseAPI: IAudioResponseAPI): Observable<EntityResponseType> {
    return this.http.put<IAudioResponseAPI>(this.resourceUrl, audioResponseAPI, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAudioResponseAPI>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAudioResponseAPI[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
