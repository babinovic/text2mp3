import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAudioResponseAPI, AudioResponseAPI } from 'app/shared/model/audio-response-api.model';
import { AudioResponseAPIService } from './audio-response-api.service';
import { AudioResponseAPIComponent } from './audio-response-api.component';
import { AudioResponseAPIDetailComponent } from './audio-response-api-detail.component';
import { AudioResponseAPIUpdateComponent } from './audio-response-api-update.component';

@Injectable({ providedIn: 'root' })
export class AudioResponseAPIResolve implements Resolve<IAudioResponseAPI> {
  constructor(private service: AudioResponseAPIService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAudioResponseAPI> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((audioResponseAPI: HttpResponse<AudioResponseAPI>) => {
          if (audioResponseAPI.body) {
            return of(audioResponseAPI.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AudioResponseAPI());
  }
}

export const audioResponseAPIRoute: Routes = [
  {
    path: '',
    component: AudioResponseAPIComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.audioResponseAPI.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AudioResponseAPIDetailComponent,
    resolve: {
      audioResponseAPI: AudioResponseAPIResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.audioResponseAPI.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AudioResponseAPIUpdateComponent,
    resolve: {
      audioResponseAPI: AudioResponseAPIResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.audioResponseAPI.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AudioResponseAPIUpdateComponent,
    resolve: {
      audioResponseAPI: AudioResponseAPIResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.audioResponseAPI.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
