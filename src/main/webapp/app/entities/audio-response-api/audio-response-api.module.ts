import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Text2Mp3SharedModule } from 'app/shared/shared.module';
import { AudioResponseAPIComponent } from './audio-response-api.component';
import { AudioResponseAPIDetailComponent } from './audio-response-api-detail.component';
import { AudioResponseAPIUpdateComponent } from './audio-response-api-update.component';
import { AudioResponseAPIDeleteDialogComponent } from './audio-response-api-delete-dialog.component';
import { audioResponseAPIRoute } from './audio-response-api.route';

@NgModule({
  imports: [Text2Mp3SharedModule, RouterModule.forChild(audioResponseAPIRoute)],
  declarations: [
    AudioResponseAPIComponent,
    AudioResponseAPIDetailComponent,
    AudioResponseAPIUpdateComponent,
    AudioResponseAPIDeleteDialogComponent
  ],
  entryComponents: [AudioResponseAPIDeleteDialogComponent]
})
export class Text2Mp3AudioResponseAPIModule {}
