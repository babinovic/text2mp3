import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAudioResponseAPI } from 'app/shared/model/audio-response-api.model';

@Component({
  selector: 'jhi-audio-response-api-detail',
  templateUrl: './audio-response-api-detail.component.html'
})
export class AudioResponseAPIDetailComponent implements OnInit {
  audioResponseAPI: IAudioResponseAPI | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ audioResponseAPI }) => (this.audioResponseAPI = audioResponseAPI));
  }

  previousState(): void {
    window.history.back();
  }
}
