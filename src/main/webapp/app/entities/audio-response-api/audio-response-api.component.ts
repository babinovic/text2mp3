import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAudioResponseAPI } from 'app/shared/model/audio-response-api.model';
import { AudioResponseAPIService } from './audio-response-api.service';
import { AudioResponseAPIDeleteDialogComponent } from './audio-response-api-delete-dialog.component';

@Component({
  selector: 'jhi-audio-response-api',
  templateUrl: './audio-response-api.component.html'
})
export class AudioResponseAPIComponent implements OnInit, OnDestroy {
  audioResponseAPIS?: IAudioResponseAPI[];
  eventSubscriber?: Subscription;

  constructor(
    protected audioResponseAPIService: AudioResponseAPIService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.audioResponseAPIService.query().subscribe((res: HttpResponse<IAudioResponseAPI[]>) => (this.audioResponseAPIS = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAudioResponseAPIS();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAudioResponseAPI): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAudioResponseAPIS(): void {
    this.eventSubscriber = this.eventManager.subscribe('audioResponseAPIListModification', () => this.loadAll());
  }

  delete(audioResponseAPI: IAudioResponseAPI): void {
    const modalRef = this.modalService.open(AudioResponseAPIDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.audioResponseAPI = audioResponseAPI;
  }
}
