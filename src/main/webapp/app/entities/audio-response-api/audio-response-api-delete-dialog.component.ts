import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAudioResponseAPI } from 'app/shared/model/audio-response-api.model';
import { AudioResponseAPIService } from './audio-response-api.service';

@Component({
  templateUrl: './audio-response-api-delete-dialog.component.html'
})
export class AudioResponseAPIDeleteDialogComponent {
  audioResponseAPI?: IAudioResponseAPI;

  constructor(
    protected audioResponseAPIService: AudioResponseAPIService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.audioResponseAPIService.delete(id).subscribe(() => {
      this.eventManager.broadcast('audioResponseAPIListModification');
      this.activeModal.close();
    });
  }
}
