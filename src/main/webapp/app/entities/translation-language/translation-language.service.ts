import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITranslationLanguage } from 'app/shared/model/translation-language.model';

type EntityResponseType = HttpResponse<ITranslationLanguage>;
type EntityArrayResponseType = HttpResponse<ITranslationLanguage[]>;

@Injectable({ providedIn: 'root' })
export class TranslationLanguageService {
  public resourceUrl = SERVER_API_URL + 'api/translation-languages';

  constructor(protected http: HttpClient) {}

  create(translationLanguage: ITranslationLanguage): Observable<EntityResponseType> {
    return this.http.post<ITranslationLanguage>(this.resourceUrl, translationLanguage, { observe: 'response' });
  }

  update(translationLanguage: ITranslationLanguage): Observable<EntityResponseType> {
    return this.http.put<ITranslationLanguage>(this.resourceUrl, translationLanguage, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITranslationLanguage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITranslationLanguage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
