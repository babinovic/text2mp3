import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITranslationLanguage, TranslationLanguage } from 'app/shared/model/translation-language.model';
import { TranslationLanguageService } from './translation-language.service';

@Component({
  selector: 'jhi-translation-language-update',
  templateUrl: './translation-language-update.component.html'
})
export class TranslationLanguageUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    translationLanguageName: [],
    translationLanguageCode: []
  });

  constructor(
    protected translationLanguageService: TranslationLanguageService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ translationLanguage }) => {
      this.updateForm(translationLanguage);
    });
  }

  updateForm(translationLanguage: ITranslationLanguage): void {
    this.editForm.patchValue({
      id: translationLanguage.id,
      translationLanguageName: translationLanguage.translationLanguageName,
      translationLanguageCode: translationLanguage.translationLanguageCode
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const translationLanguage = this.createFromForm();
    if (translationLanguage.id !== undefined) {
      this.subscribeToSaveResponse(this.translationLanguageService.update(translationLanguage));
    } else {
      this.subscribeToSaveResponse(this.translationLanguageService.create(translationLanguage));
    }
  }

  private createFromForm(): ITranslationLanguage {
    return {
      ...new TranslationLanguage(),
      id: this.editForm.get(['id'])!.value,
      translationLanguageName: this.editForm.get(['translationLanguageName'])!.value,
      translationLanguageCode: this.editForm.get(['translationLanguageCode'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITranslationLanguage>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
