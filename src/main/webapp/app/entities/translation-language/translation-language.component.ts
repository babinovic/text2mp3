import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITranslationLanguage } from 'app/shared/model/translation-language.model';
import { TranslationLanguageService } from './translation-language.service';
import { TranslationLanguageDeleteDialogComponent } from './translation-language-delete-dialog.component';

@Component({
  selector: 'jhi-translation-language',
  templateUrl: './translation-language.component.html'
})
export class TranslationLanguageComponent implements OnInit, OnDestroy {
  translationLanguages?: ITranslationLanguage[];
  eventSubscriber?: Subscription;

  constructor(
    protected translationLanguageService: TranslationLanguageService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.translationLanguageService
      .query()
      .subscribe((res: HttpResponse<ITranslationLanguage[]>) => (this.translationLanguages = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTranslationLanguages();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITranslationLanguage): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTranslationLanguages(): void {
    this.eventSubscriber = this.eventManager.subscribe('translationLanguageListModification', () => this.loadAll());
  }

  delete(translationLanguage: ITranslationLanguage): void {
    const modalRef = this.modalService.open(TranslationLanguageDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.translationLanguage = translationLanguage;
  }
}
