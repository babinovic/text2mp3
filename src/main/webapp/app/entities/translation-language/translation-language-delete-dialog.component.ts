import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITranslationLanguage } from 'app/shared/model/translation-language.model';
import { TranslationLanguageService } from './translation-language.service';

@Component({
  templateUrl: './translation-language-delete-dialog.component.html'
})
export class TranslationLanguageDeleteDialogComponent {
  translationLanguage?: ITranslationLanguage;

  constructor(
    protected translationLanguageService: TranslationLanguageService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.translationLanguageService.delete(id).subscribe(() => {
      this.eventManager.broadcast('translationLanguageListModification');
      this.activeModal.close();
    });
  }
}
