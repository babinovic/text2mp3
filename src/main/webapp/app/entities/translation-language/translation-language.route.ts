import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITranslationLanguage, TranslationLanguage } from 'app/shared/model/translation-language.model';
import { TranslationLanguageService } from './translation-language.service';
import { TranslationLanguageComponent } from './translation-language.component';
import { TranslationLanguageDetailComponent } from './translation-language-detail.component';
import { TranslationLanguageUpdateComponent } from './translation-language-update.component';

@Injectable({ providedIn: 'root' })
export class TranslationLanguageResolve implements Resolve<ITranslationLanguage> {
  constructor(private service: TranslationLanguageService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITranslationLanguage> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((translationLanguage: HttpResponse<TranslationLanguage>) => {
          if (translationLanguage.body) {
            return of(translationLanguage.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TranslationLanguage());
  }
}

export const translationLanguageRoute: Routes = [
  {
    path: '',
    component: TranslationLanguageComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.translationLanguage.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TranslationLanguageDetailComponent,
    resolve: {
      translationLanguage: TranslationLanguageResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.translationLanguage.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TranslationLanguageUpdateComponent,
    resolve: {
      translationLanguage: TranslationLanguageResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.translationLanguage.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TranslationLanguageUpdateComponent,
    resolve: {
      translationLanguage: TranslationLanguageResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.translationLanguage.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
