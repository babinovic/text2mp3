import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Text2Mp3SharedModule } from 'app/shared/shared.module';
import { TranslationLanguageComponent } from './translation-language.component';
import { TranslationLanguageDetailComponent } from './translation-language-detail.component';
import { TranslationLanguageUpdateComponent } from './translation-language-update.component';
import { TranslationLanguageDeleteDialogComponent } from './translation-language-delete-dialog.component';
import { translationLanguageRoute } from './translation-language.route';

@NgModule({
  imports: [Text2Mp3SharedModule, RouterModule.forChild(translationLanguageRoute)],
  declarations: [
    TranslationLanguageComponent,
    TranslationLanguageDetailComponent,
    TranslationLanguageUpdateComponent,
    TranslationLanguageDeleteDialogComponent
  ],
  entryComponents: [TranslationLanguageDeleteDialogComponent]
})
export class Text2Mp3TranslationLanguageModule {}
