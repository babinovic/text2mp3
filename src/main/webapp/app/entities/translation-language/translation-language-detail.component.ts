import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITranslationLanguage } from 'app/shared/model/translation-language.model';

@Component({
  selector: 'jhi-translation-language-detail',
  templateUrl: './translation-language-detail.component.html'
})
export class TranslationLanguageDetailComponent implements OnInit {
  translationLanguage: ITranslationLanguage | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ translationLanguage }) => (this.translationLanguage = translationLanguage));
  }

  previousState(): void {
    window.history.back();
  }
}
