import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IVoiceResponseAPI } from 'app/shared/model/voice-response-api.model';

type EntityResponseType = HttpResponse<IVoiceResponseAPI>;
type EntityArrayResponseType = HttpResponse<IVoiceResponseAPI[]>;

@Injectable({ providedIn: 'root' })
export class VoiceResponseAPIService {
  public resourceUrl = SERVER_API_URL + 'api/voice-response-apis';

  constructor(protected http: HttpClient) {}

  create(voiceResponseAPI: IVoiceResponseAPI): Observable<EntityResponseType> {
    return this.http.post<IVoiceResponseAPI>(this.resourceUrl, voiceResponseAPI, { observe: 'response' });
  }

  update(voiceResponseAPI: IVoiceResponseAPI): Observable<EntityResponseType> {
    return this.http.put<IVoiceResponseAPI>(this.resourceUrl, voiceResponseAPI, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IVoiceResponseAPI>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IVoiceResponseAPI[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
