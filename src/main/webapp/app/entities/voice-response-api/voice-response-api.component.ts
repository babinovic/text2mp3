import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IVoiceResponseAPI } from 'app/shared/model/voice-response-api.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { VoiceResponseAPIService } from './voice-response-api.service';
import { VoiceResponseAPIDeleteDialogComponent } from './voice-response-api-delete-dialog.component';

@Component({
  selector: 'jhi-voice-response-api',
  templateUrl: './voice-response-api.component.html'
})
export class VoiceResponseAPIComponent implements OnInit, OnDestroy {
  voiceResponseAPIS?: IVoiceResponseAPI[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;

  constructor(
    protected voiceResponseAPIService: VoiceResponseAPIService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.voiceResponseAPIService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IVoiceResponseAPI[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInVoiceResponseAPIS();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IVoiceResponseAPI): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInVoiceResponseAPIS(): void {
    this.eventSubscriber = this.eventManager.subscribe('voiceResponseAPIListModification', () => this.loadPage());
  }

  delete(voiceResponseAPI: IVoiceResponseAPI): void {
    const modalRef = this.modalService.open(VoiceResponseAPIDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.voiceResponseAPI = voiceResponseAPI;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IVoiceResponseAPI[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/voice-response-api'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.voiceResponseAPIS = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
}
