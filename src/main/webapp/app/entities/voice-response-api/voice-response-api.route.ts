import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IVoiceResponseAPI, VoiceResponseAPI } from 'app/shared/model/voice-response-api.model';
import { VoiceResponseAPIService } from './voice-response-api.service';
import { VoiceResponseAPIComponent } from './voice-response-api.component';
import { VoiceResponseAPIDetailComponent } from './voice-response-api-detail.component';
import { VoiceResponseAPIUpdateComponent } from './voice-response-api-update.component';

@Injectable({ providedIn: 'root' })
export class VoiceResponseAPIResolve implements Resolve<IVoiceResponseAPI> {
  constructor(private service: VoiceResponseAPIService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IVoiceResponseAPI> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((voiceResponseAPI: HttpResponse<VoiceResponseAPI>) => {
          if (voiceResponseAPI.body) {
            return of(voiceResponseAPI.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new VoiceResponseAPI());
  }
}

export const voiceResponseAPIRoute: Routes = [
  {
    path: '',
    component: VoiceResponseAPIComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'text2Mp3App.voiceResponseAPI.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: VoiceResponseAPIDetailComponent,
    resolve: {
      voiceResponseAPI: VoiceResponseAPIResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.voiceResponseAPI.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: VoiceResponseAPIUpdateComponent,
    resolve: {
      voiceResponseAPI: VoiceResponseAPIResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.voiceResponseAPI.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: VoiceResponseAPIUpdateComponent,
    resolve: {
      voiceResponseAPI: VoiceResponseAPIResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.voiceResponseAPI.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
