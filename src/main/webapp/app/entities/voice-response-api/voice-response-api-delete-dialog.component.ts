import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IVoiceResponseAPI } from 'app/shared/model/voice-response-api.model';
import { VoiceResponseAPIService } from './voice-response-api.service';

@Component({
  templateUrl: './voice-response-api-delete-dialog.component.html'
})
export class VoiceResponseAPIDeleteDialogComponent {
  voiceResponseAPI?: IVoiceResponseAPI;

  constructor(
    protected voiceResponseAPIService: VoiceResponseAPIService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.voiceResponseAPIService.delete(id).subscribe(() => {
      this.eventManager.broadcast('voiceResponseAPIListModification');
      this.activeModal.close();
    });
  }
}
