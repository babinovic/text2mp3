import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Text2Mp3SharedModule } from 'app/shared/shared.module';
import { VoiceResponseAPIComponent } from './voice-response-api.component';
import { VoiceResponseAPIDetailComponent } from './voice-response-api-detail.component';
import { VoiceResponseAPIUpdateComponent } from './voice-response-api-update.component';
import { VoiceResponseAPIDeleteDialogComponent } from './voice-response-api-delete-dialog.component';
import { voiceResponseAPIRoute } from './voice-response-api.route';

@NgModule({
  imports: [Text2Mp3SharedModule, RouterModule.forChild(voiceResponseAPIRoute)],
  declarations: [
    VoiceResponseAPIComponent,
    VoiceResponseAPIDetailComponent,
    VoiceResponseAPIUpdateComponent,
    VoiceResponseAPIDeleteDialogComponent
  ],
  entryComponents: [VoiceResponseAPIDeleteDialogComponent]
})
export class Text2Mp3VoiceResponseAPIModule {}
