import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IVoiceResponseAPI, VoiceResponseAPI } from 'app/shared/model/voice-response-api.model';
import { VoiceResponseAPIService } from './voice-response-api.service';

@Component({
  selector: 'jhi-voice-response-api-update',
  templateUrl: './voice-response-api-update.component.html'
})
export class VoiceResponseAPIUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    mp3Path: [],
    language: [],
    text: [],
    textPath: []
  });

  constructor(
    protected voiceResponseAPIService: VoiceResponseAPIService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ voiceResponseAPI }) => {
      this.updateForm(voiceResponseAPI);
    });
  }

  updateForm(voiceResponseAPI: IVoiceResponseAPI): void {
    this.editForm.patchValue({
      id: voiceResponseAPI.id,
      mp3Path: voiceResponseAPI.mp3Path,
      language: voiceResponseAPI.language,
      text: voiceResponseAPI.text,
      textPath: voiceResponseAPI.textPath
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const voiceResponseAPI = this.createFromForm();
    if (voiceResponseAPI.id !== undefined) {
      this.subscribeToSaveResponse(this.voiceResponseAPIService.update(voiceResponseAPI));
    } else {
      this.subscribeToSaveResponse(this.voiceResponseAPIService.create(voiceResponseAPI));
    }
  }

  private createFromForm(): IVoiceResponseAPI {
    return {
      ...new VoiceResponseAPI(),
      id: this.editForm.get(['id'])!.value,
      mp3Path: this.editForm.get(['mp3Path'])!.value,
      language: this.editForm.get(['language'])!.value,
      text: this.editForm.get(['text'])!.value,
      textPath: this.editForm.get(['textPath'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVoiceResponseAPI>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
