import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVoiceResponseAPI } from 'app/shared/model/voice-response-api.model';

@Component({
  selector: 'jhi-voice-response-api-detail',
  templateUrl: './voice-response-api-detail.component.html'
})
export class VoiceResponseAPIDetailComponent implements OnInit {
  voiceResponseAPI: IVoiceResponseAPI | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ voiceResponseAPI }) => (this.voiceResponseAPI = voiceResponseAPI));
  }

  previousState(): void {
    window.history.back();
  }
}
