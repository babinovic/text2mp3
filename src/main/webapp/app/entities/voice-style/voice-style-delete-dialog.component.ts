import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IVoiceStyle } from 'app/shared/model/voice-style.model';
import { VoiceStyleService } from './voice-style.service';

@Component({
  templateUrl: './voice-style-delete-dialog.component.html'
})
export class VoiceStyleDeleteDialogComponent {
  voiceStyle?: IVoiceStyle;

  constructor(
    protected voiceStyleService: VoiceStyleService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.voiceStyleService.delete(id).subscribe(() => {
      this.eventManager.broadcast('voiceStyleListModification');
      this.activeModal.close();
    });
  }
}
