import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IVoiceStyle } from 'app/shared/model/voice-style.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { VoiceStyleService } from './voice-style.service';
import { VoiceStyleDeleteDialogComponent } from './voice-style-delete-dialog.component';

@Component({
  selector: 'jhi-voice-style',
  templateUrl: './voice-style.component.html'
})
export class VoiceStyleComponent implements OnInit, OnDestroy {
  voiceStyles: IVoiceStyle[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected voiceStyleService: VoiceStyleService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.voiceStyles = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.voiceStyleService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<IVoiceStyle[]>) => this.paginateVoiceStyles(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.voiceStyles = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInVoiceStyles();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IVoiceStyle): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInVoiceStyles(): void {
    this.eventSubscriber = this.eventManager.subscribe('voiceStyleListModification', () => this.reset());
  }

  delete(voiceStyle: IVoiceStyle): void {
    const modalRef = this.modalService.open(VoiceStyleDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.voiceStyle = voiceStyle;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateVoiceStyles(data: IVoiceStyle[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.voiceStyles.push(data[i]);
      }
    }
  }
}
