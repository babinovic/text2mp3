import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVoiceStyle } from 'app/shared/model/voice-style.model';

@Component({
  selector: 'jhi-voice-style-detail',
  templateUrl: './voice-style-detail.component.html'
})
export class VoiceStyleDetailComponent implements OnInit {
  voiceStyle: IVoiceStyle | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ voiceStyle }) => (this.voiceStyle = voiceStyle));
  }

  previousState(): void {
    window.history.back();
  }
}
