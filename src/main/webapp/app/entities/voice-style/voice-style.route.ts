import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IVoiceStyle, VoiceStyle } from 'app/shared/model/voice-style.model';
import { VoiceStyleService } from './voice-style.service';
import { VoiceStyleComponent } from './voice-style.component';
import { VoiceStyleDetailComponent } from './voice-style-detail.component';
import { VoiceStyleUpdateComponent } from './voice-style-update.component';

@Injectable({ providedIn: 'root' })
export class VoiceStyleResolve implements Resolve<IVoiceStyle> {
  constructor(private service: VoiceStyleService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IVoiceStyle> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((voiceStyle: HttpResponse<VoiceStyle>) => {
          if (voiceStyle.body) {
            return of(voiceStyle.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new VoiceStyle());
  }
}

export const voiceStyleRoute: Routes = [
  {
    path: '',
    component: VoiceStyleComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.voiceStyle.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: VoiceStyleDetailComponent,
    resolve: {
      voiceStyle: VoiceStyleResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.voiceStyle.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: VoiceStyleUpdateComponent,
    resolve: {
      voiceStyle: VoiceStyleResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.voiceStyle.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: VoiceStyleUpdateComponent,
    resolve: {
      voiceStyle: VoiceStyleResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'text2Mp3App.voiceStyle.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
