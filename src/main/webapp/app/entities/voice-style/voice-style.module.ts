import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Text2Mp3SharedModule } from 'app/shared/shared.module';
import { VoiceStyleComponent } from './voice-style.component';
import { VoiceStyleDetailComponent } from './voice-style-detail.component';
import { VoiceStyleUpdateComponent } from './voice-style-update.component';
import { VoiceStyleDeleteDialogComponent } from './voice-style-delete-dialog.component';
import { voiceStyleRoute } from './voice-style.route';

@NgModule({
  imports: [Text2Mp3SharedModule, RouterModule.forChild(voiceStyleRoute)],
  declarations: [VoiceStyleComponent, VoiceStyleDetailComponent, VoiceStyleUpdateComponent, VoiceStyleDeleteDialogComponent],
  entryComponents: [VoiceStyleDeleteDialogComponent]
})
export class Text2Mp3VoiceStyleModule {}
