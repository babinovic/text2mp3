import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IVoiceStyle, VoiceStyle } from 'app/shared/model/voice-style.model';
import { VoiceStyleService } from './voice-style.service';
import { ILanguage } from 'app/shared/model/language.model';
import { LanguageService } from 'app/entities/language/language.service';

@Component({
  selector: 'jhi-voice-style-update',
  templateUrl: './voice-style-update.component.html'
})
export class VoiceStyleUpdateComponent implements OnInit {
  isSaving = false;
  languages: ILanguage[] = [];

  editForm = this.fb.group({
    id: [],
    voiceStyleName: [],
    voiceStyleGender: [],
    language: []
  });

  constructor(
    protected voiceStyleService: VoiceStyleService,
    protected languageService: LanguageService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ voiceStyle }) => {
      this.updateForm(voiceStyle);

      this.languageService.query().subscribe((res: HttpResponse<ILanguage[]>) => (this.languages = res.body || []));
    });
  }

  updateForm(voiceStyle: IVoiceStyle): void {
    this.editForm.patchValue({
      id: voiceStyle.id,
      voiceStyleName: voiceStyle.voiceStyleName,
      voiceStyleGender: voiceStyle.voiceStyleGender,
      language: voiceStyle.language
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const voiceStyle = this.createFromForm();
    if (voiceStyle.id !== undefined) {
      this.subscribeToSaveResponse(this.voiceStyleService.update(voiceStyle));
    } else {
      this.subscribeToSaveResponse(this.voiceStyleService.create(voiceStyle));
    }
  }

  private createFromForm(): IVoiceStyle {
    return {
      ...new VoiceStyle(),
      id: this.editForm.get(['id'])!.value,
      voiceStyleName: this.editForm.get(['voiceStyleName'])!.value,
      voiceStyleGender: this.editForm.get(['voiceStyleGender'])!.value,
      language: this.editForm.get(['language'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVoiceStyle>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ILanguage): any {
    return item.id;
  }
}
