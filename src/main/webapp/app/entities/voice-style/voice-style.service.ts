import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IVoiceStyle } from 'app/shared/model/voice-style.model';

type EntityResponseType = HttpResponse<IVoiceStyle>;
type EntityArrayResponseType = HttpResponse<IVoiceStyle[]>;

@Injectable({ providedIn: 'root' })
export class VoiceStyleService {
  public resourceUrl = SERVER_API_URL + 'api/voice-styles';

  constructor(protected http: HttpClient) {}

  create(voiceStyle: IVoiceStyle): Observable<EntityResponseType> {
    return this.http.post<IVoiceStyle>(this.resourceUrl, voiceStyle, { observe: 'response' });
  }

  update(voiceStyle: IVoiceStyle): Observable<EntityResponseType> {
    return this.http.put<IVoiceStyle>(this.resourceUrl, voiceStyle, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IVoiceStyle>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IVoiceStyle[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
