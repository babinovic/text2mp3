import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVoiceRequest } from 'app/shared/model/voice-request.model';

@Component({
  selector: 'jhi-voice-request-detail',
  templateUrl: './voice-request-detail.component.html'
})
export class VoiceRequestDetailComponent implements OnInit {
  voiceRequest: IVoiceRequest | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ voiceRequest }) => (this.voiceRequest = voiceRequest));
  }

  previousState(): void {
    window.history.back();
  }
}
