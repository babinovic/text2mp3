import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IVoiceRequest, VoiceRequest } from 'app/shared/model/voice-request.model';
import { VoiceRequestService } from './voice-request.service';
import { IVoiceStyle } from 'app/shared/model/voice-style.model';
import { VoiceStyleService } from 'app/entities/voice-style/voice-style.service';
import { ILanguage } from 'app/shared/model/language.model';
import { LanguageService } from 'app/entities/language/language.service';

type SelectableEntity = IVoiceStyle | ILanguage;

@Component({
  selector: 'jhi-voice-request-update',
  templateUrl: './voice-request-update.component.html'
})
export class VoiceRequestUpdateComponent implements OnInit {
  isSaving = false;
  voicestyles: IVoiceStyle[] = [];
  languages: ILanguage[] = [];

  editForm = this.fb.group({
    id: [],
    voiceRequestText: [],
    voiceRequestTextFormat: [],
    validation: [],
    voiceStyle: [],
    translationLanguages: []
  });

  constructor(
    protected voiceRequestService: VoiceRequestService,
    protected voiceStyleService: VoiceStyleService,
    protected languageService: LanguageService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ voiceRequest }) => {
      this.updateForm(voiceRequest);

      this.voiceStyleService.query().subscribe((res: HttpResponse<IVoiceStyle[]>) => (this.voicestyles = res.body || []));

      this.languageService.query().subscribe((res: HttpResponse<ILanguage[]>) => (this.languages = res.body || []));
    });
  }

  updateForm(voiceRequest: IVoiceRequest): void {
    this.editForm.patchValue({
      id: voiceRequest.id,
      voiceRequestText: voiceRequest.voiceRequestText,
      voiceRequestTextFormat: voiceRequest.voiceRequestTextFormat,
      validation: voiceRequest.validation,
      voiceStyle: voiceRequest.voiceStyle,
      translationLanguages: voiceRequest.translationLanguages
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const voiceRequest = this.createFromForm();
    if (voiceRequest.id !== undefined) {
      this.subscribeToSaveResponse(this.voiceRequestService.update(voiceRequest));
    } else {
      this.subscribeToSaveResponse(this.voiceRequestService.create(voiceRequest));
    }
  }

  private createFromForm(): IVoiceRequest {
    return {
      ...new VoiceRequest(),
      id: this.editForm.get(['id'])!.value,
      voiceRequestText: this.editForm.get(['voiceRequestText'])!.value,
      voiceRequestTextFormat: this.editForm.get(['voiceRequestTextFormat'])!.value,
      validation: this.editForm.get(['validation'])!.value,
      voiceStyle: this.editForm.get(['voiceStyle'])!.value,
      translationLanguages: this.editForm.get(['translationLanguages'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVoiceRequest>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: ILanguage[], option: ILanguage): ILanguage {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
