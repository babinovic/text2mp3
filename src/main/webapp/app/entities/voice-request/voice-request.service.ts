import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IVoiceRequest } from 'app/shared/model/voice-request.model';

type EntityResponseType = HttpResponse<IVoiceRequest>;
type EntityArrayResponseType = HttpResponse<IVoiceRequest[]>;

@Injectable({ providedIn: 'root' })
export class VoiceRequestService {
  public resourceUrl = SERVER_API_URL + 'api/voice-requests';

  constructor(protected http: HttpClient) {}

  create(voiceRequest: IVoiceRequest): Observable<EntityResponseType> {
    return this.http.post<IVoiceRequest>(this.resourceUrl, voiceRequest, { observe: 'response' });
  }

  update(voiceRequest: IVoiceRequest): Observable<EntityResponseType> {
    return this.http.put<IVoiceRequest>(this.resourceUrl, voiceRequest, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IVoiceRequest>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IVoiceRequest[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
