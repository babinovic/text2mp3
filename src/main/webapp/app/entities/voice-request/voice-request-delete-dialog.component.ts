import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IVoiceRequest } from 'app/shared/model/voice-request.model';
import { VoiceRequestService } from './voice-request.service';

@Component({
  templateUrl: './voice-request-delete-dialog.component.html'
})
export class VoiceRequestDeleteDialogComponent {
  voiceRequest?: IVoiceRequest;

  constructor(
    protected voiceRequestService: VoiceRequestService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.voiceRequestService.delete(id).subscribe(() => {
      this.eventManager.broadcast('voiceRequestListModification');
      this.activeModal.close();
    });
  }
}
