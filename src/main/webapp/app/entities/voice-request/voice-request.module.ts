import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Text2Mp3SharedModule } from 'app/shared/shared.module';
import { VoiceRequestComponent } from './voice-request.component';
import { VoiceRequestDetailComponent } from './voice-request-detail.component';
import { VoiceRequestUpdateComponent } from './voice-request-update.component';
import { VoiceRequestDeleteDialogComponent } from './voice-request-delete-dialog.component';
import { voiceRequestRoute } from './voice-request.route';

@NgModule({
  imports: [Text2Mp3SharedModule, RouterModule.forChild(voiceRequestRoute)],
  declarations: [VoiceRequestComponent, VoiceRequestDetailComponent, VoiceRequestUpdateComponent, VoiceRequestDeleteDialogComponent],
  entryComponents: [VoiceRequestDeleteDialogComponent]
})
export class Text2Mp3VoiceRequestModule {}
