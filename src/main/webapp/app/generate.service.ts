import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { VoiceRequest } from './shared/model/voice-request.model';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { VoiceResponseAPI } from './shared/model/voice-response-api.model';
import { AudioRequest } from './shared/model/audio-request.model';
import { AudioResponseAPI } from './shared/model/audio-response-api.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class GenerateService 
{
  baseurl = './../api';
  url: string;
 
  constructor(private http: HttpClient) 
  {
      this.url = location.hostname + this.baseurl;
  }

  // get recognition text from file path
  getRecognitionText(audioRequest: AudioRequest): Observable<AudioResponseAPI> 
  {
      return this.http.post<AudioResponseAPI>(this.url + '/audio-recognition-text', audioRequest, httpOptions)
      .pipe(
      catchError(this.errorHandl)
      );
  }
   
  // Get MP3 file path from local API
  getVoices(voiceRequest: VoiceRequest): Observable<VoiceResponseAPI[]> 
  {
      return this.http.post<VoiceResponseAPI[]>(this.url + '/voice-request-generate', voiceRequest, httpOptions)
      .pipe(
      catchError(this.errorHandl)
      );
  }

  getTranslatedVoices(audioRequest: AudioRequest): Observable<VoiceResponseAPI[]> 
  {
      return this.http.post<VoiceResponseAPI[]>(this.url + '/audio-request-generate', audioRequest, httpOptions)
      .pipe(
      catchError(this.errorHandl)
      );
  }

  getTranslatedVoicesAfterValidation(modifiedResponse: VoiceResponseAPI[]): Observable<VoiceResponseAPI[]> 
  {
      return this.http.post<VoiceResponseAPI[]>(this.url + '/voice-request-generate-validation', modifiedResponse, httpOptions)
      .pipe(
      catchError(this.errorHandl)
      );
  }

  // Error handling
  errorHandl(error: any): Observable<any> 
  {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) 
    {
      errorMessage = error.error.message;
    } 
    else 
    {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
  return throwError(errorMessage);
}
}