import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AudioComponent } from './text2mp3/audio/audio.component';
import { VideoComponent } from './text2mp3/video/video.component';
import { TextComponent } from './text2mp3/text/text.component';
import { AboutComponent } from './text2mp3/about/about.component';
import { ErrorComponent } from './layouts/error/error.component';

const routes: Routes = [
  { path: '', redirectTo: '/text', pathMatch: 'full'}, 
  { path: 'text', component: TextComponent}, 
  { path: 'audio', component: AudioComponent}, 
  { path: 'video', component: VideoComponent},
  { path: 'about', component: AboutComponent},
  { path: '**', component: ErrorComponent},  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { enableTracing: false }),
  ],
  exports: [RouterModule]
})
export class Text2Mp3AppRoutingModule {}
