import { Injectable } from '@angular/core';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import {Observable} from "rxjs";

@Injectable()
export class UploadService {

  baseurl = './../api';
  url: string;
  
  constructor(private http: HttpClient) 
  {
    this.url = location.hostname + this.baseurl;
  }

  upload(file: File): Observable<HttpEvent<any>> 
  {
    const formData: FormData = new FormData();

    formData.append('file', file);

    const req = new HttpRequest('POST', `${this.url}/uploadFile`, formData, 
    {
      reportProgress: true,
      responseType: 'json'
    })
    return this.http.request(req);
  }
}
