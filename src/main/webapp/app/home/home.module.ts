import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Text2Mp3SharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [Text2Mp3SharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent]
})
export class Text2Mp3HomeModule {}
