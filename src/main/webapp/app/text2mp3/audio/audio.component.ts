import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LanguageService } from '../language.service';
import { Subscription } from 'rxjs';
import { GenerateService } from '../../generate.service';
import { PlayerFile, PlayerTheme, PlayerThemeDark } from 'gs-player';
import { VoiceResponseAPI } from 'app/shared/model/voice-response-api.model';
import { faUserCheck, faLanguage, faUndo, faAudioDescription, faMicrophone, faRss, faSignLanguage, faDownload, faUpload, faFileAudio, faVolumeUp } from '@fortawesome/free-solid-svg-icons';
import { ILanguage, Language } from 'app/shared/model/language.model';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { ThemePalette } from '@angular/material/core';
import { AudioRequest } from 'app/shared/model/audio-request.model';
import { UploadService } from 'app/upload.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { UploadResponse } from 'app/shared/model/upload-response.model';
import { TranslationValidationComponent } from '../translation-validation/translation-validation.component';
import { AudioResponseAPI } from 'app/shared/model/audio-response-api.model';
import { RecognitionValidationComponent } from '../recognition-validation/recognition-validation.component';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'jhi-audio',
  templateUrl: './audio.component.html',
  styleUrls: ['./audio.component.scss']
})
export class AudioComponent implements OnInit
{
	audioRequest!: AudioRequest;	
	audioRequestForm: FormGroup;
	uploadResponse!: UploadResponse;
	audioResponses: Array<VoiceResponseAPI>= [];
	audioSelectedTranslationLanguages: any = []; audioAvailableTranslationLanguages: any = []
	audioSelectedLanguage!: ILanguage; audioSelectedTranslationLanguage!: ILanguage;
	audioSelectedTranslationLanguageName?: string;
	// input language
	audioInputLanguage!: ILanguage;
	languages: any = []; 

	files: Array<PlayerFile> = [];
	filePath = "";
	playerTheme: PlayerTheme = PlayerThemeDark;
	colorSlider: ThemePalette = 'primary';
	isReadyToUpload = false;
	helpMessage = "";
	
	// Booleans
	audioShow = false;
	audioLoading = false;
	audioSubmitted = false;
	recorderLoaded = false;
	modeAudioFile = false;
	modeAudioRecord = true;
	
	// Icons
	faUserCheck=faUserCheck;faVolumeUp=faVolumeUp;faFileAudio=faFileAudio;faUpload=faUpload;faLanguage=faLanguage;faUndo=faUndo;faAudioDescription=faAudioDescription;faMicrophone=faMicrophone;faRss=faRss;faSignLanguage=faSignLanguage;faDownload=faDownload;
  
	// File Upload
	currentFile?: File;
	progress = -1;
	pathPlaceHolder = "";

	// Audio recorder
	seconds!: number;
	audioURLs: any = [];
	public audioUrl!: string;

	// mic recorder
	recorder: any;
	startBtnEnabled = true;
	stopBtnEnabled = false;
	MicRecorder = require('mic-recorder').default;

	// Recognition validation
	recognitionText = '';
	// Translation Validation	
	unvalidatedTranslations: VoiceResponseAPI[] = [];
	validationStatus: number[] = [];
	
	constructor(formBuilder: FormBuilder,public languageService: LanguageService, public generateService: GenerateService,
		 public uploadService: UploadService, public dialog: MatDialog, private _snackBar: MatSnackBar)  
	{
		this.audioRequestForm = this.createFormGroup(formBuilder);
	}

	ngOnInit(): void 
	{
		this.main()
		.then()
		.catch(() => alert('Error loading languages'));	
	}

	main = async () => 
	{
		try 
		{
			await this.loadLanguages();
		}
		catch (error)
		{
			alert('Error loading data : '+ error);
		}
	};
  
	toogleAudioMode() : void
	{
		if (this.modeAudioFile)
		{
			this.modeAudioFile = false;
			this.modeAudioRecord = true;
		}
		else if (this.modeAudioRecord)
		{
			this.modeAudioFile = true;
			this.modeAudioRecord = false;
			this.openSnackBar('You can import WAV 16 bits 16Khz mono only or FLAC 16 or 24 bits 44.1Khz mono only. Neither MP3 nor OGG are supported. If you don\'t have this type of file, you can use Audacity to record or convert an audio file. FLAC format provides the best results for speech recognition.');
		}
	}

	// open the snackbar to display specified message
	openSnackBar(message: string) : void 
	{
		this._snackBar.open(message, 'Close', {
			duration: 8000,
			horizontalPosition: 'center',
			verticalPosition: 'top',
		  });
	  }

	// Creates the forrm group for text to speech request
	createFormGroup(formBuilder: FormBuilder) : FormGroup
	{
		return formBuilder.group({
			'audioFilePath': new FormControl('', Validators.required),
			'audioTranslationLanguages': new FormControl([], Validators.required),
			'language': new FormControl(Validators.required),
			'validation': new FormControl(false),
			'validationRecognition': new FormControl(false),
		  });
	}

	async loadLanguages(): Promise<void>
	{
		return this.languageService.getLanguages()
		.toPromise()
		.then((data: {}) => 
			{
				this.languages = data;
				this.audioAvailableTranslationLanguages = data;
				this.audioInputLanguage = data[0];
				this.audioSelectedLanguage = data[1];
			});
  	}

	// At the drag drop area
	// (drop)="onDropFile($event)"
	onDrop(event: DragEvent): void
	{			
		event.preventDefault();
		const draggedFile = event.dataTransfer?.files.item(0);
		this.currentFile = draggedFile!;
		this.isReadyToUpload = true;
		this.upload();		
	}

	// At the file input element
	// (change)="selectFile($event)"
	selectFile(event: any): void 
	{
		this.currentFile = event.target.files[0];
		if (this.currentFile!.size > 0)
		{
			this.isReadyToUpload = true;
			this.upload();
		}	
	}

	// file upload
	upload(): void 
	{
		if (this.isReadyToUpload)
		{
			this.progress = 0;  
			this.uploadService.upload(this.currentFile!).subscribe(event => 
			{
				if (event.type === HttpEventType.UploadProgress) 
				{
					this.progress = Math.round(100 * event.loaded / event.total!);
				} 
				else if (event instanceof HttpResponse) 
				{
					this.uploadResponse = event.body;
					this.loadUploadedFile(); 
				}
			},
			err => 
			{
				alert('Error during uploading'+err);
				this.progress = -1; 
				this.isReadyToUpload = false;
			});
		}	
	}

	// when language is changed
	onLanguageChange(language: Language): void
	{
		this.audioInputLanguage = language;
		this.audioSelectedTranslationLanguageName = this.audioSelectedLanguage.languageName;
	}

	addTranslationLanguage() : any
	{
		this.audioSelectedTranslationLanguages.push(this.audioSelectedTranslationLanguage);
	}

	removeTranslationLanguage(translationLanguage: Language): any 
	{
		this.audioSelectedTranslationLanguages.splice(this.audioSelectedTranslationLanguages.indexOf(translationLanguage), 1);
	}

	hasError = (controlName: string, errorName: string) =>
	{
		return this.audioRequestForm.controls[controlName].hasError(errorName);
	}

	// au clic sur Generate
	onSubmit(): void  
	{
		this.audioSubmitted = true;
		this.audioRequestForm.get('audioTranslationLanguages')?.setValue(this.audioSelectedTranslationLanguages); 
		this.audioRequestForm.get('audioTranslationLanguages')?.setValidators([Validators.required]);
		this.audioRequestForm.get('audioTranslationLanguages')?.updateValueAndValidity();	
		

		if (this.audioRequestForm.valid)
		{		    
			const audioRequest: AudioRequest = Object.assign({}, this.audioRequestForm.value);
			this.audioRequest = audioRequest;
			this.audioRequest.recognitionText = "";
			if (this.audioRequest.validationRecognition)
			{
				this.getRecognitionTextForValidation();
			}
			else
			{				
				this.generate();
			}		
		}
	}

	// calls API for getting recognition text for validation
	getRecognitionTextForValidation(): Subscription
	{
		this.audioShow = false;
	    this.audioLoading = true;
		return this.generateService.getRecognitionText(this.audioRequest).subscribe((data: AudioResponseAPI)=>
	    {
			this.openRecognitionValidation(data);
	    });		
	}

	// send the request for getting speech
	generate(): Subscription 
	{
	    this.audioShow = false;
	    this.audioLoading = true;
	    return this.generateService.getTranslatedVoices(this.audioRequest).subscribe((data: VoiceResponseAPI[])=>
	    {
	      this.onResponse(data);	      
	    });
	}

	// send the request for getting speech after translation validation
	generateAfterValidation(): Subscription 
	{
	    this.audioShow = false;
		this.audioLoading = true;
	    return this.generateService.getTranslatedVoicesAfterValidation(this.unvalidatedTranslations).subscribe((data: VoiceResponseAPI[])=>
	    {
	      this.onResponse(data);	      
		});
	}

	// open the recognition validation modal
	openRecognitionValidation(audioResponse: AudioResponseAPI): void 
	{
		this.audioLoading = false;
		const inputLanguageName =  audioResponse.language;
		this.recognitionText = audioResponse.text!;
		const recognitionText = audioResponse.text;

		// open the dialog
		const dialogRef = this.dialog.open(RecognitionValidationComponent, 
		{
		  width: '90%',
		  data: {recognitionText, inputLanguageName}
		});

		// when validation is done
		dialogRef.afterClosed().subscribe(result => 
		{
			this.audioRequest.recognitionText = result;
			this.recognitionText = result;
			this.audioRequest.validationRecognition = false;
			if (result !== 'CANCEL')
			{
				this.generate();
			}	  
		});
	}

	// open the translation validation modal
	openTranslationValidation(translation: VoiceResponseAPI, index: number): void 
	{
		this.audioLoading = false;
		const voiceRequestText =  this.recognitionText !=='' ? this.recognitionText : this.audioRequestForm.get('audioRequestText')?.value; 
		const inputLanguageName =  this.audioInputLanguage.languageName;
		const translatedText = translation.text;
		const outputLanguageName = translation.language;

		// open the dialog
		const dialogRef = this.dialog.open(TranslationValidationComponent, 
		{
		  width: '90%',
		  data: {voiceRequestText, inputLanguageName, translatedText, outputLanguageName}
		});
		
		// when validation is done
		dialogRef.afterClosed().subscribe(result => 
		{
			if (result === 'CANCEL')
			{
				// remove translation from translation to be done
				this.unvalidatedTranslations.splice(index,1);
			} 
			else
			{
				this.unvalidatedTranslations[index].text = result;
			} 			
			// remove translation from items to be validated
			this.validationStatus.splice(this.validationStatus.indexOf(index), 1);
			// check if all validations have been done and at least one is required
			if (this.validationStatus.length === 0 && this.unvalidatedTranslations.length > 0)
			{
				  this.generateAfterValidation();
			}					  
		});
	}

	// when receiving response
	onResponse(data: VoiceResponseAPI[]): void 
	{
		const  first: VoiceResponseAPI = data[0];
		if (first && first.mp3Path === "")
		{
			this.unvalidatedTranslations = data;
			let index = 0;
			// open validation modal for each translation done
			data.forEach((voiceResponse) => 
			{
				this.validationStatus.push(index);
				this.openTranslationValidation(voiceResponse, index);
				index ++;	
			});
		}
		else
		{
			this.producesTranslatedVoices(data);
		}					
	}

	// requests translations + text to speech 
	producesTranslatedVoices(data: VoiceResponseAPI[]): void 
	{
		this.audioResponses = data;
		this.audioLoading = false;
	  	this.audioShow = true;
	}

	// RAZ
	audioRevert() : void 
	{
		this.audioRequestForm.reset();
		this.audioRequestForm.reset({ audioLoading: new AudioRequest() });
		this.loadLanguages();
		this.audioSelectedTranslationLanguages = [];
		this.audioShow=false;
		this.audioLoading = false;
		this.audioSubmitted = false;
		this.isReadyToUpload = false;
		this.filePath = "";
	}

	// Load audio player with file
	loadUploadedFile() : void
	{		
		// load in audio player
		this.filePath =  this.uploadResponse.uploadedFilePath!;		
		// update selected file path and its placeholder
		this.audioRequestForm.get('audioFilePath')?.setValue(this.filePath);
		this.pathPlaceHolder = "";
		this.progress = -1;
		this.isReadyToUpload = false; 		
	}

	startRecorder(): void 
	{
		this.recorder = new this.MicRecorder({
			bitRate: 128,
			encoder: 'wav', // default is mp3, can be wav as well
			sampleRate: 44100, // default is 44100, it can also be set to 16000 and 8000.
		});
		this.startBtnEnabled = false;
		this.stopBtnEnabled = true;
		// Start recording. Browser will request permission to use your microphone.
		this.recorder.start().then(() => 
		{
			// something else
		}).catch((e: any) => 
		{
			alert(e);
		});
	};

	stopRecorder(): void 
	{
		this.recorder
		.stop()
		.getAudio()
		.then(([buffer, blob]: any) =>
		{
			this.startBtnEnabled = true;
			this.stopBtnEnabled = false;
			// Example: Create a mp3 file and play
			const filename = 'RecordedVoice-' + Math.random().toString(20).substr(2, 16) + '.wav';
			const file = new File(buffer, filename, 
			{
				type: blob.type,
				lastModified: Date.now()
			});

			this.currentFile = file;
			this.isReadyToUpload = true;
			this.upload();

		}).catch((e: any) => {
			alert('We could not retrieve your message :'+e);
		});
	};
}