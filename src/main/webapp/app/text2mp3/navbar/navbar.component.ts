import { Component, OnInit } from '@angular/core';
import { VERSION } from 'app/app.constants';
import { faFlag, faBars } from '@fortawesome/free-solid-svg-icons';
import { LANGUAGES } from 'app/core/language/language.constants';
import { JhiLanguageService } from 'ng-jhipster';
import { registerLocaleData } from '@angular/common';
import locale from '@angular/common/locales/en';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isNavbarCollapsed = true;
  version: string;
  languages = LANGUAGES;

  // Icons
  faFlag = faFlag;
  faBars = faBars;

  constructor(private translate: TranslateService, private languageService: JhiLanguageService) {
    this.version = VERSION ? (VERSION.toLowerCase().startsWith('v') ? VERSION : 'v' + VERSION) : '';
  }

  ngOnInit(): void {
    registerLocaleData(locale);
    this.languageService.init();
  }

  changeLanguage(languageKey: string): void {
    this.languageService.changeLanguage(languageKey);
  }

  collapseNavbar(): void {
    this.isNavbarCollapsed = true;
  }

  toggleNavbar(): void {
    this.isNavbarCollapsed = !this.isNavbarCollapsed;
  }
}
