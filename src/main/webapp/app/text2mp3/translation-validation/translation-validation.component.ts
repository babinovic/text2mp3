import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { faLanguage, faSignLanguage } from '@fortawesome/free-solid-svg-icons';


export interface TranslationValidationData 
{
  voiceRequestText: string;
  inputLanguageName: string;
  outputLanguageName: string;
  translatedText: string;
}

@Component({
  selector: 'jhi-translation-validation',
  templateUrl: './translation-validation.component.html',
  styleUrls: ['./translation-validation.component.scss']
})
export class TranslationValidationComponent implements OnInit
{
  voiceRequestText: string;
  inputLanguageName: string;
  translatedText: string;
  outputLanguageName: string;
  validationForm: FormGroup;

  faSignLanguage=faSignLanguage;faLanguage=faLanguage;

  constructor(formBuilder: FormBuilder, public dialogRef: MatDialogRef<TranslationValidationComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: TranslationValidationData) 
  {
    this.voiceRequestText = this.data.voiceRequestText;
    this.inputLanguageName = this.data.inputLanguageName;
    this.translatedText = this.data.translatedText;
    this.outputLanguageName = this.data.outputLanguageName;
    this.validationForm = this.createFormGroup(formBuilder);
  }

  ngOnInit(): void 
  {
  }

  onSubmit(): void
  {
    const result = this.translatedText;
    this.dialogRef.close(result);
  }

  // Creates the forrm group for text to speech request
	createFormGroup(formBuilder: FormBuilder) : FormGroup
	{
		return formBuilder.group({
			'translationText': new FormControl(this.translatedText, Validators.required),
		  });
  }
  
  onNoClick(): void 
  {
    this.dialogRef.close('CANCEL');
  }
}
