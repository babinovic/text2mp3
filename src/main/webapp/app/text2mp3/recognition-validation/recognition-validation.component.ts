import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { faRss, faLanguage } from '@fortawesome/free-solid-svg-icons';


export interface RecognitionValidationData 
{
  recognitionText: string;
  inputLanguageName: string;
}

@Component({
  selector: 'jhi-recognition-validation',
  templateUrl: './recognition-validation.component.html',
  styleUrls: ['./recognition-validation.component.scss']
})
export class RecognitionValidationComponent implements OnInit
{
  recognitionText: string;
  inputLanguageName: string;
  validationForm: FormGroup;
  speechDetected = true;

  faRss=faRss; faLanguage=faLanguage;

  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<RecognitionValidationComponent>, @Inject(MAT_DIALOG_DATA) public data: RecognitionValidationData) 
  {
    this.recognitionText = this.data.recognitionText;
    this.inputLanguageName = this.data.inputLanguageName;
    this.validationForm = this.createFormGroup(formBuilder);
  }

  ngOnInit(): void 
  {
    if ((this.recognitionText === null) || (this.recognitionText.length < 2))
    {
      this.speechDetected = false;
    }
  }

  onSubmit(): void
  {
    const result = this.recognitionText;
    this.dialogRef.close(result);
  }

  // Creates the forrm group for text to speech request
	createFormGroup(formBuilder: FormBuilder) : FormGroup
	{
		return formBuilder.group({
			'recognitionText': new FormControl(this.recognitionText, Validators.required),
		  });
	}

  onNoClick(): void 
  {
    this.dialogRef.close('CANCEL');
  }
}
