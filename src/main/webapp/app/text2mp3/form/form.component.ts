import { Component, OnInit, Renderer2, RendererFactory2 } from '@angular/core';
import { Router } from '@angular/router';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  private renderer: Renderer2;

  navLinks: any[];
  activeLinkIndex = -1;
  constructor(private router: Router, private translateService: TranslateService, rootRenderer: RendererFactory2) {
    this.navLinks = [
      {
        label: 'TEXT TRANSLATOR',
        link: './text',
        index: 0
      },
      {
        label: 'AUDIO TRANSLATOR',
        link: './audio',
        index: 1
      },
      {
        label: 'ABOUT',
        link: './about',
        index: 2
      }
    ];

    this.renderer = rootRenderer.createRenderer(document.querySelector('html'), null);
  }

  ngOnInit(): void {
    this.router.events.subscribe(() => {
      this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
    });

    this.translateService.onLangChange.subscribe((langChangeEvent: LangChangeEvent) => {
      this.renderer.setAttribute(document.querySelector('html'), 'lang', langChangeEvent.lang);
    });
  }
}
