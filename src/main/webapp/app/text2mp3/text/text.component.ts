import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { VoiceRequest} from './../../shared/model/voice-request.model'
import { IVoiceStyle } from '../../shared/model/voice-style.model';
import { LanguageService } from '../language.service';
import { Subscription } from 'rxjs';
import { GenerateService } from '../../generate.service';
import { PlayerFile, PlayerTheme, PlayerThemeDark } from 'gs-player';
import { VoiceResponseAPI } from 'app/shared/model/voice-response-api.model';
import { faUserCheck,  faLanguage, faUndo, faAudioDescription, faMicrophone, faRss, faSignLanguage, faDownload } from '@fortawesome/free-solid-svg-icons';
import { ILanguage, Language } from 'app/shared/model/language.model';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { TranslationValidationComponent } from '../translation-validation/translation-validation.component';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'jhi-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit 
{
	voiceRequestText = "";	
	voiceRequestForm: FormGroup;
	voiceRequest!: VoiceRequest;
	voiceStyle!: IVoiceStyle;
	voiceResponses: VoiceResponseAPI[] = [];
	languages: any = []; selectedTranslationLanguages: any = []; availableTranslationLanguages: any = []
	selectedLanguage!: ILanguage; selectedTranslationLanguage!: ILanguage; selectedVoice!: IVoiceStyle;
	selectedTranslationLanguageName?: string;
	voiceStyles: any = [];
	files: Array<PlayerFile> = [];
	mp3Path = "";
	playerTheme: PlayerTheme = PlayerThemeDark;
  
	// Booleans
	show = false;
	loading = false;
	submitted = false;
	translated = false;
	inProduction?: boolean;
	swaggerEnabled?: boolean;
	
	// Icons
	faUserCheck=faUserCheck;faLanguage=faLanguage;faUndo=faUndo;faAudioDescription=faAudioDescription;faMicrophone=faMicrophone;faRss=faRss;faSignLanguage=faSignLanguage;faDownload=faDownload;
	
	// Translation Validation
	unvalidatedTranslations: VoiceResponseAPI[] = [];
	validationStatus: number[] = [];

	constructor(formBuilder: FormBuilder,public languageService: LanguageService, public generateService: GenerateService,
		public dialog: MatDialog, public _snackBar: MatSnackBar) 
	{
		this.voiceRequestForm = this.createFormGroup(formBuilder);
	}

	ngOnInit(): void 
	{
		this.main()
		.then()
		.catch(() => alert('Error loading languages'));	
	}

	main = async () => 
	{
		try 
		{
			await this.loadLanguages();
			this.getVoiceStyles(this.selectedLanguage);
		}
		catch (error)
		{
			alert('Error loading data : '+ error);
		}
	};


	// Creates the forrm group for text to speech request
	createFormGroup(formBuilder: FormBuilder) : FormGroup
	{
		return formBuilder.group({
			'voiceRequestText': new FormControl("", Validators.required),
			'voiceRequestTextFormat': new FormControl("text", Validators.required),
			'voiceStyle': new FormControl("",Validators.required),
			'translationLanguages': new FormControl([]),
			'validation': new FormControl(false),
		  });
	}

	async loadLanguages(): Promise<void>
	{
		return this.languageService.getLanguages()
		.toPromise()
		.then((data: {}) => 
			{
				this.languages = data;
				this.selectedLanguage = data[0];
				this.selectedTranslationLanguageName = this.selectedLanguage.languageName;
				this.availableTranslationLanguages = data;
			});
	}

	// when language is changed
	onLanguageChange(language: Language): void
	{
		this.selectedLanguage = language;
		this.selectedTranslationLanguageName = this.selectedLanguage.languageName;
		this.getVoiceStyles(language);		
	}
	
	// load voice styles list
	getVoiceStyles(language: Language) : Promise<void>
	{
		return this.languageService.getVoiceStyles(language)
		.toPromise()
		.then((data: {}) => 
		{			
			this.voiceStyles = data;
			this.selectedVoice = data[0];
		});
	}

	onTranslationAtivation() : any
	{
		if (this.translated)
		{
			this.voiceRequestForm.get('translationLanguages')?.setValidators([]);
			this.voiceRequestForm.get('translationLanguages')?.updateValueAndValidity();
		}
		else
		{
			this.openSnackBar('Choose one or several languages of translation.');
		}
		this.translated = !this.translated;
		this.show = false;
	}

	addTranslationLanguage() : any
	{
		this.selectedTranslationLanguages.push(this.selectedTranslationLanguage);
	}

	removeTranslationLanguage(translationLanguage: Language): any 
	{
		this.selectedTranslationLanguages.splice(this.selectedTranslationLanguages.indexOf(translationLanguage), 1);
	}

	hasError = (controlName: string, errorName: string) =>
	{
		return this.voiceRequestForm.controls[controlName].hasError(errorName);
	}

	// au clic sur Generate
	onSubmit(): void  
	{
		this.submitted = true;
		if (this.translated)
		{
			this.voiceRequestForm.get('translationLanguages')?.setValue(this.selectedTranslationLanguages); 
			this.voiceRequestForm.get('translationLanguages')?.setValidators([Validators.required]);
			this.voiceRequestForm.get('translationLanguages')?.updateValueAndValidity();	
		}
		else
		{
			this.selectedTranslationLanguages = [];
			this.voiceRequestForm.get('translationLanguages')?.setValue(this.selectedTranslationLanguages); 
			this.voiceRequestForm.get('translationLanguages')?.setValidators([]);
			this.voiceRequestForm.get('translationLanguages')?.updateValueAndValidity();
		}
		if (this.voiceRequestForm.valid)
		{		    
			const voiceRequest: VoiceRequest = Object.assign({}, this.voiceRequestForm.value);
			this.voiceRequest = voiceRequest;
			this.generate();
		}
	}

	// send the request for getting speech
	generate(): Subscription 
	{
	    this.show = false;
	    this.loading = true;
	    return this.generateService.getVoices(this.voiceRequest).subscribe((data: VoiceResponseAPI[])=>
	    {
	      this.onResponse(data);	      
	    });
	}

	// send the request for getting speech after translation validation
	generateAfterValidation(): Subscription 
	{
	    this.show = false;
		this.loading = true;
	    return this.generateService.getTranslatedVoicesAfterValidation(this.unvalidatedTranslations).subscribe((data: VoiceResponseAPI[])=>
	    {
	      this.onResponse(data);	      
	    });
	}

	// when receiving response
	onResponse(data: VoiceResponseAPI[]): void 
	{
		if (this.translated)
		{
			const  first: VoiceResponseAPI = data[0];
			if (first.mp3Path === "")
			{
				this.unvalidatedTranslations = data;
				let index = 0;
				// open validation modal for each translation done
				data.forEach((voiceResponse) => 
				{
					this.validationStatus.push(index);
					this.openTranslationValidation(voiceResponse, index);
					index ++;	
				});
			}
			else
			{
				this.producesTranslatedVoices(data);
			}						
		}
		else
		{
			this.producesVoice(data);
		}
	}

	// open the translation validation modal
	openTranslationValidation(translation: VoiceResponseAPI, index: number): void 
	{
		this.loading = false;
		const voiceRequestText =  this.voiceRequestForm.get('voiceRequestText')?.value;
		const inputLanguageName =  this.selectedTranslationLanguageName;
		const translatedText = translation.text;
		const outputLanguageName = translation.language;

		// open the dialog
		const dialogRef = this.dialog.open(TranslationValidationComponent, 
		{
		  width: '90%',
		  data: {voiceRequestText, inputLanguageName, translatedText, outputLanguageName}
		});
		
		// when validation is done
		dialogRef.afterClosed().subscribe(result => 
		{
			if (result === 'CANCEL')
			{
				// remove translation from translation to be done
				this.unvalidatedTranslations.splice(index,1);
			} 
			else
			{
				this.unvalidatedTranslations[index].text = result;
			} 			
			// remove translation from items to be validated
			this.validationStatus.splice(this.validationStatus.indexOf(index), 1);
			// check if all validations have been done and at least one is required
			if (this.validationStatus.length === 0 && this.unvalidatedTranslations.length > 0)
			{
				  this.generateAfterValidation();
			}			  
		});
	}

	// load the response on UI
	producesVoice(data: VoiceResponseAPI[]): void 
	{
		this.loadAudioFile(data[0].mp3Path);
		this.loading = false;
	    this.show = true;
	}

	// load the response on UI
	producesTranslatedVoices(data: VoiceResponseAPI[]): void 
	{
		this.loadAudioFile(data[0].mp3Path);
		this.voiceResponses = data;
		this.loading = false;
	    this.show = true;
	}

	// RAZ
	revert() : void 
	{
		this.voiceRequestForm.reset();
		this.voiceRequestForm.reset({ voiceRequest: new VoiceRequest() });
		this.loadLanguages();
		this.selectedTranslationLanguages = [];
		this.show = false;
		this.loading = false;
		this.submitted = false;
	}

	// open the snackbar to display specified message
	openSnackBar(message: string) : void 
	{
		this._snackBar.open(message, 'Close', {
			duration: 5000,
			horizontalPosition: 'center',
			verticalPosition: 'top',
		  });
	  }

	// Load audio player with file
	loadAudioFile(filePath?: string) : void
	{
		const mp3File: PlayerFile = {url: "",name: ""};
		mp3File.url = filePath!;
		mp3File.name = this.selectedVoice.voiceStyleName + " - " + this.voiceRequestForm.value.voiceRequestText.slice(0,30)+"...";
		this.files.unshift(mp3File);
		this.mp3Path =  mp3File.url;
	}
}