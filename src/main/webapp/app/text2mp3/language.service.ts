import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Language } from '../shared/model/language.model';
import { VoiceStyle } from '../shared/model/voice-style.model';
import { TranslationLanguage } from 'app/shared/model/translation-language.model';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  baseurl = './../api';
  url: string;

   // Http Headers
   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private http: HttpClient) 
  {
    this.url = location.hostname + this.baseurl;
  }

  // Get languages list
  getLanguages(): Observable<Language> {
    return this.http.get<Language>(this.url + '/languages-default/' + navigator.language)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  // Get languages list
  getTranslationLanguages(): Observable<TranslationLanguage> {
    return this.http.get<TranslationLanguage>(this.url + '/translation-languages')
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  // Get languages list
  getVoiceStyles(language: Language): Observable<VoiceStyle> {
    return this.http.get<VoiceStyle>(this.url + '/voice-styles-by-language/'+language.id)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  // Error handling
  errorHandl(error: any): Observable<any> 
  {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) 
    {
      // Get client-side error
      errorMessage = error.error.message;
    } 
    else 
    {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
 }
}
