import { Component } from '@angular/core';
import { faAudioDescription } from '@fortawesome/free-solid-svg-icons';
import { VERSION } from 'app/app.constants';

@Component({
  selector: 'jhi-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent {
  faAudioDescription = faAudioDescription;
  version: string;

  constructor() {
    this.version = VERSION ? (VERSION.toLowerCase().startsWith('v') ? VERSION : 'v' + VERSION) : '';
  }
}
