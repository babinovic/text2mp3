import { NgModule } from '@angular/core';
import './vendor';
import { Text2Mp3AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
// Material
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule, MAT_RADIO_DEFAULT_OPTIONS } from '@angular/material/radio';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { NgxAudioPlayerModule } from 'ngx-audio-player';

import { RouterModule } from '@angular/router';
import { FormComponent } from './text2mp3/form/form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GenerateService } from './generate.service';
import { LanguageService } from './text2mp3/language.service';
import { ErrorComponent } from 'app/layouts/error/error.component';
import { FooterComponent } from 'app/layouts/footer/footer.component';
import { AudioComponent } from './text2mp3/audio/audio.component';
import { VideoComponent } from './text2mp3/video/video.component';
import { TextComponent } from './text2mp3/text/text.component';
import { NavbarComponent } from './text2mp3/navbar/navbar.component';
import { UploadService } from './upload.service';
import { TranslationValidationComponent } from './text2mp3/translation-validation/translation-validation.component';
import { RecognitionValidationComponent } from './text2mp3/recognition-validation/recognition-validation.component';
import { AboutComponent } from './text2mp3/about/about.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { Text2Mp3SharedModule } from './shared/shared.module';
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';
import { NgJhipsterModule, translatePartialLoader, missingTranslationHandler, JhiConfigService } from 'ng-jhipster';
import { NgxWebstorageModule } from 'ngx-webstorage';

@NgModule({
  imports: [
    BrowserModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    Text2Mp3AppRoutingModule,
    HttpClientModule,
    Text2Mp3SharedModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatIconModule,
    MatCheckboxModule,
    MatChipsModule,
    MatRadioModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatSnackBarModule,
    NgxAudioPlayerModule,
    NgbModule,
    NgJhipsterModule.forRoot({
      i18nEnabled: true,
      defaultI18nLang: 'en'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translatePartialLoader,
        deps: [HttpClient]
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useFactory: missingTranslationHandler,
        deps: [JhiConfigService]
      }
    })
  ],
  exports: [RouterModule],
  providers: [
    GenerateService,
    LanguageService,
    UploadService,
    {
      provide: MAT_RADIO_DEFAULT_OPTIONS,
      useValue: { color: 'primary' }
    },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: { hasBackdrop: false }
    }
  ],
  declarations: [
    FormComponent,
    ErrorComponent,
    FooterComponent,
    AudioComponent,
    VideoComponent,
    TextComponent,
    NavbarComponent,
    TranslationValidationComponent,
    RecognitionValidationComponent,
    AboutComponent,
    ActiveMenuDirective
  ],
  bootstrap: [FormComponent]
})
export class Text2Mp3AppModule {}
